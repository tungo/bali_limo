<?php

class UpdateStatusEventHandler {

	CONST EVENT   = 'status.update';
	CONST CHANNEL = 'status.update';

  public function handle($data)
  {
    $redis = Redis::connection();
    $redis->publish(self::CHANNEL, $data);
  }

  public function handleAdmin($data)
  {
    $redis = Redis::connection();
    $redis->publish(Config::get('admin.event.status_update'), $data);
  }

  public function handlePartner($data)
  {
    $redis = Redis::connection();
    $redis->publish(Config::get('partner.event.status_update'), $data);
  }

  public function handleCar($data)
  {
    $redis = Redis::connection();
    $redis->publish(Config::get('car.event.status_update'), $data);
  }

  public function handleCarWork($data)
  {
    $redis = Redis::connection();
    $redis->publish(Config::get('admin.event.car_work'), $data);
  }
}