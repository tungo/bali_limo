<?php

function fn_print($array = array())
{
  echo '<pre>';
  print_r($array);
  echo '</pre>';
}
function fn_printe($array = array())
{
  echo '<pre>';
  print_r($array);
  echo '</pre>';
  exit;
}
function fn_vdump($array = array())
{
  echo '<pre>';
  var_dump($array);
  echo '</pre>';
}
function fn_vdumpe($array = array())
{
  echo '<pre>';
  var_dump($array);
  echo '</pre>';
  exit;
}

function fn_sort_link($name = '', $value = '', $filter = array(), $options = array())
{
  $image = ($filter['sort'] == $value) ? $options['image'] : '';
  $html = '<a href="'.$options['link'].'&sort='.$value.'">'.$name.$image.'</a>';
  return $html;
}
function fn_select_per_page($query_strings, $route, $name)
{
  $query_strings = array_except($query_strings, ['per_page']);
  $html = '';
  for ($i = 10; $i <= 100; $i = $i + 10)
  {
    $query_strings['per_page'] = $i;
    $html .= '<li><a href="'.URL::route($route, $query_strings).'">'.$i.' '.$name.'</a></li>';
  }
  return $html;
}

function fn_datetime($timestamp, $from_format = 'Y-m-d H:i:s', $to_format = 'd-m-Y H:i', $default = 'default')
{
  $dt = DateTime::createFromFormat($from_format, $timestamp);
  if ($dt && $dt->format($from_format) == $timestamp)
  {
    return $dt->format($to_format);
  }
  else
  {
    // if ($default == 'default')
    // {
    //   $dt = new DateTime('0000-00-00');
    //   return $dt->format($to_format);
    // }
    if ($default == 'now')
    {
      $dt = new DateTime(date('Y-m-d'));
      return $dt->format($to_format);
    }
    else
    {
      return null;
    }
  }
}

function fn_datetime_get($timestamp)
{
  return fn_datetime($timestamp, 'Y-m-d H:i:s', 'd-m-Y H:i:s');
}

function fn_datetime_set($timestamp)
{
  return fn_datetime($timestamp, 'd-m-Y H:i:s', 'Y-m-d H:i:s');
}

function fn_datetime_get_month_time($month, $format = 'm-Y')
{
  $start_date = fn_datetime('01-'.$month, 'd-m-Y', 'Y-m-d', null);
  if ($start_date)
  {
    $end_date = date("Y-m-t", strtotime($start_date));
    $start_time = $start_date.' 00:00:00';
    $end_time = $end_date.' 23:59:59';
    return [$start_time, $end_time];
  }
  else
  {
    return [null, null];
  }
}
function fn_datetime_get_month_date($month, $format = 'm-Y')
{
  $start_date = fn_datetime('01-'.$month, 'd-m-Y', 'Y-m-d', null);
  if ($start_date)
  {
    $end_date = date("Y-m-t", strtotime($start_date));
    return [$start_date, $end_date];
  }
  else
  {
    return [null, null];
  }
}
function fn_datetime_get_month_last_date($month, $format = 'm-Y')
{
  $start_date = fn_datetime('01-'.$month, 'd-m-Y', 'Y-m-d', null);
  if ($start_date)
  {
    $last_date = date("t", strtotime($start_date));
    return $last_date;
  }
  else
  {
    return null;
  }
}
function fn_datetime_start_day($timestamp, $format = 'd-m-Y')
{
  $date = fn_datetime($timestamp, 'd-m-Y', 'Y-m-d', null);
  return $date.' 00:00:00';
}
function fn_datetime_end_day($timestamp, $format = 'd-m-Y')
{
  $date = fn_datetime($timestamp, 'd-m-Y', 'Y-m-d', null);
  return $date.' 23:59:59';
}
function fn_datetime_get_week_date($date) {
    $timestamp = strtotime($date);
    $start = date('w', $timestamp) == 1 ? $timestamp : strtotime('last monday', $timestamp);
    $start_date = date('Y-m-d', $start);
    $end_date = date('Y-m-d', strtotime('next monday', $start));
    return [$start_date, $end_date];
}

function fn_init_parser($inputs)
{
  $parser = [];
  if (!empty($inputs))
  {
    foreach ($inputs as $label => $input)
    {
      $parser[] = [
        'label' => trans('tn.'.$label),
        'input' => $input,
      ];
    }
  }
  return $parser;
}

function fn_url_put($default = '')
{
  $path = $default ? $default : URL::full();
  Session::put('tn_url.intended', $default);
}
function fn_url_get($default = '')
{
  $path = Session::get('tn_url.intended', $default);
  Session::forget('tn_url.intended');
  return $path;
}

function fn_hide_price($price = 0, $default = '')
{
  return $price > 0 ? $price + 0 : $default;
}

function fn_theme_pre_table_form($logged_in_user, $main_route)
{
  $status = [
    0 => '<span class="label label-warning"><i class="icon-block-4"></i> '.trans('tn.inactive').'</span>',
    1 => '<span class="label label-success"><i class="icon-ok-circled2-1"></i> '.trans('tn.active').'</span>',
  ];
  if ($logged_in_user->hasAccess($main_route.'.enable'))
  {
    $status[0] = '<a href="[url]" class="btn btn-warning btn-xs" title="'.trans('tn.click_to_enable').'"><i class="icon-block-4"></i> '.trans('tn.inactive').'</a>';
  }
  if ($logged_in_user->hasAccess($main_route.'.disable'))
  {
    $status[1] = '<a href="[url]" class="btn btn-success btn-xs" title="'.trans('tn.click_to_disable').'"><i class="icon-ok-circled2-1"></i> '.trans('tn.active').'</a>';
  }

  $position = '[value]';
  if ($logged_in_user->hasAccess($main_route.'.save'))
  {
    $position = Form::text('position[[id]]', '[value]', ['class' => 'form-control input-sm input-text text-center']);
  }

  $control = '';
  if ($logged_in_user->hasAccess($main_route.'.edit'))
  {
    $control .= '<a href="[edit_url]" class="btn btn-primary btn-xs" title="Edit"><i class="icon-pencil"></i> '.trans('tn.edit').'</a> ';
  }
  if ($logged_in_user->hasAccess($main_route.'.delete'))
  {
    $control .= '<a href="[delete_url]" class="btn btn-danger btn-xs" title="Delete" onclick="return confirm(\''.trans('tn.confirm_delete').'\')"><i class="icon-trash-8"></i> '.trans('tn.delete').'</a>';
  }
  return [$status, $position, $control];
}

// function fn_replace_params(&$default = array(), $new = array())
// {
//   if (empty($default) || empty($new)) {
//     return false;
//   }

//   foreach ($new as $key => $value) {
//     if (array_key_exists($key, $default)) {
//       $default[$key] = $value;
//     }
//   }
//   return true;
// }
// function fn_query_remove()
// {
//   $args = func_get_args();
//   $query_string = array_shift($args);

//   if ( ! empty($args))
//   {
//     parse_str($query_string, $params);
//     foreach ($args as $arg)
//     {
//       unset($params[$arg]);
//     }
//     $query_string = http_build_query($params);
//   }

//   return $query_string;
// }