<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

foreach (['admin', 'partner', 'car'] as $module)
{
	$prefix = Config::get($module.'.prefix');
	$module_path = Config::get($module.'.path');

	$routes_objects = Config::get($module.'.routes_objects');
	$routes_actions = Config::get($module.'.routes_actions');
	foreach ($routes_objects as $object => $properties)
	{
		list($controller, $disabled) = $properties;
		foreach ($routes_actions as $action => $action_properties)
		{
			if (in_array($action, $disabled)) continue;
			list($verb, $path, $method) = $action_properties;
			Route::$verb($module_path.'/'.$object.$path, [
				'as' => $prefix.'.'.$object.$action,
				'uses' => $controller.'@'.$method,
			]);
		}
	}

	$module_routes = Config::get($module.'.routes');
	foreach ($module_routes as $objects)
	{
		list($object, $controller, $actions) = $objects;
		foreach ($actions as $action_properties)
		{
			list($verb, $path, $action, $method) = $action_properties;
			Route::$verb($module_path.'/'.$object.$path, [
				'as' => $prefix.'.'.$object.$action,
				'uses' => $controller.'@'.$method,
			]);
		}
	}

}

// Route::group(['prefix' => Config::get('partner.path')], function()
// {
// 	$prefix = Config::get('partner.prefix');

// 	$routes_objects = Config::get('partner.routes_objects');
// 	$routes_actions = Config::get('partner.routes_actions');
// 	foreach ($routes_objects as $object => $objects)
// 	{
// 		list($controller, $disabled) = $objects;
// 		foreach ($routes_actions as $action => $actions)
// 		{
// 			if (in_array($action, $disabled)) continue;
// 			list($verb, $path, $method) = $actions;
// 			Route::$verb($object.$path, [
// 				'as' => $prefix.'.'.$object.$action,
// 				'uses' => $controller.'@'.$method,
// 			]);
// 		}
// 	}

// 	$partner_routes = Config::get('partner.routes');
// 	foreach ($partner_routes['booking'] as $booking)
// 	{
// 		list($verb, $path, $action, $method) = $booking;
// 		Route::$verb('booking'.$path, [
// 			'as' => $prefix.'.booking'.$action,
// 			'uses' => 'PartnerBookingController@'.$method,
// 		]);
// 	}

// 	// system
// 	Route::get('/', [
// 		'as' => $prefix.'.dashboard',
// 		'uses' => 'PartnerSystemController@getIndex',
// 	]);

// 	// auth
// 	Route::get('login', [
// 		'as' => $prefix.'.login',
// 		'uses' => 'PartnerAuthController@getLogin',
// 	]);
// 	Route::post('login', [
// 		'as' => $prefix.'.login.post',
// 		'uses' => 'PartnerAuthController@postLogin',
// 	]);
// 	Route::get('logout', [
// 		'as' => $prefix.'.logout',
// 		'uses' => 'PartnerAuthController@getLogout',
// 	]);

// });

// Route::group(['prefix' => Config::get('car.path')], function()
// {
// 	$prefix = Config::get('car.prefix');

// 	$routes_objects = Config::get('car.routes_objects');
// 	$routes_actions = Config::get('car.routes_actions');
// 	foreach ($routes_objects as $object => $objects)
// 	{
// 		list($controller, $disabled) = $objects;
// 		foreach ($routes_actions as $action => $actions)
// 		{
// 			if (in_array($action, $disabled)) continue;
// 			list($verb, $path, $method) = $actions;
// 			Route::$verb($object.$path, [
// 				'as' => $prefix.'.'.$object.$action,
// 				'uses' => $controller.'@'.$method,
// 			]);
// 		}
// 	}

// 	$car_routes = Config::get('car.routes');
// 	foreach ($car_routes['booking'] as $booking)
// 	{
// 		list($verb, $path, $action, $method) = $booking;
// 		Route::$verb('booking'.$path, [
// 			'as' => $prefix.'.booking'.$action,
// 			'uses' => 'CarBookingController@'.$method,
// 		]);
// 	}

// 	// system
// 	Route::get('/', [
// 		'as' => $prefix.'.dashboard',
// 		'uses' => 'CarSystemController@getIndex',
// 	]);

// 	// auth
// 	Route::get('login', [
// 		'as' => $prefix.'.login',
// 		'uses' => 'CarAuthController@getLogin',
// 	]);
// 	Route::post('login', [
// 		'as' => $prefix.'.login.post',
// 		'uses' => 'CarAuthController@postLogin',
// 	]);
// 	Route::get('logout', [
// 		'as' => $prefix.'.logout',
// 		'uses' => 'CarAuthController@getLogout',
// 	]);
// 	Route::post('logout', [
// 		'as' => $prefix.'.logout.post',
// 		'uses' => 'CarAuthController@postLogout',
// 	]);

// });
