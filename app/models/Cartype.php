<?php

class Cartype extends Admin {

	protected $guarded = ['id'];
	protected $table = 'cartypes';

	public function cars()
	{
		return $this->hasMany('Car');
	}

	public function delete()
	{
		if (count($this->cars))
		{
			throw new TnExceptions\RelationRecordExistException('car');
			return false;
		}

		return parent::delete();
	}

	public static function getCodesSelect($options = [])
	{
		$query = static::select('code');

		if (in_array('active', $options))
		{
			$query->where('status', '=', 1);
		}
		if (!in_array('no_position', $options))
		{
			$query->orderBy('position', 'asc');
		}

		$items = $query->get();
		$return = [];
		if (in_array('all', $options))
		{
			$return = array_add($return, '', '-- '.trans('tn.all').' --');
		}
		if (in_array('none', $options))
		{
			$return = array_add($return, 0, trans('tn.none'));
		}

		foreach ($items as $item)
		{
			$return[$item->code] = $item->code;
		}
		return $return;
	}

	public static function getCodesName($options = [])
	{
		$items = static::select('name', 'code')->get();
		$return = [];
		foreach ($items as $item)
		{
			$return[$item->code] = $item->name;
		}
		return $return;
	}
}
