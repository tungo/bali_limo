<?php

class AnnoyingClient extends Admin {

	protected $guarded = ['id'];
	protected $table = 'annoying_clients';

	public function partner()
	{
    return $this->belongsTo('Partner');
	}

	public static function check($customer, $partner_id)
	{
		return static::where('customer', '=', $customer)
		->where('partner_id', '=', $partner_id)
		->count();
	}

}
