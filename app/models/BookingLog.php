<?php

class BookingLog extends Admin {

	protected $guarded = ['id'];
	protected $table = 'booking_logs';

	public function booking()
	{
    return $this->belongsTo('Booking');
	}
	public function status()
	{
    return $this->belongsTo('BookingStatus', 'status_id');
	}
	public function user()
	{
    return $this->belongsTo('User');
	}

	public static function initData($data = [])
	{
		if (!empty($data['partner_id']))
		{
			$partner = Partner::find($data['partner_id']);
			$data['partner'] = '('.$partner->id.') '.$partner->name;
			unset($data['partner_id']);
		}
		if (!empty($data['driver_id']))
		{
			$driver = Driver::find($data['driver_id']);
			$data['driver'] = '('.$driver->id.') '.$driver->full_name;
			unset($data['driver_id']);
		}
		if (!empty($data['car_id']))
		{
			$car = Car::find($data['car_id']);
			$data['car_number'] = '('.$car->id.') '.$car->number;
			unset($data['car_id']);
		}
		if (!empty($data['cartype']))
		{
			$data['car'] = $data['cartype'];
			unset($data['cartype']);
		}
		if (!empty($data['type']))
		{
			$types = Booking::getTypes();
			$data['type'] = $types[$data['type']];
		}
		ksort($data);
		return $data;
	}
}
