<?php

class User extends Admin {

	protected $guarded = ['id'];
	protected $table = 'users';

	public function groups()
	{
		return $this->belongsToMany('Group', 'users_groups');
	}

  public function partner()
  {
    return $this->belongsTo('Partner');
  }

  public function car()
  {
    return $this->belongsTo('Car');
  }

  public function booking_logs()
  {
    return $this->hasMany('BookingLog');
  }

  public function news()
  {
    return $this->hasMany('News');
  }

  public function logs()
  {
    return $this->hasMany('LogM');
  }

	public function delete()
	{
    if (count($this->booking_logs))
    {
      throw new TnExceptions\RelationRecordExistException('booking');
      return false;
    }

		$this->groups()->detach();
		return parent::delete();
	}

  public static function getLevel($groups)
  {
    $levels = [];
    foreach ($groups as $group)
    {
      $levels[] = (int) $group->level;
    }
    sort($levels);
    $levels = array_unique($levels);
    if(($key = array_search('0', $levels)) !== false)
    {
      unset($levels[$key]);
    }
    if (empty($levels))
    {
      return 0;
    }
    return reset($levels);
  }

  public static function getSelect($options = [])
  {
    $options['name'] = 'full_name';
    return parent::getSelect($options);
  }
}
