<?php

class PartnerType extends Admin {

	protected $guarded = ['id'];
	protected $table = 'partner_types';

	public function partners()
	{
		return $this->hasMany('Partner');
	}

	public function delete()
	{
		if (count($this->partners))
		{
			throw new TnExceptions\RelationRecordExistException('partner');
			return false;
		}

		return parent::delete();
	}

}
