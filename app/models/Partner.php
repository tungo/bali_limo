<?php

class Partner extends Admin {

	protected $guarded = ['id'];
	protected $table = 'partners';

  public function partner_type()
  {
    return $this->belongsTo('PartnerType');
  }

	public function bookings()
	{
		return $this->hasMany('Booking');
	}

	public function users()
	{
		return $this->hasMany('User');
	}

	public function annoying_clients()
	{
		return $this->hasMany('AnnoyingClient');
	}

	public function delete()
	{
		if (count($this->bookings))
		{
			throw new TnExceptions\RelationRecordExistException('booking');
			return false;
		}
		if (count($this->users))
		{
			throw new TnExceptions\RelationRecordExistException('user');
			return false;
		}
		if (count($this->annoying_clients))
		{
			throw new TnExceptions\RelationRecordExistException('annoying_client');
			return false;
		}

		return parent::delete();
	}

	public static function checkActive($id)
	{
		return static::where('id', '=', $id)->where('status', '=', 1)->count();
	}

}
