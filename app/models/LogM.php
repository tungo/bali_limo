<?php

class LogM extends Admin {

	protected $guarded = ['id'];
	protected $table = 'logs';

  public function user()
  {
    return $this->belongsTo('User');
  }

}
