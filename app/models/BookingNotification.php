<?php

class BookingNotification extends Admin {

	protected $guarded = ['id'];
	protected $table = 'booking_notifications';

	public function status()
	{
    return $this->belongsTo('BookingStatus', 'status_id');
	}
	public function user()
	{
    return $this->belongsTo('User');
	}
	public function partner()
	{
    return $this->belongsTo('Partner');
	}
	public function car()
	{
    return $this->belongsTo('Car');
	}

	public static function getList($type, $type_id = 0)
	{
		if ($type == Config::get('admin.prefix'))
		{
			return static::where('type', Config::get('user.type_id.admin'))->with('user')->take(Config::get('admin.notification.list'))->orderBy('created_at', 'desc')->get();
		}
		elseif ($type == Config::get('partner.prefix'))
		{
			return static::where('type', Config::get('user.type_id.partner'))->where('partner_id', $type_id)->with('user')->take(Config::get('admin.notification.list'))->orderBy('created_at', 'desc')->get();
		}
		elseif ($type == Config::get('car.prefix'))
		{
			return static::where('type', Config::get('user.type_id.car'))->where('car_id', $type_id)->with('user')->take(Config::get('admin.notification.list'))->orderBy('created_at', 'desc')->get();
		}
		else
		{
			return false;
		}
	}

}
