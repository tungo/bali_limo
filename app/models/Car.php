<?php

class Car extends Admin {

	protected $guarded = ['id'];
	protected $table = 'cars';

  public function cartype()
  {
    return $this->belongsTo('Cartype');
    // return $this->belongsTo('Cartype')->select(array('id', 'name'));
  }
	public function bookings()
	{
		return $this->hasMany('Booking');
	}

	public function users()
	{
		return $this->hasMany('User');
	}

	public function delete()
	{
		if (count($this->bookings))
		{
			throw new TnExceptions\RelationRecordExistException('booking');
			return false;
		}
		if (count($this->users))
		{
			throw new TnExceptions\RelationRecordExistException('user');
			return false;
		}

		return parent::delete();
	}

	public static function getSelect($options = array())
	{
		$query = static::select('id', 'number', 'cartype_id')->with('cartype');

		if (in_array('active', $options))
		{
			$query->where('status', '=', 1);
		}
		if (!in_array('no_position', $options))
		{
			$query->orderBy('position', 'asc');
		}

		$items = $query->get();
		$return = [];
		if (in_array('all', $options))
		{
			$return = array_add($return, '', '-- '.trans('tn.all').' --');
		}
		if (in_array('none', $options))
		{
			$return = array_add($return, 0, trans('tn.none'));
		}

		foreach ($items as $item)
		{
			if (count($item->cartype))
			{
				$return[$item->cartype->name][$item->id] = $item->number;
			}
			else
			{
				$return[trans('tn.unknown')][$item->id] = $item->number;
			}
		}
		return $return;
	}

	public static function getWorks($options = array())
	{
		$return = [];

		$cars_on_duty = [];
		$cars = self::select('cars.*')
			->leftJoin('bookings', 'cars.id', '=', 'bookings.car_id')
			->whereIn('bookings.status_id', [Config::get('booking.status_id.on_duty'), Config::get('booking.status_id.issue')])
			->where('cars.status', '=', 1)
			->orderBy('cars.position', 'asc')
			->get();
		foreach ($cars as $car)
		{
			$cars_on_duty[] = $car->id;
		}

		$cars = self::get();
		foreach ($cars as $car)
		{
			$car->on_duty = in_array($car->id, $cars_on_duty) ? true : false;
		}
		return $cars;
	}

	public static function getIdsByBookings($ids)
	{
		$return = [];
		$bookings = Booking::select('car_id')->whereIn('id', $ids)->get();
		foreach ($bookings as $booking)
		{
			$return[] = $booking->car_id;
		}
		return array_unique($return);
	}

	public static function getNumbers($options = array())
	{
		$items = static::select('id', 'number')->get();
		$return = [];
		foreach ($items as $item)
		{
			$return[$item->id] = $item->number;
		}
		return $return;
	}

	public static function checkActive($id)
	{
		return static::where('id', '=', $id)->where('status', '=', 1)->count();
	}

}
