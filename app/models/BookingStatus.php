<?php

class BookingStatus extends Admin {

	protected $guarded = ['id'];
	protected $table = 'booking_statuses';

	public function bookings()
	{
		return $this->hasMany('Booking', 'status_id');
	}
  public function logs()
  {
    return $this->hasMany('BookingLog', 'status_id');
  }
  public function notifications()
  {
    return $this->hasMany('BookingNotification', 'status_id');
  }

  public static function getDoneSelect($options = [])
  {
  	$select = parent::getSelect($options);
  	for ($i = 1; $i <= 8; $i++)
  	{
  		unset($select[$i]);
  	}
  	return $select;
  }

}
