<?php

class News extends Admin {

	protected $guarded = ['id'];
	protected $table = 'news';

  public function user()
  {
    return $this->belongsTo('User');
  }
  public function groups()
  {
    return $this->belongsToMany('Group', 'news_group');
  }

  public function delete()
  {
    $this->groups()->detach();
    return parent::delete();
  }

}
