<?php

class Driver extends Admin {

	protected $guarded = ['id'];
	protected $table = 'drivers';

	public function bookings()
	{
		return $this->hasMany('Booking');
	}

	public function delete()
	{
		if (count($this->bookings))
		{
			throw new TnExceptions\RelationRecordExistException('booking');
			return false;
		}

		return parent::delete();
	}

	public static function getSelect($options = array())
	{
    $options['name'] = 'full_name';
    return parent::getSelect($options);
	}

	public static function getWorks($options = array())
	{
		return self::where('status', '=', 1)->orderBy('position', 'asc')->get();
	}
}
