<?php

class Admin extends Eloquent {

	// tìm kiếm trong trang quản lý
	public static function scopeSearch($query, $column, $keyword = null)
	{
		if ($keyword) $query->where($column, '=', $keyword);
		return $query;
	}

	public static function scopeSearchLike($query, $column, $keyword = null)
	{
		if ($keyword) $query->where($column, 'LIKE', '%'.$keyword.'%');
		return $query;
	}

	public static function scopeSearchBetween($query, $column, $from = null, $to = null)
	{
		if ($from) $query->where($column, '>=', $from);
		if ($to) $query->where($column, '<=', $to);
		return $query;
	}

	public static function scopeSearchIn($query, $column, $array = null)
	{
		$array = is_array($array) ? array_filter($array) : [];
		if (!empty($array)) $query->whereIn($column, $array);
		return $query;
	}

	public static function scopeSearchMany($query, $columns, $keyword = null)
	{

	}

	// thao tác
	public function enable()
	{
		$this->status = 1;
		return $this->update();
	}

	public function disable()
	{
		$this->status = 0;
		return $this->update();
	}


	public static function getSelect($options = [])
	{
		$id = isset($options['id']) ? $options['id']: 'id';
		$name = isset($options['name']) ? $options['name']: 'name';

		$query = static::select($id, $name);

		if (in_array('active', $options))
		{
			$query->where('status', '=', 1);
		}
		if (!in_array('no_position', $options))
		{
			$query->orderBy('position', 'asc');
		}

		$items = $query->get();
		$return = [];
		if (in_array('all', $options))
		{
			$return = array_add($return, '', '-- '.trans('tn.all').' --');
		}
		if (in_array('none', $options))
		{
			$return = array_add($return, 0, trans('tn.none'));
		}

		foreach ($items as $item)
		{
			$return[$item->{$id}] = $item->{$name};
		}
		return $return;
	}

}
