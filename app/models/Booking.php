<?php

class Booking extends Admin {

	protected $guarded = ['id'];
	protected $table = 'bookings';

  public function status()
  {
    return $this->belongsTo('BookingStatus', 'status_id');
  }
  public function partner()
  {
    return $this->belongsTo('Partner');
  }
  public function car()
  {
    return $this->belongsTo('Car');
  }
  public function driver()
  {
    return $this->belongsTo('Driver');
  }
  public function logs()
  {
    return $this->hasMany('BookingLog');
  }

  public static function getTypes($options = array())
  {
    $return = [];
    if (in_array('all', $options))
    {
      $return = array_add($return, '', '-- '.trans('tn.all').' --');
    }
    if (in_array('none', $options))
    {
      $return = array_add($return, 0, trans('tn.none'));
    }
    foreach (Config::get('booking.types') as $key => $value)
    {
      $return[$key] = trans('tn.'.$value);
    }
    return $return;
  }

  public function updateStatus($status)
  {
    $this->status_id = Config::get('booking.status_id.'.$status);
    return $this->update();
  }

  public static function logUpdateStatus($ids, $status_id, $log_data = [], $options = [])
  {
    $logs = [];
    $items = [];
    $user = Sentry::getUser();

    // tách log data riêng của các item ra khỏi data chung
    if (isset($log_data['items']))
    {
      $items = $log_data['items'];
      unset($log_data['items']);
    }

    $general = [
      'status_id'  => $status_id,
      'user_id'    => $user->id,
      'created_at' => date('Y-m-d H:i:s'),
    ];

    foreach ($ids as $id)
    {
      $item_data = !empty($items[$id]) ? array_merge($items[$id], $log_data) : $log_data;
      $item = [
        'booking_id' => $id,
        'data'       => serialize($item_data),
      ];
      $log_item = array_merge($item, $general);

      if (!empty($item_data['status_id']))
      {
        $log_item['status_id'] = $item_data['status_id'];
      }

      $logs[] = $log_item;
    }

    if (!empty($logs))
    {
      DB::table('booking_logs')->insert($logs);
    }
  }

  public static function notificationUpdateStatus($ids, $status_id, $type, $type_id = 0, $noti_data = [], $options = [])
  {
    $user = Sentry::getUser();

    $data = [
      'type'        => $type,
      'booking_ids' => implode(',', $ids),
      'status_id'   => $status_id,
      'user_id'     => $user->id,
      'created_at'  => date('Y-m-d H:i:s'),
      'data'        => serialize($noti_data),
    ];

    if ($type == 2)
    {
      $data['partner_id'] = $type_id;
    }
    elseif ($type == 3)
    {
      $data['car_id'] = $type_id;
    }

    DB::table('booking_notifications')->insert($data);
  }

  public static function insertData($id, $type, $data = [])
  {
    if (!is_array($data))
    {
      return false;
    }

    $booking_data = [
      'booking_id' => $id,
      'type'       => $type,
      'data'       => serialize($data),
    ];
    return DB::table('booking_data')->insert($booking_data);
  }

  public function getData()
  {
    $data = [];
    $result = DB::table('booking_data')->where('booking_id', $this->id)->get();
    foreach ($result as $row)
    {
      $data[$row->type] = unserialize($row->data);
    }
    $this->data = $data;
    return $data;
  }

  public static function calcReportChiTiet($items)
  {
    $total = 0;
    foreach ($items as $item)
    {
      $total += $item->total;
    }
    $vat = $total * 10 / 100;
    $report = [
      'total' => $total,
      'vat'   => $vat,
    ];
    return $report;
  }
  public static function calcReportKeToan($items)
  {
    $report = [];
    $total = 0;
    foreach ($items as $item)
    {
      if (!isset($report['rows'][$item->partner_id]))
      {
        $report['rows'][$item->partner_id] = $item->total;
      }
      else
      {
        $report['rows'][$item->partner_id] += $item->total;
      }
      $total += $item->total;
    }
    $report['total'] = $total;
    return $report;
  }
  public static function calcReportXe($items)
  {
    $report = [];
    $total = 0;
    $count = 0;
    foreach ($items as $item)
    {
      if (!isset($report['rows'][$item->car_id]))
      {
        $report['rows'][$item->car_id] = [
          'count' => 1,
          'total' => $item->total,
        ];
      }
      else
      {
        $report['rows'][$item->car_id]['count']++;
        $report['rows'][$item->car_id]['total'] += $item->total;
      }
      $total += $item->total;
      $count++;
    }
    $report['total'] = $total;
    $report['count'] = $count;
    return $report;
  }
  public static function calcReportDoanhThu($items)
  {
    $report['rows'] = [
      'week_1' => 0,
      'week_2' => 0,
      'week_3' => 0,
      'week_4' => 0,
    ];
    $total = 0;
    $last_day = 0;
    foreach ($items as $item)
    {
      $day = date('d', strtotime($item->done_at));
      $last_day = date('t', strtotime($item->done_at));
      if ($day >= 1 && $day <= 7)
      {
        $report['rows']['week_1'] += $item->total;
      }
      elseif ($day >=8 && $day <= 14)
      {
        $report['rows']['week_2'] += $item->total;
      }
      elseif ($day >=15 && $day <= 21)
      {
        $report['rows']['week_3'] += $item->total;
      }
      elseif ($day >=22 && $day <= $last_day)
      {
        $report['rows']['week_4'] += $item->total;
      }
      $total += $item->total;
    }
    $report['last_day'] = $last_day;
    $report['total'] = $total;
    return $report;
  }

  public static function calcStatistic($items)
  {
    $statistic = [
      'total'          => $items->sum('total'),
      'count'          => $items->count(),
      'drop_off'       => 0,
      'pick_up'        => 0,
      'other'          => 0,
      'cancel'         => 0,
      'no_show'        => 0,
      'completed'      => 0,
      'free_of_charge' => 0,
    ];
    foreach ($items as $item)
    {
      switch ($item->type)
      {
        case Config::get('booking.types_code.p'):
          $statistic['pick_up']++;
          break;
        case Config::get('booking.types_code.d'):
          $statistic['drop_off']++;
          break;
        case Config::get('booking.types_code.o'):
          $statistic['other']++;
          break;
      }

      switch ($item->status_id)
      {
        case Config::get('booking.status_id.cancel'):
          $statistic['cancel']++;
          break;
        case Config::get('booking.status_id.no_show'):
          $statistic['no_show']++;
          break;
        case Config::get('booking.status_id.completed'):
          $statistic['completed']++;
          break;
        case Config::get('booking.status_id.free_of_charge'):
          $statistic['free_of_charge']++;
          break;
      }
    }
    return $statistic;
  }
  public static function calcReportThongKe($items)
  {
    $total = 0;
    foreach ($items as $item)
    {
      $total += $item->total;
    }
    $vat = $total * 10 / 100;
    $report = [
      'total' => $total,
      'vat'   => $vat,
    ];
    return $report;
  }
}
