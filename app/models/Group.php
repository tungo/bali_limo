<?php

class Group extends Admin {

	protected $guarded = ['id'];
	protected $table = 'groups';

	public function users()
	{
		return $this->belongsToMany('User', 'users_groups');
	}

	public function news()
	{
		return $this->belongsToMany('News', 'news_group');
	}

	public function delete()
	{
		$this->users()->detach();
		$this->news()->detach();
		return parent::delete();
	}

	public static function getGroupsByLevel($level, $operator = '>')
	{
		return static::select()->where('level', '<>', '0')->where('level', $operator, $level)->get();
	}
}
