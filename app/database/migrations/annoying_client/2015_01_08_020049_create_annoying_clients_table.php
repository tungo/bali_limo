<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnoyingClientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('annoying_clients', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('customer');
			$table->integer('partner_id')->unsigned();
			$table->boolean('status');
			$table->integer('position');
			$table->integer('created_by')->unsigned()
			$table->integer('updated_by')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('annoying_clients');
	}

}
