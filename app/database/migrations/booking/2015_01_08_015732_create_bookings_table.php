<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bookings', function(Blueprint $table)
		{
			$table->increments('id');

			$table->date('date');
			$table->time('time');

			$table->integer('partner_id')->unsigned();

			$table->string('customer_name');
			$table->string('customer_email');
			$table->string('customer_phone');
			$table->integer('customer_amount');

			$table->tinyInteger('type');

			$table->string('flight');
			$table->date('flight_date');
			$table->time('flight_time');

			$table->string('cartype');
			$table->text('remark');

			$table->integer('status_id')->unsigned()->default(1);

			$table->integer('driver_id')->unsigned();
			$table->string('car_code');
			$table->integer('car_id')->unsigned();
			$table->text('note');

			$table->decimal('total', 12, 2);

			$table->string('conform_no');

			$table->timestamp('done_at');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bookings');
	}

}
