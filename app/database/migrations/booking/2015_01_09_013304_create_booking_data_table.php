<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking_data', function(Blueprint $table)
		{
			$table->integer('booking_id')->unsigned();
			$table->string('type');
			$table->text('data');
			$table->primary(['booking_id', 'type'], $name = 'booking_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking_data');
	}

}
