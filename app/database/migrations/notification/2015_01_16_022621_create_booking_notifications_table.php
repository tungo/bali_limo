<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking_notifications', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('type');
			$table->integer('status_id')->unsigned();
			$table->integer('partner_id')->unsigned();
			$table->integer('car_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->text('data');
			$table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking_notifications');
	}

}
