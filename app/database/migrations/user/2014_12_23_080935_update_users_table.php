<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table) {
			$table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned();
			$table->integer('partner_id')->unsigned();
			$table->integer('car_id')->unsigned();
			$table->integer('type');
			$table->boolean('status');
			$table->integer('position');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
    	$table->dropColumn([
    		'created_by',
    		'updated_by',
    		'partner_id',
    		'type',
    		'status',
    		'position',
    	]);
		});
	}

}
