<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('groups', function(Blueprint $table) {
			$table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned();
			$table->boolean('status');
			$table->integer('position');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('groups', function(Blueprint $table)
		{
    	$table->dropColumn([
    		'created_by',
    		'updated_by',
    		'status',
    		'position',
    	]);
		});
	}

}
