<?php

class CarController extends BackendController {

	public $user_car_id = 0;

	public function __construct()
	{
		parent::__construct();

		$this->beforeFilter('@auth');
		$this->beforeFilter('@account_type');
		$this->beforeFilter('@permission');
		$this->afterFilter('@log');

		$this->beforeFilter('csrf', ['on' => 'post', 'except' => ['postSign', 'postReview']]);

		$this->data['prefix'] = $this->prefix = Config::get('car.prefix');

		parent::_init();

		if (!empty($this->logged_in_user))
		{
			$this->user_car_id = $this->data['user_car_id'] = $this->logged_in_user->car_id;
		}
		$this->data['notification_allowed'] = true;
	}

	// kiểm tra loại tk sau khi đăng nhập
	public function account_type($route, $request)
	{
		if (Sentry::check())
		{
			$user = Sentry::getUser();
			if (empty($user->car_id) || $user->type != Config::get('user.type_id.car'))
			{
				return $this->view404();
			}
			if ( ! Car::checkActive($user->car_id))
			{
				return $this->view404();
			}
		}
	}

}
