<?php

class AdminBookingController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = 'Booking';
		$this->data['main_route'] = $this->main_route = 'booking';
		$this->data['page_title'] = trans('tn.booking');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.booking')];
	}

	public function getIndex()
	{
		// filter
		$filter = [
			'date_from',
			'date_to',
			'time_from',
			'time_to',
			'partner_id',
			'customer_name',
			'customer_amount',
			'type',
			'flight',
			'cartype',
			'status_id',
			'driver_id',
			'car_id',
			'total_from',
			'total_to',

			'ids',
		];
		$columns = [
			'id'              => 'bookings.id',
			'date'            => 'bookings.date',
			'time'            => 'bookings.time',
			'partner'         => 'partners.name',
			'customer_name'   => 'bookings.customer_name',
			'customer_amount' => 'bookings.customer_amount',
			'type'            => 'bookings.type',
			'flight'          => 'bookings.flight',
			'cartype'         => 'bookings.cartype',
			'remark'          => 'bookings.remark',
			'status'          => 'booking_statuses.name',
			'driver'          => 'drivers.full_name',
			'car_code'        => 'bookings.car_code',
			'car'             => 'cars.number',
			'total'           => 'bookings.total',
		];
		$this->data['filter'] = $this->initFilter($filter, $columns, ['except' => ['ids'], 'default_sort' => 'date']);
		extract($this->data['filter']);
		// end filter

		$this->data['types'] = Booking::getTypes(['all']);
		$this->data['partners'] = Partner::getSelect(['all']);
		$this->data['cars'] = Car::getSelect(['all']);
		$this->data['drivers'] = Driver::getSelect(['all']);
		$this->data['statuses'] = BookingStatus::getSelect(['all']);

		$this->data['items'] = Booking::select('bookings.*')
			->leftJoin('partners', 'bookings.partner_id', '=', 'partners.id')
			->leftJoin('drivers', 'bookings.driver_id', '=', 'drivers.id')
			->leftJoin('cars', 'bookings.car_id', '=', 'cars.id')
			->leftJoin('booking_statuses', 'bookings.status_id', '=', 'booking_statuses.id')
			->with('partner')
			->with('driver')
			->with('car')
			->with('status')
			->searchBetween('bookings.date', fn_datetime($date_from, 'd-m-Y', 'Y-m-d', null), fn_datetime($date_to, 'd-m-Y', 'Y-m-d', null))
			->searchBetween('bookings.time', fn_datetime($time_from, 'H:i', 'H:i:s', null), fn_datetime($time_to, 'H:i', 'H:i:s', null))
			->search('bookings.partner_id', $partner_id)
			->searchLike('bookings.customer_name', $customer_name)
			->search('bookings.customer_amount', $customer_amount)
			->search('bookings.type', $type)
			->searchLike('bookings.flight', $flight)
			->searchLike('bookings.cartype', $cartype)
			->search('bookings.status_id', $status_id)
			->search('bookings.driver_id', $driver_id)
			->search('bookings.car_id', $car_id)
			->searchBetween('bookings.total', $total_from, $total_to)
			->searchIn('bookings.id', explode(',', $ids))
			->orderBy($columns[$sort], $order)
			->orderBy('bookings.time', 'asc')
			->paginate($per_page);

		$this->data['breadcrumb'][] = trans('tn.manage');
		return View::make($this->prefix.'.'.$this->main_route.'.index', $this->data);
	}

	public function getAdd()
	{
		$this->data['types'] = Booking::getTypes();
		$this->data['partners'] = Partner::getSelect(['active']);

		fn_url_put(URL::previous());

		$this->data['breadcrumb'][] = trans('tn.add');
		return View::make($this->prefix.'.'.$this->main_route.'.add', $this->data);
	}

	public function postAdd()
	{
		$hid = Input::has('hid') ? Input::get('hid') : [];
		$items = Input::has('items') ? Input::get('items') : [];
		$i = 0;
		$ids = [];

		foreach ($hid as $key => $value)
		{
			$item = new Booking();

			$item->date            = fn_datetime($items['date'][$key], 'd-m-Y', 'Y-m-d');
			$item->time            = fn_datetime($items['time'][$key], 'H:i', 'H:i:s');
			$item->partner_id      = $items['partner_id'][$key];
			$item->customer_name   = $items['customer_name'][$key];
			$item->customer_amount = $items['customer_amount'][$key];
			$item->type            = $items['type'][$key];
			$item->flight          = $items['flight'][$key];
			$item->cartype         = $items['cartype'][$key];
			$item->remark          = $items['remark'][$key];
			$item->status_id       = Config::get('booking.status_id.approved');

			if (!$item->save())
			{
				break;
			}

			$ids[] = $item->id;
			$i++;
		}
		if ($i > 0 && $i == count($hid))
		{
			Notification::success(trans('tn.succeeded'));

			$this->logUpdateStatus($ids, 'approved');
			$this->notificationUpdateStatus($ids, 'approved');
		}
		else
		{
			Booking::destroy($ids);
			Notification::error(trans('tn.failed'));
		}
		$redirect = fn_url_get(URL::route($this->prefix.'.'.$this->main_route));
		return Redirect::to($redirect);
	}

	public function getImport()
	{
		$this->data['partners'] = Partner::getSelect(['active']);

		fn_url_put(URL::previous());

		$this->data['breadcrumb'][] = trans('tn.import');
		return View::make($this->prefix.'.'.$this->main_route.'.import', $this->data);
	}

	public function postImport()
	{
		$rules = [
			'partner_id' => 'required',
			'excel'      => 'required|mimes:xls,xlsx',
		];
		$attributes = [
			'partner_id' => trans('tn.partner'),
			'excel'      => trans('tn.excel'),
		];
		$validator = Validator::make(Input::all(), $rules);
		$validator->setAttributeNames($attributes);

		if ($validator->fails())
		{
			$messages = $validator->messages();
			$error = $this->getValidatorError($messages);
			Notification::error($error);
			return Redirect::to(URL::current());
		}

		$excel = Input::file('excel');
		$file_name = $excel->getClientOriginalName();
		$file_location = storage_path('files/excel');
		$excel->move($file_location, $file_name);

		$rows = Excel::selectSheetsByIndex(0)->load($file_location.'/'.$file_name)->get();

		$types = Config::get('booking.types_code');

		$i = 0;
		$ids = [];
		$row_count = $rows->count();

		foreach ($rows as $row)
		{
			if (empty($row->date) || empty($row->customer) || empty($row->type) || empty($row->time))
			{
				$row_count--;
				continue;
			}

			$row_amount = !empty($row->amount) ? $row->amount : '';
			$row_flight = !empty($row->flight) ? $row->flight : '';
			$row_car    = !empty($row->car) ? $row->car : '';
			$row_remark = !empty($row->remark) ? $row->remark : '';

			$item = new Booking();

			$item->date            = fn_datetime(str_pad($row->date, 6, "0", STR_PAD_LEFT), 'dmy', 'Y-m-d');
			$item->time            = fn_datetime(str_pad($row->time, 4, "0", STR_PAD_LEFT), 'Hi', 'H:i:s');
			$item->partner_id      = Input::get('partner_id');
			$item->customer_name   = $row->customer;
			$item->customer_amount = $row_amount;
			$item->type            = $types[strtolower($row->type)];
			$item->flight          = $row_flight;
			$item->cartype         = $row_car;
			$item->remark          = $row_remark;
			$item->status_id       = Config::get('booking.status_id.approved');

			try
			{
				$item->save();
				$ids[] = $item->id;
				$i++;
			}
			catch (Exception $e)
			{
				Notification::error(trans('tn.excel_is_incorrect'));
				break;
			}
		}
		if ($i > 0 && $i == $row_count && !empty($ids))
		{
			Notification::success(trans('tn.succeeded'));

			$this->logUpdateStatus($ids, 'approved');
			$this->notificationUpdateStatus($ids, 'approved');
		}
		else
		{
			if (!empty($ids))
			{
				Booking::destroy($ids);
			}
			Notification::error(trans('tn.failed'));
		}
		$redirect = fn_url_get(URL::route($this->prefix.'.'.$this->main_route));
		return Redirect::to($redirect);
	}

	public function getView($id)
	{
		try
		{
			$this->data['item'] = Booking::with('partner')->with('driver')->with('car')->with('status')->with('logs')->findOrFail($id);
			$this->data['types'] = Booking::getTypes();
			$this->data['item']->getData();

			$file_location = public_path('files/signature');
			$this->data['signature_path'] = public_path('files/signature');
			$this->data['signature_url'] =  FILES_URL.'/signature';
			$this->data['customer_signature'] =  $id.'_customer.png';
			$this->data['partner_signature'] =  $id.'_partner.png';

			$this->data['breadcrumb'][] = trans('tn.view');
			return View::make($this->prefix.'.'.$this->main_route.'.view', $this->data);
		}
		catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function getEdit($id = null)
	{
		try
		{
			$this->checkUpdateStatus('edit');
		}
		catch (TnExceptions\InfoNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			return Redirect::to(URL::previous());
		}
		catch (TnExceptions\UpdateStatusException $e)
		{
			Notification::error(trans('tn.items_unable_to_update').': '.$e->getMessage());
			return Redirect::to(URL::previous());
		}

		$ids = Input::get('cid');
		$this->data['items'] = Booking::whereIn('id', $ids)
			->orderBy('date', 'desc')
			->orderBy('time', 'asc')
			->get();

		$this->data['types'] = Booking::getTypes();
		$this->data['partners'] = Partner::getSelect(['active']);

		fn_url_put(URL::previous());

		$this->data['breadcrumb'][] = trans('tn.edit');
		return View::make($this->prefix.'.'.$this->main_route.'.edit', $this->data);
	}

	public function postEdit($id = null)
	{
		$hid = Input::has('hid') ? Input::get('hid') : [];
		$items = Input::has('items') ? Input::get('items') : [];
		$i = 0;
		$error_items = [];
		foreach ($hid as $key => $value)
		{
			try
			{
				$item = Booking::findOrFail($value);

				$item->date            = fn_datetime($items['date'][$key], 'd-m-Y', 'Y-m-d');
				$item->time            = fn_datetime($items['time'][$key], 'H:i', 'H:i:s');
				$item->partner_id      = $items['partner_id'][$key];
				$item->customer_name   = $items['customer_name'][$key];
				$item->customer_amount = $items['customer_amount'][$key];
				$item->type            = $items['type'][$key];
				$item->flight          = $items['flight'][$key];
				$item->cartype         = $items['cartype'][$key];
				$item->remark          = $items['remark'][$key];

				if (!$item->save())
				{
					$error_items[] = $value;
				}
				else
				{
					$ids[] = $value;
					$log_data['items'][$value] = [
						'progress'				=> 'edit',
						'date'            => $item->date,
						'time'            => $item->time,
						'partner_id'      => $item->partner_id,
						'customer_name'   => $item->customer_name,
						'customer_amount' => $item->customer_amount,
						'type'            => $item->type,
						'flight'          => $item->flight,
						'cartype'         => $item->cartype,
						'remark'          => $item->remark,
					];
				}
			}
			catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
			{
				$error_items[] = $value;
			}
		}
		if (empty($error_items))
		{
			Notification::success(trans('tn.succeeded'));

			Booking::logUpdateStatus($ids, 0, $log_data);
		}
		else
		{
			Notification::error(trans('tn.failed_to_update_items').': '.implode(', ', $error_items));
		}

		$redirect = fn_url_get(URL::route($this->prefix.'.'.$this->main_route));
		return Redirect::to($redirect);
	}

	public function postDelete()
	{
		try
		{
			$this->checkUpdateStatus('delete');
		}
		catch (TnExceptions\InfoNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			return Redirect::to(URL::previous());
		}
		catch (TnExceptions\UpdateStatusException $e)
		{
			Notification::error(trans('tn.items_unable_to_update').': '.$e->getMessage());
			return Redirect::to(URL::previous());
		}

		$ids = Input::get('cid');
		if (Booking::destroy($ids))
		{
			Notification::success(trans('tn.succeeded'));
		}
		else
		{
			Notification::error(trans('tn.failed'));
		}
		return Redirect::to(URL::previous());
	}

	public function getUpdate($id = null)
	{
		try
		{
			$this->checkUpdateStatus('update');
		}
		catch (TnExceptions\InfoNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			return Redirect::to(URL::previous());
		}
		catch (TnExceptions\UpdateStatusException $e)
		{
			Notification::error(trans('tn.items_unable_to_update').': '.$e->getMessage());
			return Redirect::to(URL::previous());
		}

		$ids = Input::get('cid');
		$this->data['items'] = Booking::whereIn('bookings.id', $ids)
			->with('partner')
			->with('driver')
			->with('car')
			->with('status')
			->orderBy('bookings.date', 'desc')
			->orderBy('bookings.time', 'asc')
			->get();

		$this->data['types'] = Booking::getTypes();
		$this->data['partners'] = Partner::getSelect(['active']);
		$this->data['car_codes'] = Cartype::getCodesSelect(['active']);
		$this->data['cars'] = Car::getSelect(['active', 'none']);
		$this->data['drivers'] = Driver::getSelect(['active', 'none']);
		$this->data['statuses'] = BookingStatus::getSelect();

		fn_url_put(URL::previous());

		$this->data['breadcrumb'][] = trans('tn.update');
		return View::make($this->prefix.'.'.$this->main_route.'.update', $this->data);
	}

	public function postUpdate($id = null)
	{
		$hid = Input::has('hid') ? Input::get('hid') : [];
		$items = Input::has('items') ? Input::get('items') : [];
		$i = 0;
		$error_items = [];
		foreach ($hid as $key => $value)
		{
			try
			{
				$item = Booking::findOrFail($value);

				$item->date            = fn_datetime($items['date'][$key], 'd-m-Y', 'Y-m-d');
				$item->time            = fn_datetime($items['time'][$key], 'H:i', 'H:i:s');
				$item->partner_id      = $items['partner_id'][$key];
				$item->customer_name   = $items['customer_name'][$key];
				$item->customer_amount = $items['customer_amount'][$key];
				$item->type            = $items['type'][$key];
				$item->flight          = $items['flight'][$key];
				$item->cartype         = $items['cartype'][$key];
				$item->remark          = $items['remark'][$key];
				$item->status_id       = $items['status_id'][$key];
				$item->driver_id       = $items['driver_id'][$key];
				$item->car_code        = $items['car_code'][$key];
				$item->car_id          = $items['car_id'][$key];
				$item->note            = $items['note'][$key];
				$item->total           = $items['total'][$key];
				$item->done_at         = fn_datetime($items['done_at'][$key], 'd-m-Y H:i', 'Y-m-d H:i:s');;

				if (!$item->save())
				{
					$error_items[] = $value;
				}
				else
				{
					$ids[] = $value;
					$log_data['items'][$value] = [
						'progress'  			=> 'update',
						'date'            => $item->date,
						'time'            => $item->time,
						'partner_id'      => $item->partner_id,
						'customer_name'   => $item->customer_name,
						'customer_amount' => $item->customer_amount,
						'type'            => $item->type,
						'flight'          => $item->flight,
						'cartype'         => $item->cartype,
						'remark'          => $item->remark,
						'status_id'       => $item->status_id,
						'driver_id'       => $item->driver_id,
						'car_code'        => $item->car_code,
						'car_id'          => $item->car_id,
						'note'            => $item->note,
						'total'           => $item->total,
						'done_at'         => $item->done_at,
					];
				}
			}
			catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
			{
				$error_items[] = $value;
			}
		}
		if (empty($error_items))
		{
			Notification::success(trans('tn.succeeded'));

			Booking::logUpdateStatus($ids, 0, $log_data);
		}
		else
		{
			Notification::error(trans('tn.failed_to_update_items').': '.implode(', ', $error_items));
		}

		$redirect = fn_url_get(URL::route($this->prefix.'.'.$this->main_route));
		return Redirect::to($redirect);
	}

	public function getNote($id = null)
	{
		try
		{
			$this->checkUpdateStatus('update');
		}
		catch (TnExceptions\InfoNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			return Redirect::to(URL::previous());
		}
		catch (TnExceptions\UpdateStatusException $e)
		{
			Notification::error(trans('tn.items_unable_to_update').': '.$e->getMessage());
			return Redirect::to(URL::previous());
		}

		$ids = Input::get('cid');
		$this->data['items'] = Booking::whereIn('bookings.id', $ids)
			->with('partner')
			->with('driver')
			->with('car')
			->with('status')
			->orderBy('bookings.date', 'desc')
			->orderBy('bookings.time', 'asc')
			->get();

		$this->data['types'] = Booking::getTypes();
		$this->data['partners'] = Partner::getSelect(['active']);
		$this->data['car_codes'] = Cartype::getCodesSelect(['active']);
		$this->data['cars'] = Car::getSelect(['active', 'none']);
		$this->data['drivers'] = Driver::getSelect(['active', 'none']);
		$this->data['statuses'] = BookingStatus::getSelect();

		fn_url_put(URL::previous());

		$this->data['breadcrumb'][] = trans('tn.note');
		return View::make($this->prefix.'.'.$this->main_route.'.note', $this->data);
	}

	public function postNote($id = null)
	{
		$hid = Input::has('hid') ? Input::get('hid') : [];
		$items = Input::has('items') ? Input::get('items') : [];
		$i = 0;
		$error_items = [];
		foreach ($hid as $key => $value)
		{
			try
			{
				$item = Booking::findOrFail($value);

				$item->note = $items['note'][$key];

				if (!$item->save())
				{
					$error_items[] = $value;
				}
				else
				{
					$ids[] = $value;
					$log_data['items'][$value] = [
						'progress'  			=> 'note',
						'note'            => $item->note,
					];
				}
			}
			catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
			{
				$error_items[] = $value;
			}
		}
		if (empty($error_items))
		{
			Notification::success(trans('tn.succeeded'));

			Booking::logUpdateStatus($ids, 0, $log_data);
		}
		else
		{
			Notification::error(trans('tn.failed_to_update_items').': '.implode(', ', $error_items));
		}

		$redirect = fn_url_get(URL::route($this->prefix.'.'.$this->main_route));
		return Redirect::to($redirect);
	}

	public function getPrint($id)
	{
		try
		{
			$this->data['item'] = Booking::with('partner')->with('driver')->with('car')->with('status')->with('logs')->findOrFail($id);
			$this->data['types'] = Booking::getTypes();
			$this->data['item']->getData();

			$file_location = public_path('files/signature');
			$this->data['signature_path'] = public_path('files/signature');
			$this->data['signature_url'] =  FILES_URL.'/signature';
			$this->data['customer_signature'] =  $id.'_customer.png';
			$this->data['partner_signature'] =  $id.'_partner.png';

			$this->data['breadcrumb'][] = trans('tn.print');
			$this->data['layout'] = $this->data['theme'].'.blank';
			return View::make($this->prefix.'.'.$this->main_route.'.print', $this->data);
		}
		catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function postApproved()
	{
		return $this->updateStatus('approved');
	}

	public function postNotApproved()
	{
		return $this->updateStatus('not_approved');
	}

	public function getCarRequested()
	{
		try
		{
			$this->checkUpdateStatus('car_requested');
		}
		catch (TnExceptions\InfoNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			return Redirect::to(URL::previous());
		}
		catch (TnExceptions\UpdateStatusException $e)
		{
			Notification::error(trans('tn.items_unable_to_update').': '.$e->getMessage());
			return Redirect::to(URL::previous());
		}

		$ids = Input::get('cid');
		$this->data['items'] = Booking::whereIn('id', $ids)
			->with('partner')
			->with('driver')
			->with('car')
			->with('status')
			->orderBy('date', 'desc')
			->orderBy('time', 'asc')
			->get();

		$this->data['types'] = Booking::getTypes();
		$this->data['partners'] = Partner::getSelect(['active']);

		$this->data['car_codes'] = Cartype::getCodesSelect(['active']);
		$this->data['cars'] = Car::getSelect(['active']);
		$this->data['drivers'] = Driver::getSelect(['active']);

		fn_url_put(URL::previous());

		$this->data['breadcrumb'][] = trans('tn.car_request');
		return View::make($this->prefix.'.'.$this->main_route.'.car_request', $this->data);
	}

	public function postCarRequested()
	{
		$hid = Input::has('hid') ? Input::get('hid') : [];
		$items = Input::has('items') ? Input::get('items') : [];
		$i = 0;
		$error_items = [];
		$ids = [];
		$log_data = [];

		foreach ($hid as $key => $value)
		{
			try
			{
				$item = Booking::findOrFail($value);

				$item->driver_id = $items['driver_id'][$key];
				$item->car_code  = $items['car_code'][$key];
				$item->car_id    = $items['car_id'][$key];
				$item->note      = $items['note'][$key];
				$item->total     = $items['total'][$key];
				$item->status_id = Config::get('booking.status_id.car_requested');

				if (!$item->save())
				{
					$error_items[] = $value;
				}
				else
				{
					$ids[] = $value;
					$log_data['items'][$value] = [
						'driver_id' => $item->driver_id,
						'car_code'  => $item->car_code,
						'car_id'    => $item->car_id,
						'note'      => $item->note,
						'total'     => $item->total,
					];
				}
			}
			catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
			{
				$error_items[] = $value;
			}
		}
		if (empty($error_items))
		{
			Notification::success(trans('tn.succeeded'));

			$this->logUpdateStatus($ids, 'car_requested', $log_data);
			$this->notificationUpdateStatus($ids, 'car_requested');
		}
		else
		{
			Notification::error(trans('tn.failed_to_update_items').': '.implode(', ', $error_items));
		}

		$redirect = fn_url_get(URL::route($this->prefix.'.'.$this->main_route));
		return Redirect::to($redirect);
	}

	public function postOnDuty()
	{
		return $this->updateStatus('on_duty');
	}

	public function postIssue()
	{
		return $this->updateStatus('issue');
	}

	public function postFinished()
	{
		return $this->updateStatus('finished');
	}

	public function postCancel()
	{
		return $this->updateStatus('cancel', true);
	}

	public function getNoShow()
	{
		try
		{
			$this->checkUpdateStatus('no_show');
		}
		catch (TnExceptions\InfoNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			return Redirect::to(URL::previous());
		}
		catch (TnExceptions\UpdateStatusException $e)
		{
			Notification::error(trans('tn.items_unable_to_update').': '.$e->getMessage());
			return Redirect::to(URL::previous());
		}

		$ids = Input::get('cid');
		$this->data['items'] = 	Booking::whereIn('id', $ids)
			->with('partner')
			->with('driver')
			->with('car')
			->with('status')
			->orderBy('date', 'desc')
			->orderBy('time', 'asc')
			->get();

		$this->data['types'] = Booking::getTypes();
		$this->data['partners'] = Partner::getSelect(['active']);

		$this->data['car_codes'] = Cartype::getCodesSelect(['active']);
		$this->data['cars'] = Car::getSelect(['active']);
		$this->data['drivers'] = Driver::getSelect(['active']);

		fn_url_put(URL::previous());

		$this->data['breadcrumb'][] = trans('tn.no_show');
		return View::make($this->prefix.'.'.$this->main_route.'.no_show', $this->data);
	}

	public function postNoShow()
	{
		$hid = Input::has('hid') ? Input::get('hid') : [];
		$items = Input::has('items') ? Input::get('items') : [];
		$i = 0;
		$error_items = [];
		$ids = [];
		$log_data = [];

		foreach ($hid as $key => $value)
		{
			try
			{
				$item = Booking::findOrFail($value);

				$item->total     = $items['total'][$key];
				$item->status_id = Config::get('booking.status_id.no_show');
				$item->done_at   = date('Y-m-d H:i:s');

				if (!$item->save())
				{
					$error_items[] = $value;
				}
				else
				{
					$ids[] = $value;
					$log_data['items'][$value] = [
						'total'  => $item->total,
						'reason' => $items['reason'][$key],
					];
				}
			}
			catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
			{
				$error_items[] = $value;
			}
		}
		if (empty($error_items))
		{
			Notification::success(trans('tn.succeeded'));

			$this->logUpdateStatus($ids, 'no_show', $log_data);
			$this->notificationUpdateStatus($ids, 'no_show');
		}
		else
		{
			Notification::error(trans('tn.failed_to_update_items').': '.implode(', ', $error_items));
		}

		$redirect = fn_url_get(URL::route($this->prefix.'.'.$this->main_route));
		return Redirect::to($redirect);
	}

	// public function postNoShow()
	// {
	// 	return $this->updateStatus('no_show', true);
	// }

	public function postCompleted()
	{
		$redirect = Input::get('redirect') == 'manage' ? URL::route($this->prefix.'.'.$this->main_route) : false;
		return $this->updateStatus('completed', false, $redirect);
	}

	public function postFreeOfCharge()
	{
		$redirect = Input::get('redirect') == 'manage' ? URL::route($this->prefix.'.'.$this->main_route) : false;
		return $this->updateStatus('free_of_charge', false, $redirect);
	}

	// ----- ----- ----- ----- -----

	public function checkUpdateStatus($status)
	{
		$ids = Input::has('cid') ? Input::get('cid') : [];
		if (!is_array($ids) || !count($ids))
		{
			throw new TnExceptions\InfoNotFoundException('cid');
			return false;
		}

		$this->data['items'] = Booking::whereIn('id', $ids)->get();
		if (!$this->data['items']->count())
		{
			throw new TnExceptions\InfoNotFoundException('bookings');
			return false;
		}

		$items_unable = [];
		foreach ($this->data['items'] as $item)
		{
			if (!in_array($item->status_id, Config::get('booking.may.'.$status)))
			{
				$items_unable[] = $item->id;
			}
		}
		if (!empty($items_unable))
		{
			throw new TnExceptions\UpdateStatusException(implode(', ', $items_unable));
			return false;
		}
	}

	public function updateStatus($status, $reason = false, $redirect = false, $redirect = false)
	{
		try
		{
			$this->checkUpdateStatus($status);
		}
		catch (TnExceptions\InfoNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			return Redirect::to(URL::previous());
		}
		catch (TnExceptions\UpdateStatusException $e)
		{
			Notification::error(trans('tn.items_unable_to_update').': '.$e->getMessage());
			return Redirect::to(URL::previous());
		}

		if (!$redirect)
		{
			$redirect = URL::previous();
		}

		$ids = Input::get('cid');
		$data = [
			'status_id' => Config::get('booking.status_id.'.$status),
		];

		if (in_array($status, ['no_show', 'completed', 'free_of_charge']))
		{
			$data['done_at'] = date('Y-m-d H:i:s');
		}
		if ($status == 'free_of_charge')
		{
			$data['total'] = 0;
		}

		if (Booking::whereIn('id', $ids)->update($data))
		{
			Notification::success(trans('tn.succeeded'));

			$log_data = [];
			$noti_data = [];
			if ($reason)
			{
				$noti_data['reason'] = $log_data['reason'] = (Input::has('reason')) ? Input::get('reason') : '';
			}
			$this->logUpdateStatus($ids, $status, $log_data);
			$this->notificationUpdateStatus($ids, $status, $noti_data);
			$this->carworkUpdateStatus($ids, $status);

		}
		else
		{
			Notification::error(trans('tn.failed'));
		}
		return Redirect::to($redirect);
	}

	public function logUpdateStatus($ids, $status, $log_data = [], $options = [])
	{
		return Booking::logUpdateStatus($ids, Config::get('booking.status_id.'.$status), $log_data, $options);
	}

	public function notificationUpdateStatus($ids, $status, $noti_data = [])
	{
		$partners = [];
		$cars = [];
		$error_ids = [];
		$status_id = Config::get('booking.status_id.'.$status);

		foreach ($ids as $id)
		{
			try
			{
				$item = Booking::findOrFail($id);

				if ($item->partner_id)
				{
					$partners[$item->partner_id][] = $id;
				}

				if ($item->car_id)
				{
					$cars[$item->car_id][] = $id;
				}
			}
			catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
			{
				$error_ids[] = $id;
			}
		}

		if (!empty($error_ids))
		{
			$noti_data['error_ids'] = implode(',', $error_ids);
		}

		$general_event_data = [
			'status'   => trans('tn.'.$status),
			'color'    => Config::get('booking.color.'.$status_id),
			'user'     => $this->logged_in_user->full_name,
			'datetime' => date('d-m-Y H:i:s'),
		];

		if (!empty($ids))
		{
			$prefix = Config::get('admin.prefix');
			$user_type = Config::get('user.type_id.admin');
			$event = Config::get('admin.event.status_update');

			Booking::notificationUpdateStatus($ids, $status_id, $user_type, false, $noti_data);

			$event_data = $general_event_data + [
				'type' => $user_type,
				'ids'  => $ids,
				'link' => URL::route($prefix.'.booking', ['ids' => implode(',', $ids)]),
			];
			Event::fire($event, array(json_encode($event_data)));
		}

		if (!empty($partners))
		{
			$prefix = Config::get('partner.prefix');
			$user_type = Config::get('user.type_id.partner');
			$event = Config::get('partner.event.status_update');

			foreach ($partners as $partner_id => $booking_ids)
			{
				Booking::notificationUpdateStatus($booking_ids, $status_id, $user_type, $partner_id, $noti_data);

				$event_data = $general_event_data + [
					'type'    => $user_type,
					'type_id' => $partner_id,
					'ids'     => $booking_ids,
					'link'    => URL::route($prefix.'.booking', ['ids' => implode(',', $booking_ids)]),
				];
				Event::fire($event, array(json_encode($event_data)));
			}
		}

		if (!empty($cars))
		{
			$prefix = Config::get('car.prefix');
			$user_type = Config::get('user.type_id.car');
			$event = Config::get('car.event.status_update');

			foreach ($cars as $car_id => $booking_ids)
			{
				Booking::notificationUpdateStatus($booking_ids, $status_id, $user_type, $car_id, $noti_data);

				$event_data = $general_event_data + [
					'type'    => $user_type,
					'type_id' => $car_id,
					'ids'     => $booking_ids,
					'link'    => URL::route($prefix.'.booking', ['ids' => implode(',', $booking_ids)]),
				];
				Event::fire($event, array(json_encode($event_data)));
			}
		}
	}

	public function carworkUpdateStatus($ids, $status)
	{
		$car_works = Config::get('booking.car_works');
		if (in_array($status, $car_works['y']))
		{
			$event_data = ['on_duty' => 1];
		}
		elseif (in_array($status, $car_works['n']))
		{
			$event_data = ['on_duty' => 0];
		}
		else
		{
			return false;
		}
		$event_data['ids'] = Car::getIdsByBookings($ids);
		Event::fire(Config::get('admin.event.car_work'), array(json_encode($event_data)));
	}

}
