<?php

class AdminNewsController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = 'News';
		$this->data['main_route'] = $this->main_route = 'news';
		$this->data['page_title'] = trans('tn.news');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.admin'), trans('tn.news')];

		if (empty($this->logged_in_user->level) || $this->logged_in_user->level <= 0)
		{
			Notification::error(trans('tn.can_not_access'));
			$redirect = URL::previous();
		}

		$this->logged_in_user_groups = [];
		foreach ($this->logged_in_user->groups as $group)
		{
			$this->logged_in_user_groups[] = $group->id;
		}
	}

	public function getIndex()
	{
		// filter
		$filter = ['id', 'title', 'hot', 'user_id'];
		$columns = [
			'id'         => 'news.id',
			'title'      => 'news.title',
			'user'       => 'users.full_name',
			'created_at' => 'news.created_at',
			'status'     => 'news.status',
			'position'   => 'news.position',
		];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		$groups = [];
		foreach ($this->logged_in_user->groups as $group)
		{
			$groups[] = $group->id;
		}

		$this->data['items'] = News::select('news.*')
			->leftJoin('news_group', 'news_group.news_id', '=', 'news.id')
			->searchIn('news_group.group_id', $groups)
			->leftJoin('users', 'users.id', '=', 'news.user_id')
			->with('user')
			->searchLike('news.id', $id)
			->searchLike('news.title', $title)
			->searchLike('news.hot', $hot)
			->search('news.user_id', $user_id)
			->groupBy('news.id')
			->orderBy($columns[$sort], $order)
			->paginate($per_page);

		$this->data['breadcrumb'][] = trans('tn.manage');
		return View::make($this->prefix.'.'.$this->main_route.'.index', $this->data);
	}

	public function getAdd()
	{
		$this->data['groups_manage'] = Group::getGroupsByLevel($this->logged_in_user->level, '>');

		$this->data['breadcrumb'][] = trans('tn.add');
		return View::make($this->prefix.'.'.$this->main_route.'.add', $this->data);
	}

	public function postAdd()
	{
		$attributes = Input::all();
		$groups = [];

		$groups_check = Input::has('groups') ? Input::get('groups') : [];
		unset($attributes['groups']);
		foreach ($groups_check as $group => $checked)
		{
			if ($checked)
			{
				$groups[] = $group;
			}
		}

		$groups_manage = Group::getGroupsByLevel($this->logged_in_user->level, '<=');
		foreach ($groups_manage as $group)
		{
			$groups[] = $group->id;
		}

		$attributes['user_id'] = $this->logged_in_user->id;

		$item = new News($attributes);
		if ($item->save())
		{
			$item->groups()->attach($groups);
			Notification::success(trans('tn.succeeded'));
		}
		else
		{
			Notification::error(trans('tn.failed'));
		}
		$redirect = URL::route($this->prefix.'.'.$this->main_route);
		return Redirect::to($redirect);
	}

	public function getEdit($id)
	{
		try
		{
			$this->data['item'] = News::findOrFail($id);

			$item_groups = [];
			foreach ($this->data['item']->groups as $group)
			{
				$item_groups[] = $group->id;
			}
			$intersect = array_intersect($item_groups, $this->logged_in_user_groups);
			if (empty($intersect))
			{
				Notification::error(trans('tn.info_not_found'));
				return Redirect::to(URL::previous());
			}
			$this->data['item_groups'] = $item_groups;

			$this->data['groups_manage'] = Group::getGroupsByLevel($this->logged_in_user->level, '>');
			$this->data['item']->groups_array = explode(',', $this->data['item']->groups);

			$this->data['breadcrumb'][] = trans('tn.edit');
			return View::make($this->prefix.'.'.$this->main_route.'.edit', $this->data);
		}
		catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function postEdit($id)
	{
		try
		{
			$item = News::findOrFail($id);

			$item_groups = [];
			foreach ($item->groups as $group)
			{
				$item_groups[] = $group->id;
			}
			$intersect = array_intersect($item_groups, $this->logged_in_user_groups);
			if (empty($intersect))
			{
				Notification::error(trans('tn.info_not_found'));
				return Redirect::to(URL::previous());
			}

			$attributes = Input::all();
			$groups = [];

			$groups_check = Input::has('groups') ? Input::get('groups') : [];
			unset($attributes['groups']);
			foreach ($groups_check as $group => $checked)
			{
				if ($checked)
				{
					$groups[] = $group;
				}
			}

			$groups_manage = Group::getGroupsByLevel($this->logged_in_user->level, '<=');
			foreach ($groups_manage as $group)
			{
				if (in_array($group->id, $item_groups))
				{
					$groups[] = $group->id;
				}
			}

			if ($item->update($attributes))
			{
				$item->groups()->detach();
				$item->groups()->attach($groups);
				Notification::success(trans('tn.succeeded'));
			}
			else
			{
				Notification::error(trans('tn.failed'));
			}
			$redirect = URL::route($this->prefix.'.'.$this->main_route);
		}
		catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function getDelete($id)
	{
		try
		{
			$item = News::findOrFail($id);

			$item_groups = [];
			foreach ($item->groups as $group)
			{
				$item_groups[] = $group->id;
			}
			$intersect = array_intersect($item_groups, $this->logged_in_user_groups);
			if (empty($intersect))
			{
				Notification::error(trans('tn.info_not_found'));
				return Redirect::to(URL::previous());
			}

			if ($item->delete())
			{
				Notification::success(trans('tn.succeeded'));
			}
			else
			{
				Notification::error(trans('tn.failed'));
			}
			$redirect = URL::previous();
		}
		catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

}
