<?php

class AdminCartypeController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = 'Cartype';
		$this->data['main_route'] = $this->main_route = 'cartype';
		$this->data['page_title'] = trans('tn.cartype');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.fleet'), trans('tn.cartype')];
	}

	public function getIndex()
	{
		// filter
		$filter = ['id', 'name', 'code'];
		$columns = [
			'id'         => 'cartypes.id',
			'name'       => 'cartypes.name',
			'code'       => 'cartypes.code',
			'status'     => 'cartypes.status',
			'position'   => 'cartypes.position',
		];
		$this->data['filter'] = $this->initFilter($filter, $columns, ['default_sort' => 'position']);
		extract($this->data['filter']);
		// end filter

		$this->data['items'] = Cartype::select('cartypes.*', DB::raw('COUNT(cars.id) AS car_amount'))
			->leftJoin('cars', 'cars.cartype_id', '=', 'cartypes.id')
			->groupBy('cartypes.id')
			->searchLike('cartypes.id', $id)
			->searchLike('cartypes.name', $name)
			->searchLike('cartypes.code', $code)
			->orderBy($columns[$sort], $order)
			->paginate($per_page);

		$this->data['breadcrumb'][] = trans('tn.manage');
		return View::make($this->prefix.'.'.$this->main_route.'.index', $this->data);
	}

}
