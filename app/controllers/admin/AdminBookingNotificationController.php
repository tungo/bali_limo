<?php

class AdminBookingNotificationController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = 'BookingNotification';
		$this->data['main_route'] = $this->main_route = 'booking_notification';
		$this->data['page_title'] = trans('tn.booking_notification');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.booking_notification')];

	}

	public function getIndex()
	{
		// filter
		$filter = ['id', 'status_id', 'user_id'];
		$columns = [
			'id'      => 'booking_notifications.id',
			'status'  => 'booking_statuses.name',
			'user'    => 'users.full_name',
			'at'      => 'booking_notifications.created_at',
		];
		$this->data['filter'] = $this->initFilter($filter, $columns, ['default_sort' => 'at']);
		extract($this->data['filter']);
		// end filter

		$this->data['statuses'] = BookingStatus::getSelect(['all']);
		$this->data['users'] = User::getSelect(['all']);

		$this->data['items'] = BookingNotification::select('booking_notifications.*')
			->where('booking_notifications.type', '=', Config::get('user.type_id.admin'))
			->leftJoin('booking_statuses', 'booking_notifications.status_id', '=', 'booking_statuses.id')
			->leftJoin('users', 'booking_notifications.user_id', '=', 'users.id')
			->with('status')
			->with('user')
			->searchLike('booking_notifications.id', $id)
			->search('booking_notifications.status_id', $status_id)
			->search('booking_notifications.user_id', $user_id)
			->orderBy($columns[$sort], $order)
			->paginate($per_page);

		return View::make($this->prefix.'.'.$this->main_route.'.index', $this->data);
	}

}
