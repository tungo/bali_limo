<?php

class AdminGroupController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = 'Group';
		$this->data['main_route'] = $this->main_route = 'group';
		$this->data['page_title'] = trans('tn.group');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.account'), trans('tn.group')];
	}

	public function getIndex()
	{
		// filter
		$filter = ['id', 'name', 'level'];
		$columns = [
			'id'    => 'groups.id',
			'name'  => 'groups.name',
			'level' => 'groups.level',
		];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		$this->data['items'] = Group::select()
			->searchLike('groups.id', $id)
			->searchLike('groups.name', $name)
			->search('groups.level', $level)
			->orderBy($columns[$sort], $order)
			->paginate($per_page);

		$this->data['breadcrumb'][] = trans('tn.manage');
		return View::make($this->prefix.'.'.$this->main_route.'.index', $this->data);
	}

	public function getAdd()
	{
		$this->data['permissions_manage'] = Config::get('bali.permissions_manage');

		$this->data['breadcrumb'][] = trans('tn.add');
		return View::make($this->prefix.'.'.$this->main_route.'.add', $this->data);
	}

	public function postAdd()
	{
		try
		{
			$permissions = Input::has('permissions') ? Input::get('permissions') : [];
			$admin_permissions = Config::get('bali.permissions');
			$group_permissions = [];
			foreach ($admin_permissions as $name => $privileges)
			{
				$per = array_key_exists($name, $permissions) ? 1 : 0;
				foreach ($privileges as $privilege)
				{
					// $group_permissions[$this->prefix.'.'.$privilege] = $per;
					$group_permissions[$privilege] = $per;
				}
			}

			$group = Sentry::createGroup(array(
				'name' => Input::get('name'),
				'level' => Input::get('level'),
				'permissions' => $group_permissions,
			));
			if ($group)
			{
				Notification::success(trans('tn.succeeded'));
			}
			else
			{
				Notification::success(trans('tn.failed'));
			}
			$redirect = URL::route($this->prefix.'.'.$this->main_route);
		}
		catch (Cartalyst\Sentry\Groups\NameRequiredException $e)
		{
			Notification::error(trans('tn.name_field_required'));
			$redirect = URL::route($this->prefix.'.'.$this->main_route.'.add');
		}
		catch (Cartalyst\Sentry\Groups\GroupExistsException $e)
		{
			Notification::error(trans('tn.name_exists'));
			$redirect = URL::route($this->prefix.'.'.$this->main_route.'.add');
		}
		return Redirect::to($redirect);
	}

	public function getEdit($id)
	{
		try
		{
			$this->data['item'] = Sentry::findGroupById($id);

			$permissions = [];
			$admin_permissions = Config::get('bali.permissions');
			$group_permissions = $this->data['item']->getPermissions();
			foreach ($admin_permissions as $name => $privileges)
			{
				// $per = array_key_exists($this->prefix.'.'.$privileges[0], $group_permissions) ? 1 : 0;
				$per = array_key_exists($privileges[0], $group_permissions) ? 1 : 0;
				$permissions[$name] = $per;
			}

			$this->data['permissions'] = $permissions;
			$this->data['permissions_manage'] = Config::get('bali.permissions_manage');

			$this->data['breadcrumb'][] = trans('tn.edit');
			return View::make($this->prefix.'.'.$this->main_route.'.edit', $this->data);
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function postEdit($id)
	{
		try
		{
			$group = Sentry::findGroupById($id);

			$permissions = Input::has('permissions') ? Input::get('permissions') : [];
			$admin_permissions = Config::get('bali.permissions');
			$group_permissions = [];
			foreach ($admin_permissions as $name => $privileges)
			{
				$per = array_key_exists($name, $permissions) ? 1 : 0;
				foreach ($privileges as $privilege)
				{
					// $group_permissions[$this->prefix.'.'.$privilege] = $per;
					$group_permissions[$privilege] = $per;
				}
			}

			$group->name = Input::get('name');
			$group->level = Input::get('level');
			$group->permissions = $group_permissions;

			if ($group->save())
			{
				Notification::success(trans('tn.succeeded'));
			}
			else
			{
				Notification::error(trans('tn.failed'));
			}
			$redirect = URL::route($this->prefix.'.'.$this->main_route);
		}
		catch (Cartalyst\Sentry\Groups\NameRequiredException $e)
		{
			Notification::error(trans('tn.name_field_required'));
			$redirect = URL::previous();
		}
		catch (Cartalyst\Sentry\Groups\GroupExistsException $e)
		{
			Notification::error(trans('tn.name_exists'));
			$redirect = URL::previous();
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

}
