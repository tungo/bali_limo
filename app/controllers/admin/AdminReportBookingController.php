<?php

class AdminReportBookingController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = 'Booking';
		$this->data['main_route'] = $this->main_route = 'report_booking';
		$this->data['page_title'] = trans('tn.report_booking');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.report'), trans('tn.report_booking')];
	}

	public function getIndex()
	{
		return Redirect::route($this->prefix.'.'.$this->main_route.'.chi_tiet');
	}

	public function getChiTiet()
	{
		// filter
		$filter = ['partner_id', 'time'];
		$columns = [];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		$this->data['partners'] = Partner::getSelect();

		if (empty($partner_id))
		{
			$this->data['filter']['partner_id'] = $partner_id = key($this->data['partners']);
		}
		if (empty($time))
		{
			$this->data['filter']['time'] = $time = date('m-Y');
		}

		list($start_time, $end_time) = fn_datetime_get_month_time($time);

		if (empty($partner_id) || empty($start_time) || empty($end_time))
		{
			Notification::error(trans('tn.info_not_found'));
			return Redirect::to(URL::previous());
		}

		$this->data['types'] = Booking::getTypes(['all']);

		$this->data['month'] = date('m', strtotime($start_time));
		$this->data['year'] = date('Y', strtotime($start_time));

		$this->data['items'] = Booking::select()
			->search('bookings.partner_id', $partner_id)
			->searchBetween('bookings.done_at', $start_time, $end_time)
			->searchIn('bookings.status_id', [Config::get('booking.status_id.no_show'), Config::get('booking.status_id.completed'), Config::get('booking.status_id.free_of_charge')])
			->get();
		$this->data['report'] = Booking::calcReportChiTiet($this->data['items']);

		$this->data['page_title'] = trans('tn.bang_ke_chi_tiet');
		$this->data['breadcrumb'][] = trans('tn.bang_ke_chi_tiet');
		return View::make($this->prefix.'.'.$this->main_route.'.chi_tiet', $this->data);
	}

	public function getChiTietExport()
	{
		// filter
		$filter = ['partner_id', 'time'];
		$columns = [];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		$this->data['partners'] = Partner::getSelect();

		if (empty($partner_id))
		{
			$this->data['filter']['partner_id'] = $partner_id = key($this->data['partners']);
		}
		if (empty($time))
		{
			$this->data['filter']['time'] = $time = date('m-Y');
		}

		list($start_time, $end_time) = fn_datetime_get_month_time($time);

		if (empty($partner_id) || empty($start_time) || empty($end_time))
		{
			Notification::error(trans('tn.info_not_found'));
			return Redirect::to(URL::previous());
		}

		$this->data['month'] = date('m', strtotime($start_time));
		$this->data['year'] = date('Y', strtotime($start_time));

		$this->data['cartypes'] = Cartype::getCodesName();
		$this->data['types'] = Booking::getTypes(['all']);

		$this->data['items'] = Booking::select()
			->search('bookings.partner_id', $partner_id)
			->searchBetween('bookings.done_at', $start_time, $end_time)
			->searchIn('bookings.status_id', [Config::get('booking.status_id.no_show'), Config::get('booking.status_id.completed'), Config::get('booking.status_id.free_of_charge')])
			->get();
		$this->data['report'] = Booking::calcReportChiTiet($this->data['items']);

		$data = $this->data;
		$file_name = 'Bao-cao-chi-tiet_'.$time.'_'.$this->data['partners'][$partner_id];

		Excel::create($file_name, function($excel) use($data) {
			$excel->sheet('Sheet1', function($sheet) use($data) {

				$sheet->setAutoSize(false);

				$sheet->getColumnDimension('A')->setWidth(10);
				$sheet->getColumnDimension('B')->setWidth(13);
				$sheet->getColumnDimension('C')->setWidth(13);
				$sheet->getColumnDimension('D')->setWidth(20);
				$sheet->getColumnDimension('E')->setWidth(25);
				$sheet->getColumnDimension('F')->setWidth(10);
				$sheet->getColumnDimension('G')->setWidth(15);
				$sheet->getColumnDimension('H')->setWidth(15);
				$sheet->getColumnDimension('I')->setWidth(10);
				$sheet->getColumnDimension('J')->setWidth(15);
				$sheet->getColumnDimension('K')->setWidth(35);

				$objDrawing = new PHPExcel_Worksheet_Drawing();
				$objDrawing->setWorksheet($sheet);
				$objDrawing->setName('Logo');
				$objDrawing->setDescription('Bali Limousines');
				public_path('files/signature');
				$objDrawing->setPath(public_path('themes').'/default/img/icon/57x57.png');
				$objDrawing->setCoordinates('A1');
				$objDrawing->setOffsetX(3);
				$objDrawing->setOffsetY(3);

				$sheet->mergeCells('C1:J1');
				$sheet->setCellValue('C1', 'Công ty TNHH BALILIMOUSINES');
				$sheet->mergeCells('C2:J2');
				$sheet->setCellValue('C2', '15 Thái Thị Nhạn, Phường 10, Quận Tân Bình, Tp.HCM');
				$sheet->mergeCells('C3:J3');
				$sheet->setCellValue('C3', 'MST: 0 3 1 1 8 0 3 6 4 1');

				$sheet->mergeCells('A5:J5');
				$sheet->setCellValue('A5', 'BẢNG KÊ CHI TiẾT VẬN CHUYỂN KHÁCH THÁNG '.$data['month'].' NĂM '.$data['year']);
				$sheet->getStyle('A5:J5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$sheet->mergeCells('C6:J6');
				$sheet->setCellValue('C6', 'Kính gửi: '.$data['partners'][$data['filter']['partner_id']]);

				$sheet->setCellValue('A8', 'STT');
				$sheet->setCellValue('B8', 'Ngày tháng');
				$sheet->setCellValue('C8', 'Số confrom');
				$sheet->setCellValue('D8', 'Tên khách');
				$sheet->mergeCells('E8:F8');
				$sheet->setCellValue('E8', 'Hành trình');
				$sheet->setCellValue('G8', 'Số tiền (USD)');
				$sheet->setCellValue('H8', 'Số tiền (VND)');
				$sheet->mergeCells('I8:J8');
				$sheet->setCellValue('I8', 'Loại xe / Bộ phận');
				$sheet->setCellValue('K8', 'Ghi chú');

				$sheet->getStyle('A1:K8')->getFont()->setBold(true);
				$sheet->getStyle('A8:K8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$e_row = 8;
				$i = 0;
				foreach ($data['items'] as $item)
				{
					$e_row++;
					$i++;
					$sheet->setCellValue('A'.$e_row, $i);
					$sheet->setCellValue('B'.$e_row, fn_datetime($item->done_at, 'Y-m-d H:i:s', 'd-m-Y', null));
					$sheet->setCellValue('C'.$e_row, $item->id);
					$sheet->setCellValue('D'.$e_row, $item->customer_name);
					$sheet->setCellValue('E'.$e_row, $item->flight);
					$sheet->setCellValue('F'.$e_row, $this->data['types'][$item->type]);
					$sheet->setCellValue('G'.$e_row, $item->total);
					$sheet->setCellValue('I'.$e_row, $item->cartype);
				}
				$sheet->getStyle('A9:C'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$sheet->getStyle('G9:H'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$sheet->getStyle('I9:J'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$e_row++;
				$sheet->setCellValue('A'.$e_row, 'A');
				$sheet->mergeCells('B'.$e_row.':E'.$e_row);
				$sheet->setCellValue('B'.$e_row, 'Tổng cộng ('.$i.' chuyến)');
				$sheet->setCellValue('G'.$e_row, $data['report']['total']);
				$sheet->mergeCells('I'.$e_row.':J'.$e_row);
				$sheet->setCellValue('I'.$e_row, 'Tỷ giá');

				$e_row++;
				$sheet->setCellValue('A'.$e_row, 'B');
				$sheet->mergeCells('B'.$e_row.':E'.$e_row);
				$sheet->setCellValue('B'.$e_row, 'Thuế GTGT (10%)');
				$sheet->setCellValue('G'.$e_row, $data['report']['vat']);
				$sheet->mergeCells('I'.$e_row.':J'.$e_row);

				$e_row++;
				$sheet->setCellValue('A'.$e_row, 'C');
				$sheet->mergeCells('B'.$e_row.':E'.$e_row);
				$sheet->setCellValue('B'.$e_row, 'Tổng giá trị thành tiền');
				$sheet->setCellValue('G'.$e_row, $data['report']['total'] + $data['report']['vat']);
				$sheet->mergeCells('I'.$e_row.':J'.$e_row);

				$sheet->getStyle('A'.($e_row-2).':J'.$e_row)->getFont()->setBold(true);
				$sheet->getStyle('A'.($e_row-2).':F'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$sheet->getStyle('G'.($e_row-2).':H'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$sheet->getStyle('I'.($e_row-2).':J'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


				$sheet->setBorder('A8:K'.$e_row, 'thin');

				$e_row += 2;
				$e_row_sign = $e_row;

				$sheet->setCellValue('A'.$e_row, 'Ghi chú:');
				foreach ($data['cartypes'] as $key => $cartype)
				{
					$e_row ++;
					$sheet->setCellValue('B'.$e_row, $key);
					$sheet->setCellValue('C'.$e_row, $cartype);
				}

				$sheet->mergeCells('E'.$e_row_sign.':J'.$e_row_sign);
				$sheet->setCellValue('E'.$e_row_sign, 'Ngày '.date('d').' tháng '.date('m').' năm '.date('Y'));
				$sheet->getStyle('E'.$e_row_sign.':J'.$e_row_sign)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$e_row_sign++;
				$sheet->mergeCells('E'.$e_row_sign.':J'.$e_row_sign);
				$sheet->setCellValue('E'.$e_row_sign, 'Kế toán');
				$sheet->getStyle('E'.$e_row_sign.':J'.$e_row_sign)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$e_row_sign += 5;
				$sheet->mergeCells('E'.$e_row_sign.':J'.$e_row_sign);
				$sheet->setCellValue('E'.$e_row_sign, 'Phạm Thị Thu Trinh');
				$sheet->getStyle('E'.$e_row_sign.':J'.$e_row_sign)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			});
		})->store('xlsx')->export('xlsx');
	}

	public function getKeToan()
	{
		// filter
		$filter = ['time'];
		$columns = [];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		if (empty($time))
		{
			$this->data['filter']['time'] = $time = date('m-Y');
		}

		list($start_time, $end_time) = fn_datetime_get_month_time($time);

		if (empty($start_time) || empty($end_time))
		{
			Notification::error(trans('tn.info_not_found'));
			return Redirect::to(URL::previous());
		}

		$this->data['partners'] = Partner::getSelect();

		$this->data['month'] = date('m', strtotime($start_time));
		$this->data['year'] = date('Y', strtotime($start_time));

		$this->data['items'] = Booking::select()
			->searchBetween('bookings.done_at', $start_time, $end_time)
			->searchIn('bookings.status_id', [Config::get('booking.status_id.no_show'), Config::get('booking.status_id.completed'), Config::get('booking.status_id.free_of_charge')])
			->get();
		$this->data['report'] = Booking::calcReportKeToan($this->data['items']);

		$this->data['page_title'] = trans('tn.bao_cao_ke_toan');
		$this->data['breadcrumb'][] = trans('tn.bao_cao_ke_toan');
		return View::make($this->prefix.'.'.$this->main_route.'.ke_toan', $this->data);
	}

	public function getKeToanExport()
	{
		// filter
		$filter = ['time'];
		$columns = [];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		if (empty($time))
		{
			$this->data['filter']['time'] = $time = date('m-Y');
		}

		list($start_time, $end_time) = fn_datetime_get_month_time($time);

		$this->data['partners'] = Partner::getSelect();

		$this->data['month'] = date('m', strtotime($start_time));
		$this->data['year'] = date('Y', strtotime($start_time));

		$this->data['items'] = Booking::select()
			->searchBetween('bookings.done_at', $start_time, $end_time)
			->searchIn('bookings.status_id', [Config::get('booking.status_id.no_show'), Config::get('booking.status_id.completed'), Config::get('booking.status_id.free_of_charge')])
			// ->orderBy($columns[$sort], $order)
			->get();
		$this->data['report'] = Booking::calcReportKeToan($this->data['items']);

		$data = $this->data;
		$file_name = 'Bao-cao-ke_toan_'.$time;

		Excel::create($file_name, function($excel) use($data) {
			$excel->sheet('Sheet1', function($sheet) use($data) {

				$sheet->setAutoSize(false);

				$sheet->getColumnDimension('A')->setWidth(10);
				$sheet->getColumnDimension('B')->setWidth(30);
				$sheet->getColumnDimension('C')->setWidth(15);
				$sheet->getColumnDimension('D')->setWidth(15);
				$sheet->getColumnDimension('E')->setWidth(15);
				$sheet->getColumnDimension('F')->setWidth(15);
				$sheet->getColumnDimension('G')->setWidth(15);
				$sheet->getColumnDimension('H')->setWidth(15);

				$sheet->mergeCells('A1:H1');
				$sheet->setCellValue('A1', 'Công ty TNHH BALILIMOUSINES');
				$sheet->mergeCells('A2:H2');
				$sheet->setCellValue('A2', '15 Thái Thị Nhạn, Phường 10, Quận Tân Bình, Tp.HCM');
				$sheet->mergeCells('A3:H3');
				$sheet->setCellValue('A3', 'MST: 0 3 1 1 8 0 3 6 4 1');

				$sheet->mergeCells('A5:H5');
				$sheet->setCellValue('A5', 'BÁO CÁO KẾ TOÁN THÁNG '.$data['month'].' NĂM '.$data['year']);
				$sheet->getStyle('A5:H5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$sheet->mergeCells('A7:A9');
				$sheet->setCellValue('A7', 'STT');
				$sheet->mergeCells('B7:B9');
				$sheet->setCellValue('B7', 'Tên nhà cung cấp');
				$sheet->mergeCells('C7:H7');
				$sheet->setCellValue('C7 Tháng', $data['month'].' năm '.$data['year']);
				$sheet->mergeCells('C8:C9');
				$sheet->setCellValue('C8', 'Tồn đầu');
				$sheet->mergeCells('D8:D9');
				$sheet->setCellValue('D8', 'Phát sinh');
				$sheet->mergeCells('E8:G8');
				$sheet->setCellValue('E8', 'Thanh toán');
				$sheet->setCellValue('E9', 'Tiền mặt');
				$sheet->setCellValue('F9', 'Ngân hàng');
				$sheet->setCellValue('G9', 'Tổng cộng');
				$sheet->mergeCells('H8:H9');
				$sheet->setCellValue('H8', 'Tồn cuối');

				$sheet->getStyle('A7:H9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$sheet->getStyle('A1:H9')->getFont()->setBold(true);

				$e_row = 9;
				$i = 0;
				foreach ($data['partners'] as $partner_id => $partner)
				{
					$e_row++;
					$i++;
					$phat_sinh = !empty($data['report']['rows'][$partner_id]) ? $data['report']['rows'][$partner_id] : '';

					$sheet->setCellValue('A'.$e_row, $i);
					$sheet->setCellValue('B'.$e_row, $partner);
					$sheet->setCellValue('D'.$e_row, $phat_sinh);
				}
				$sheet->getStyle('A10:A'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$sheet->getStyle('C10:H'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				$e_row++;
				$sheet->setCellValue('B'.$e_row, 'Tổng cộng');
				$sheet->getStyle('B'.$e_row.':B'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$sheet->setCellValue('D'.$e_row, $data['report']['total']);
				$sheet->getStyle('A'.$e_row.':H'.$e_row)->getFont()->setBold(true);

				$sheet->setBorder('A7:H'.$e_row, 'thin');

			});
		})->store('xlsx')->export('xlsx');
	}

	public function getXe()
	{
		// filter
		$filter = ['time'];
		$columns = [];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		if (empty($time))
		{
			$this->data['filter']['time'] = $time = date('m-Y');
		}

		list($start_time, $end_time) = fn_datetime_get_month_time($time);

		if (empty($start_time) || empty($end_time))
		{
			Notification::error(trans('tn.info_not_found'));
			return Redirect::to(URL::previous());
		}

		$this->data['cars'] = Car::getNumbers();

		$this->data['month'] = date('m', strtotime($start_time));
		$this->data['year'] = date('Y', strtotime($start_time));

		$this->data['items'] = Booking::select()
			->searchBetween('bookings.done_at', $start_time, $end_time)
			->searchIn('bookings.status_id', [Config::get('booking.status_id.no_show'), Config::get('booking.status_id.completed'), Config::get('booking.status_id.free_of_charge')])
			->get();
		$this->data['report'] = Booking::calcReportXe($this->data['items']);

		$this->data['page_title'] = trans('tn.chi_phi_theo_xe');
		$this->data['breadcrumb'][] = trans('tn.chi_phi_theo_xe');
		return View::make($this->prefix.'.'.$this->main_route.'.xe', $this->data);
	}

	public function getXeExport()
	{
		// filter
		$filter = ['time'];
		$columns = [];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		if (empty($time))
		{
			$this->data['filter']['time'] = $time = date('m-Y');
		}

		list($start_time, $end_time) = fn_datetime_get_month_time($time);

		if (empty($start_time) || empty($end_time))
		{
			Notification::error(trans('tn.info_not_found'));
			return Redirect::to(URL::previous());
		}

		$this->data['cars'] = Car::getNumbers();

		$this->data['month'] = date('m', strtotime($start_time));
		$this->data['year'] = date('Y', strtotime($start_time));

		$this->data['items'] = Booking::select()
			->searchBetween('bookings.done_at', $start_time, $end_time)
			->searchIn('bookings.status_id', [Config::get('booking.status_id.no_show'), Config::get('booking.status_id.completed'), Config::get('booking.status_id.free_of_charge')])
			->get();
		$this->data['report'] = Booking::calcReportXe($this->data['items']);

		$data = $this->data;
		$file_name = 'Chi-phi-theo-xe_'.$time;

		Excel::create($file_name, function($excel) use($data) {
			$excel->sheet('Sheet1', function($sheet) use($data) {

				$sheet->setAutoSize(false);

				$sheet->getColumnDimension('A')->setWidth(10);
				$sheet->getColumnDimension('B')->setWidth(25);
				$sheet->getColumnDimension('C')->setWidth(10);
				$sheet->getColumnDimension('D')->setWidth(15);
				$sheet->getColumnDimension('E')->setWidth(15);
				$sheet->getColumnDimension('F')->setWidth(10);
				$sheet->getColumnDimension('G')->setWidth(10);
				$sheet->getColumnDimension('H')->setWidth(10);
				$sheet->getColumnDimension('I')->setWidth(10);
				$sheet->getColumnDimension('J')->setWidth(10);
				$sheet->getColumnDimension('K')->setWidth(10);
				$sheet->getColumnDimension('L')->setWidth(10);
				$sheet->getColumnDimension('M')->setWidth(10);
				$sheet->getColumnDimension('N')->setWidth(10);
				$sheet->getColumnDimension('O')->setWidth(15);
				$sheet->getColumnDimension('P')->setWidth(7);
				$sheet->getColumnDimension('Q')->setWidth(10);
				$sheet->getColumnDimension('R')->setWidth(10);
				$sheet->getColumnDimension('S')->setWidth(15);
				$sheet->getColumnDimension('T')->setWidth(10);
				$sheet->getColumnDimension('U')->setWidth(15);
				$sheet->getColumnDimension('V')->setWidth(15);
				$sheet->getColumnDimension('W')->setWidth(15);
				$sheet->getColumnDimension('X')->setWidth(15);

				$sheet->mergeCells('A1:J1');
				$sheet->setCellValue('A1', 'Công ty TNHH BALILIMOUSINES');
				$sheet->mergeCells('A2:J2');
				$sheet->setCellValue('A2', '15 Thái Thị Nhạn, Phường 10, Quận Tân Bình, Tp.HCM');
				$sheet->mergeCells('A3:J3');
				$sheet->setCellValue('A3', 'MST: 0 3 1 1 8 0 3 6 4 1');

				$sheet->mergeCells('A5:H5');
				$sheet->setCellValue('A5', 'CHI PHÍ THEO XE THÁNG '.$data['month'].' NĂM '.$data['year']);
				$sheet->getStyle('A5:H5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$sheet->mergeCells('A7:A8');
				$sheet->setCellValue('A7', 'STT');
				$sheet->mergeCells('B7:B8');
				$sheet->setCellValue('B7', 'Số xe');
				$sheet->mergeCells('C7:C8');
				$sheet->setCellValue('C7', 'Số chuyến');
				$sheet->mergeCells('D7:D8');
				$sheet->setCellValue('D7', 'Số tiền (USD)');
				$sheet->mergeCells('E7:E8');
				$sheet->setCellValue('E7', 'Số tiền (VNĐ)');
				$sheet->mergeCells('F7:H7');
				$sheet->setCellValue('F7', 'Xăng');
				$sheet->setCellValue('F8', 'SL');
				$sheet->setCellValue('G8', 'DG');
				$sheet->setCellValue('H8', 'TT');
				$sheet->mergeCells('I7:K7');
				$sheet->setCellValue('I7', 'Nước');
				$sheet->setCellValue('I8', 'SL');
				$sheet->setCellValue('J8', 'DG');
				$sheet->setCellValue('K8', 'TT');
				$sheet->mergeCells('L7:N7');
				$sheet->setCellValue('L7', 'Khăn');
				$sheet->setCellValue('L8', 'SL');
				$sheet->setCellValue('M8', 'DG');
				$sheet->setCellValue('N8', 'TT');
				$sheet->mergeCells('O7:O8');
				$sheet->setCellValue('O7', 'Cầu đường');
				$sheet->mergeCells('P7:P8');
				$sheet->setCellValue('P7', 'OT');
				$sheet->mergeCells('Q7:Q8');
				$sheet->setCellValue('Q7', 'Lương');
				$sheet->mergeCells('R7:R8');
				$sheet->setCellValue('R7', 'Thuê VP');
				$sheet->mergeCells('S7:S8');
				$sheet->setCellValue('S7', 'Chi phí khấu hao xe');
				$sheet->mergeCells('T7:T8');
				$sheet->setCellValue('T7', 'KHVP');
				$sheet->mergeCells('U7:U8');
				$sheet->setCellValue('U7', 'Chi phí trích trước');
				$sheet->mergeCells('V7:V8');
				$sheet->setCellValue('V7', 'Chi phí linh tinh khác');
				$sheet->mergeCells('W7:W8');
				$sheet->setCellValue('W7', 'Tổng cộng');
				$sheet->mergeCells('X7:X8');
				$sheet->setCellValue('X7', 'Lãi lỗ');

				$sheet->getStyle('A7:X8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$sheet->getStyle('A1:X8')->getFont()->setBold(true);

				$e_row = 8;
				$i = 0;
				foreach ($data['cars'] as $car_id => $car)
				{
					$e_row++;
					$i++;
					$count = !empty($data['report']['rows'][$car_id]['count']) ? $data['report']['rows'][$car_id]['count'] : '';
					$total = !empty($data['report']['rows'][$car_id]['total']) ? $data['report']['rows'][$car_id]['total'] : '';

					$sheet->setCellValue('A'.$e_row, $i);
					$sheet->setCellValue('B'.$e_row, $car);
					$sheet->setCellValue('C'.$e_row, $count);
					$sheet->setCellValue('D'.$e_row, $total);
				}
				$sheet->getStyle('A9:A'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$sheet->getStyle('C9:C'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$sheet->getStyle('D9:E'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				$e_row++;
				$sheet->setCellValue('B'.$e_row, 'Tổng cộng');
				$sheet->getStyle('B'.$e_row.':C'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$sheet->setCellValue('C'.$e_row, $data['report']['count']);
				$sheet->setCellValue('D'.$e_row, $data['report']['total']);
				$sheet->getStyle('B'.$e_row.':D'.$e_row)->getFont()->setBold(true);

				$sheet->setBorder('A7:X'.$e_row, 'thin');

			});
		})->store('xlsx')->export('xlsx');
	}

	public function getDoanhThu()
	{
		// filter
		$filter = ['time'];
		$columns = [];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		if (empty($time))
		{
			$this->data['filter']['time'] = $time = date('m-Y');
		}

		list($start_time, $end_time) = fn_datetime_get_month_time($time);

		if (empty($start_time) || empty($end_time))
		{
			Notification::error(trans('tn.info_not_found'));
			return Redirect::to(URL::previous());
		}

		$this->data['month'] = date('m', strtotime($start_time));
		$this->data['year'] = date('Y', strtotime($start_time));

		$this->data['items'] = Booking::select()
			->searchBetween('bookings.done_at', $start_time, $end_time)
			->searchIn('bookings.status_id', [Config::get('booking.status_id.no_show'), Config::get('booking.status_id.completed'), Config::get('booking.status_id.free_of_charge')])
			->get();
		$this->data['report'] = Booking::calcReportDoanhThu($this->data['items']);

		$this->data['page_title'] = trans('tn.bao_cao_doanh_thu');
		$this->data['breadcrumb'][] = trans('tn.bao_cao_doanh_thu');
		return View::make($this->prefix.'.'.$this->main_route.'.doanh_thu', $this->data);
	}

	public function getDoanhThuExport()
	{
		// filter
		$filter = ['time'];
		$columns = [];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		if (empty($time))
		{
			$this->data['filter']['time'] = $time = date('m-Y');
		}

		list($start_time, $end_time) = fn_datetime_get_month_time($time);

		if (empty($start_time) || empty($end_time))
		{
			Notification::error(trans('tn.info_not_found'));
			return Redirect::to(URL::previous());
		}

		$this->data['month'] = date('m', strtotime($start_time));
		$this->data['year'] = date('Y', strtotime($start_time));

		$this->data['items'] = Booking::select()
			->searchBetween('bookings.done_at', $start_time, $end_time)
			->searchIn('bookings.status_id', [Config::get('booking.status_id.no_show'), Config::get('booking.status_id.completed'), Config::get('booking.status_id.free_of_charge')])
			->get();
		$this->data['report'] = Booking::calcReportDoanhThu($this->data['items']);

		$data = $this->data;
		$file_name = 'Bao-cao-doanh-thu_'.$time;

		Excel::create($file_name, function($excel) use($data) {
			$excel->sheet('Sheet1', function($sheet) use($data) {

				$sheet->setAutoSize(false);

				$sheet->getColumnDimension('A')->setWidth(10);
				$sheet->getColumnDimension('B')->setWidth(30);
				$sheet->getColumnDimension('C')->setWidth(15);
				$sheet->getColumnDimension('D')->setWidth(25);
				$sheet->getColumnDimension('E')->setWidth(15);
				$sheet->getColumnDimension('F')->setWidth(15);
				$sheet->getColumnDimension('G')->setWidth(15);

				$sheet->mergeCells('A1:H1');
				$sheet->setCellValue('A1', 'Công ty TNHH BALILIMOUSINES');
				$sheet->mergeCells('A2:H2');
				$sheet->setCellValue('A2', '15 Thái Thị Nhạn, Phường 10, Quận Tân Bình, Tp.HCM');
				$sheet->mergeCells('A3:H3');
				$sheet->setCellValue('A3', 'MST: 0 3 1 1 8 0 3 6 4 1');

				$sheet->mergeCells('A5:H5');
				$sheet->setCellValue('A5', 'BÁO CÁO DOANH THU THÁNG '.$data['month'].' NĂM '.$data['year']);
				$sheet->getStyle('A5:H5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$sheet->setCellValue('A7', 'Ngày');
				$sheet->setCellValue('B7', 'Diễn giải');
				$sheet->setCellValue('C7', 'Số tiền (USD)');
				$sheet->setCellValue('D7', 'Số tiền (VND)'."\n".'(Tỷ giá: 21.000)');
				$sheet->getStyle('D7')->getAlignment()->setWrapText(true);
				$sheet->setCellValue('E7', 'Phải thu');
				$sheet->setCellValue('F7', 'Tiền mặt');
				$sheet->setCellValue('G7', 'Toàn lũy kế');

				$sheet->setCellValue('A8', 'A');
				$sheet->setCellValue('B8', 'B');
				$sheet->setCellValue('C8', '1');
				$sheet->setCellValue('D8', '2 = 1 * 21.000');
				$sheet->setCellValue('E8', '3');
				$sheet->setCellValue('F8', '4');
				$sheet->setCellValue('G8', '5');

				$sheet->getStyle('A7:G8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$sheet->getStyle('A1:G8')->getFont()->setBold(true);

				$sheet->setCellValue('B9', 'Tuần lễ (từ ngày - đến ngày)');

				$sheet->setCellValue('B10', 'Tuần 1 (1 - 7)');
				$sheet->setCellValue('C10', $data['report']['rows']['week_1']);
				$sheet->setCellValue('B11', 'Tuần 1 (8 - 14)');
				$sheet->setCellValue('C11', $data['report']['rows']['week_2']);
				$sheet->setCellValue('B12', 'Tuần 1 (15 - 21)');
				$sheet->setCellValue('C12', $data['report']['rows']['week_3']);
				$sheet->setCellValue('B13', 'Tuần 1 (22 - '.$data['report']['last_day'].')');
				$sheet->setCellValue('C13', $data['report']['rows']['week_4']);

				$sheet->setCellValue('B14', 'Thuê tài xế (PARTHYATT)');
				$sheet->setCellValue('B15', 'Thuế GTGT');
				$sheet->setCellValue('B16', 'Phí khác');
				$sheet->setCellValue('B17', 'Tổng cộng');
				$sheet->getStyle('B17')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$sheet->getStyle('B17')->getFont()->setBold(true);

				$sheet->setBorder('A7:G17', 'thin');

			});
		})->store('xlsx')->export('xlsx');
	}

	public function getThongKe()
	{
		// filter
		$filter = ['date_from', 'date_to'];
		$columns = [];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		if (empty($date_from))
		{
			$this->data['filter']['date_from'] = $date_from = date('d-m-Y');
		}
		if (empty($date_to))
		{
			$this->data['filter']['date_to'] = $date_to = date('d-m-Y');
		}

		$start_time = fn_datetime($date_from, 'd-m-Y', 'Y-m-d', null);
		$end_time   = fn_datetime($date_to, 'd-m-Y', 'Y-m-d', null);

		if (empty($start_time) || empty($end_time))
		{
			Notification::error(trans('tn.info_not_found'));
			return Redirect::to(URL::previous());
		}

		$this->data['types'] = Booking::getTypes(['all']);

		$this->data['items'] = Booking::select()
			->searchBetween('bookings.date', $start_time, $end_time)
			->searchIn('bookings.status_id', [Config::get('booking.status_id.no_show'), Config::get('booking.status_id.completed'), Config::get('booking.status_id.free_of_charge')])
			->get();
		$this->data['report'] = Booking::calcReportThongKe($this->data['items']);

		$this->data['page_title'] = trans('tn.bao_cao_thong_ke');
		$this->data['breadcrumb'][] = trans('tn.bao_cao_thong_ke');
		return View::make($this->prefix.'.'.$this->main_route.'.thong_ke', $this->data);
	}

	public function getThongKeExport()
	{
		// filter
		$filter = ['date_from', 'date_to'];
		$columns = [];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		$this->data['partners'] = Partner::getSelect();

		if (empty($date_from))
		{
			$this->data['filter']['date_from'] = $date_from = date('d-m-Y');
		}
		if (empty($date_to))
		{
			$this->data['filter']['date_to'] = $date_to = date('d-m-Y');
		}

		$start_time = fn_datetime($date_from, 'd-m-Y', 'Y-m-d', null);
		$end_time   = fn_datetime($date_to, 'd-m-Y', 'Y-m-d', null);

		if (empty($start_time) || empty($end_time))
		{
			Notification::error(trans('tn.info_not_found'));
			return Redirect::to(URL::previous());
		}

		$this->data['cartypes'] = Cartype::getCodesName();
		$this->data['types'] = Booking::getTypes(['all']);

		$this->data['items'] = Booking::select()
			->searchBetween('bookings.date', $start_time, $end_time)
			->searchIn('bookings.status_id', [Config::get('booking.status_id.no_show'), Config::get('booking.status_id.completed'), Config::get('booking.status_id.free_of_charge')])
			->get();
		$this->data['report'] = Booking::calcReportChiTiet($this->data['items']);

		$data = $this->data;
		$file_name = 'Bao-cao-thong-ke_'.$date_from.'_'.$date_to;

		Excel::create($file_name, function($excel) use($data) {
			$excel->sheet('Sheet1', function($sheet) use($data) {

				$sheet->setAutoSize(false);

				$sheet->getColumnDimension('A')->setWidth(10);
				$sheet->getColumnDimension('B')->setWidth(13);
				$sheet->getColumnDimension('C')->setWidth(13);
				$sheet->getColumnDimension('D')->setWidth(20);
				$sheet->getColumnDimension('E')->setWidth(20);
				$sheet->getColumnDimension('F')->setWidth(25);
				$sheet->getColumnDimension('G')->setWidth(10);
				$sheet->getColumnDimension('H')->setWidth(15);
				$sheet->getColumnDimension('I')->setWidth(15);
				$sheet->getColumnDimension('J')->setWidth(15);
				$sheet->getColumnDimension('K')->setWidth(15);
				$sheet->getColumnDimension('L')->setWidth(25);
				$sheet->getColumnDimension('M')->setWidth(15);
				$sheet->getColumnDimension('N')->setWidth(20);

				$objDrawing = new PHPExcel_Worksheet_Drawing();
				$objDrawing->setWorksheet($sheet);
				$objDrawing->setName('Logo');
				$objDrawing->setDescription('Bali Limousines');
				public_path('files/signature');
				$objDrawing->setPath(public_path('themes').'/default/img/icon/57x57.png');
				$objDrawing->setCoordinates('A1');
				$objDrawing->setOffsetX(3);
				$objDrawing->setOffsetY(3);

				$sheet->mergeCells('C1:J1');
				$sheet->setCellValue('C1', 'Công ty TNHH BALILIMOUSINES');
				$sheet->mergeCells('C2:J2');
				$sheet->setCellValue('C2', '15 Thái Thị Nhạn, Phường 10, Quận Tân Bình, Tp.HCM');
				$sheet->mergeCells('C3:J3');
				$sheet->setCellValue('C3', 'MST: 0 3 1 1 8 0 3 6 4 1');

				$sheet->mergeCells('A5:J5');
				$sheet->setCellValue('A5', 'BÁO CÁO THỐNG KÊ');
				$sheet->getStyle('A5:J5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				if ($data['filter']['date_from'] == $data['filter']['date_to'])
				{
					$sheet->setCellValue('C6', 'Ngày:');
					$sheet->setCellValue('D6', $data['filter']['date_from']);
				}
				else
				{
					$sheet->setCellValue('C6', 'Từ ngày:');
					$sheet->setCellValue('D6', $data['filter']['date_from']);
					$sheet->setCellValue('E6', 'Đến ngày:');
					$sheet->setCellValue('F6', $data['filter']['date_to']);
				}

				$sheet->setCellValue('A8', 'STT');
				$sheet->setCellValue('B8', 'Ngày tháng');
				$sheet->setCellValue('C8', 'Số confrom');
				$sheet->setCellValue('D8', 'Tên khách');
				$sheet->setCellValue('E8', 'Đối tác');
				$sheet->mergeCells('F8:G8');
				$sheet->setCellValue('F8', 'Hành trình');
				$sheet->setCellValue('H8', 'Số tiền (USD)');
				$sheet->setCellValue('I8', 'Số tiền (VND)');
				$sheet->mergeCells('J8:K8');
				$sheet->setCellValue('J8', 'Loại xe / Bộ phận');
				$sheet->setCellValue('L8', 'Tài xế');
				$sheet->setCellValue('M8', 'Số xe');
				$sheet->setCellValue('N8', 'Ghi chú');

				$sheet->getStyle('A1:N8')->getFont()->setBold(true);
				$sheet->getStyle('A8:N8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$e_row = 8;
				$i = 0;
				foreach ($data['items'] as $item)
				{
					$item_partner = count($item->partner) ? $item->partner->name : '';
		      $item_driver  = count($item->driver) ? $item->driver->full_name : '';
		      $item_car     = count($item->car) ? $item->car->number : '';

					$e_row++;
					$i++;
					$sheet->setCellValue('A'.$e_row, $i);
					$sheet->setCellValue('B'.$e_row, fn_datetime($item->date, 'Y-m-d', 'd-m-Y', null));
					$sheet->setCellValue('C'.$e_row, $item->id);
					$sheet->setCellValue('D'.$e_row, $item->customer_name);
					$sheet->setCellValue('E'.$e_row, $item_partner);
					$sheet->setCellValue('F'.$e_row, $item->flight);
					$sheet->setCellValue('G'.$e_row, $this->data['types'][$item->type]);
					$sheet->setCellValue('H'.$e_row, $item->total);
					$sheet->setCellValue('J'.$e_row, $item->cartype);
					$sheet->setCellValue('L'.$e_row, $item_driver);
					$sheet->setCellValue('M'.$e_row, $item_car);
				}
				$sheet->getStyle('A9:C'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$sheet->getStyle('H9:I'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$sheet->getStyle('J9:K'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$sheet->getStyle('M9:M'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$e_row++;
				$sheet->setCellValue('A'.$e_row, 'A');
				$sheet->mergeCells('B'.$e_row.':F'.$e_row);
				$sheet->setCellValue('B'.$e_row, 'Tổng cộng ('.$i.' chuyến)');
				$sheet->setCellValue('H'.$e_row, $data['report']['total']);
				$sheet->mergeCells('J'.$e_row.':K'.$e_row);
				$sheet->setCellValue('J'.$e_row, 'Tỷ giá');

				$e_row++;
				$sheet->setCellValue('A'.$e_row, 'B');
				$sheet->mergeCells('B'.$e_row.':F'.$e_row);
				$sheet->setCellValue('B'.$e_row, 'Thuế GTGT (10%)');
				$sheet->setCellValue('H'.$e_row, $data['report']['vat']);
				$sheet->mergeCells('J'.$e_row.':K'.$e_row);

				$e_row++;
				$sheet->setCellValue('A'.$e_row, 'C');
				$sheet->mergeCells('B'.$e_row.':F'.$e_row);
				$sheet->setCellValue('B'.$e_row, 'Tổng giá trị thành tiền');
				$sheet->setCellValue('H'.$e_row, $data['report']['total'] + $data['report']['vat']);
				$sheet->mergeCells('J'.$e_row.':K'.$e_row);

				$sheet->getStyle('A'.($e_row-2).':K'.$e_row)->getFont()->setBold(true);
				$sheet->getStyle('A'.($e_row-2).':G'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$sheet->getStyle('H'.($e_row-2).':I'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$sheet->getStyle('J'.($e_row-2).':K'.$e_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


				$sheet->setBorder('A8:N'.$e_row, 'thin');

				$e_row += 2;
				$e_row_sign = $e_row;

				$sheet->setCellValue('A'.$e_row, 'Ghi chú:');
				foreach ($data['cartypes'] as $key => $cartype)
				{
					$e_row ++;
					$sheet->setCellValue('B'.$e_row, $key);
					$sheet->setCellValue('C'.$e_row, $cartype);
				}

				$sheet->mergeCells('E'.$e_row_sign.':J'.$e_row_sign);
				$sheet->setCellValue('E'.$e_row_sign, 'Ngày '.date('d').' tháng '.date('m').' năm '.date('Y'));
				$sheet->getStyle('E'.$e_row_sign.':J'.$e_row_sign)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$e_row_sign++;
				$sheet->mergeCells('E'.$e_row_sign.':J'.$e_row_sign);
				$sheet->setCellValue('E'.$e_row_sign, 'Kế toán');
				$sheet->getStyle('E'.$e_row_sign.':J'.$e_row_sign)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$e_row_sign += 5;
				$sheet->mergeCells('E'.$e_row_sign.':J'.$e_row_sign);
				$sheet->setCellValue('E'.$e_row_sign, 'Phạm Thị Thu Trinh');
				$sheet->getStyle('E'.$e_row_sign.':J'.$e_row_sign)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			});
		})->store('xlsx')->export('xlsx');
	}

}
