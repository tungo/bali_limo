<?php

class AdminDriverWorkController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = 'Driver';
		$this->data['main_route'] = $this->main_route = 'driver_work';
		$this->data['page_title'] = trans('tn.driver_work');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.admin'), trans('tn.driver_work')];
	}

	public function getIndex()
	{
		// filter
		$filter = ['id', 'full_name', 'time', 'car'];
		$columns = [
			'id'        => 'drivers.id',
			'full_name' => 'drivers.full_name',
			'time'      => 'drivers.time',
			'car'       => 'drivers.car',
			'status'    => 'drivers.status',
			'position'  => 'drivers.position',
		];
		$this->data['filter'] = $this->initFilter($filter, $columns, ['default_sort' => 'position']);
		extract($this->data['filter']);
		// end filter

		$this->data['items'] = Driver::select()
			->searchLike('drivers.id', $id)
			->searchLike('drivers.full_name', $full_name)
			->searchLike('drivers.time', $time)
			->searchLike('drivers.car', $car)
			->orderBy($columns[$sort], $order)
			->paginate($per_page);

		$this->data['breadcrumb'][] = trans('tn.manage');
		return View::make($this->prefix.'.'.$this->main_route.'.index', $this->data);
	}


	public function postSave()
	{
		$this->data['fields'] = ['time', 'car'];
		return parent::postSave();
	}

}
