<?php

class AdminCarController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = 'Car';
		$this->data['main_route'] = $this->main_route = 'car';
		$this->data['page_title'] = trans('tn.car');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.fleet'), trans('tn.car')];

		$this->data['cartypes'] = Cartype::getSelect();
	}

	public function getIndex()
	{
		// filter
		$filter = ['id', 'number', 'cartype_id'];
		$columns = [
			'id'           => 'cars.id',
			'number'       => 'cars.number',
			'cartype'      => 'cartypes.name',
			'status'       => 'cars.status',
			'position'     => 'cars.position',
		];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		$this->data['cartypes'] = Cartype::getSelect(['all']);

		$this->data['items'] = Car::select('cars.*')
			->leftJoin('cartypes', 'cars.cartype_id', '=', 'cartypes.id')
			->with('cartype')
			->searchLike('cars.id', $id)
			->searchLike('cars.number', $number)
			->search('cars.cartype_id', $cartype_id)
			->orderBy($columns[$sort], $order)
			->paginate($per_page);

		$this->data['breadcrumb'][] = trans('tn.manage');
		return View::make($this->prefix.'.'.$this->main_route.'.index', $this->data);
	}

}
