<?php

class AdminAnnoyingClientController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = 'AnnoyingClient';
		$this->data['main_route'] = $this->main_route = 'annoying_client';
		$this->data['page_title'] = trans('tn.annoying_client');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.network'), trans('tn.annoying_client')];

		$this->data['partners'] = Partner::getSelect();
	}

	public function getIndex()
	{
		// filter
		$filter = ['id', 'customer', 'partner_id'];
		$columns = [
			'id'       => 'annoying_clients.id',
			'customer' => 'annoying_clients.customer',
			'partner'  => 'partners.name',
			'status'   => 'annoying_clients.status',
			'position' => 'annoying_clients.position',
		];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		$this->data['partners'] = Partner::getSelect(['all']);

		$this->data['items'] = AnnoyingClient::select('annoying_clients.*')
			->leftJoin('partners', 'annoying_clients.partner_id', '=', 'partners.id')
			->with('partner')
			->searchLike('annoying_clients.id', $id)
			->searchLike('annoying_clients.customer', $customer)
			->search('annoying_clients.partner_id', $partner_id)
			->orderBy($columns[$sort], $order)
			->paginate($per_page);

		$this->data['breadcrumb'][] = trans('tn.manage');
		return View::make($this->prefix.'.'.$this->main_route.'.index', $this->data);
	}

}
