<?php

class AdminPartnerController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = 'Partner';
		$this->data['main_route'] = $this->main_route = 'partner';
		$this->data['page_title'] = trans('tn.partner');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.network'), trans('tn.partner')];

		$this->data['partner_types'] = PartnerType::getSelect();
	}

	public function getIndex()
	{
		// filter
		$filter = ['id', 'name', 'partner_type_id', 'address', 'phone', 'email'];
		$columns = [
			'id'                    => 'partners.id',
			'name'                  => 'partners.name',
			'partner_type'          => 'partner_types.name',
			'rate_pick_up'          => 'partners.rate_pick_up',
			'rate_drop_off'         => 'partners.rate_drop_off',
			'rate_no_show_pick_up'  => 'partners.rate_no_show_pick_up',
			'rate_no_show_drop_off' => 'partners.rate_no_show_drop_off',
			'address'               => 'partners.address',
			'phone'                 => 'partners.phone',
			'email'                 => 'partners.email',
			'status'                => 'partners.status',
			'position'              => 'partners.position',
		];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		$this->data['partner_types'] = PartnerType::getSelect(['all']);

		$this->data['items'] = Partner::select('partners.*')
			->leftJoin('partner_types', 'partners.partner_type_id', '=', 'partner_types.id')
			->with('partner_type')
			->searchLike('partners.id', $id)
			->searchLike('partners.name', $name)
			->search('partners.partner_type_id', $partner_type_id)
			->searchLike('partners.address', $address)
			->searchLike('partners.phone', $phone)
			->searchLike('partners.email', $email)
			->orderBy($columns[$sort], $order)
			->paginate($per_page);

		$this->data['breadcrumb'][] = trans('tn.manage');
		return View::make($this->prefix.'.'.$this->main_route.'.index', $this->data);
	}

}
