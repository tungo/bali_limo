<?php

class AdminContractController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = 'Contract';
		$this->data['main_route'] = $this->main_route = 'contract';
		$this->data['page_title'] = trans('tn.contract');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.admin'), trans('tn.contract')];
	}

	public function getIndex()
	{
		// filter
		$filter = ['id', 'full_name', 'office'];
		$columns = [
			'id'        => 'contracts.id',
			'full_name' => 'contracts.full_name',
			'office'    => 'contracts.office',
			'status'    => 'contracts.status',
			'position'  => 'contracts.position',
		];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		$this->data['items'] = Contract::select()
			->searchLike('contracts.id', $id)
			->searchLike('contracts.full_name', $full_name)
			->searchLike('contracts.office', $office)
			->orderBy($columns[$sort], $order)
			->paginate($per_page);

		$this->data['breadcrumb'][] = trans('tn.manage');
		return View::make($this->prefix.'.'.$this->main_route.'.index', $this->data);
	}

}
