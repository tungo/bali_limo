<?php

class AdminDriverController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = 'Driver';
		$this->data['main_route'] = $this->main_route = 'driver';
		$this->data['page_title'] = trans('tn.driver');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.fleet'), trans('tn.driver')];
	}

	public function getIndex()
	{
		// filter
		$filter = ['id', 'full_name', 'phone'];
		$columns = [
			'id'        => 'drivers.id',
			'full_name' => 'drivers.full_name',
			'phone'     => 'drivers.phone',
			'status'    => 'drivers.status',
			'position'  => 'drivers.position',
		];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		$this->data['items'] = Driver::select()
			->searchLike('drivers.id', $id)
			->searchLike('drivers.full_name', $full_name)
			->searchLike('drivers.phone', $phone)
			->orderBy($columns[$sort], $order)
			->paginate($per_page);

		$this->data['breadcrumb'][] = trans('tn.manage');
		return View::make($this->prefix.'.'.$this->main_route.'.index', $this->data);
	}

}
