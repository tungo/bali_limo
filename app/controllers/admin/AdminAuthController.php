<?php

class AdminAuthController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		$this->data['layout'] = $this->data['theme'].'.login';
		$this->data['main_route'] = $this->main_route = 'auth';
	}

	public function getLogin()
	{
		if (Sentry::check())
    {
      return Redirect::intended($this->path);
    }

		$this->data['page_title'] = trans('login');
		return View::make($this->prefix.'.'.$this->main_route.'.login', $this->data);
	}

	public function postLogin()
	{
		if (Sentry::check())
    {
      return Redirect::intended($this->path);
    }

		try
		{
			$credentials = [
				'email' => Input::get('email'),
				'password' => Input::get('password'),
			];
			$remember_me = (Input::has('remember_me')) ? true : false;

			$user = Sentry::authenticate($credentials, $remember_me);
			if ($user->type != 1)
			{
				Sentry::logout();
				return Redirect::to(URL::previous());
			}
			$this->logged_in_user = $user;
			return Redirect::intended($this->path);
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
			Notification::error(trans('tn.email_field_required'));
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
			Notification::error(trans('tn.password_field_required'));
		}
		catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
		{
			Notification::error(trans('tn.wrong_password'));
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			Notification::error(trans('tn.user_not_found'));
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
			Notification::error(trans('tn.user_not_activated'));
		}

		// The following is only required if the throttling is enabled
		catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
		{
			Notification::error(trans('tn.user_suspended'));
		}
		catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
		{
			Notification::error(trans('tn.user_banned'));
		}

		return Redirect::to($this->path.'/login');
	}

	public function getLogout()
	{
		if (Sentry::check())
		{
			Sentry::logout();
		}
		return Redirect::to($this->path.'/login');
	}

}
