<?php

class AdminPartnerTypeController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = 'PartnerType';
		$this->data['main_route'] = $this->main_route = 'partner_type';
		$this->data['page_title'] = trans('tn.partner_type');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.network'), trans('tn.partner_type')];
	}

	public function getIndex()
	{
		// filter
		$filter = ['id', 'name'];
		$columns = [
			'id'       => 'partner_types.id',
			'name'     => 'partner_types.name',
			'status'   => 'partner_types.status',
			'position' => 'partner_types.position',
		];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		$this->data['items'] = PartnerType::select()
			->searchLike('partner_types.id', $id)
			->searchLike('partner_types.name', $name)
			->orderBy($columns[$sort], $order)
			->paginate($per_page);

		$this->data['breadcrumb'][] = trans('tn.manage');
		return View::make($this->prefix.'.'.$this->main_route.'.index', $this->data);
	}

}
