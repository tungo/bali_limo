<?php

class AdminUserController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = 'User';
		$this->data['main_route'] = $this->main_route = 'user';
		$this->data['page_title'] = trans('tn.user');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.account'), trans('tn.user')];
	}

	public function getIndex()
	{
		// filter
		$filter = ['id', 'email', 'full_name'];
		$columns = [
			'id'         => 'users.id',
			'email'      => 'users.email',
			'full_name'  => 'users.full_name',
			'type'       => 'users.type',
			'partner'    => 'partners.id',
			'car'        => 'cars.id',
			'last_login' => 'users.last_login',
		];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		$this->data['groups'] = Group::getSelect(['all']);
		$this->data['types'] = Config::get('user.type');

		$this->data['items'] = User::select('users.*')
			->leftJoin('partners', 'users.partner_id', '=', 'partners.id')
			->leftJoin('cars', 'users.car_id', '=', 'cars.id')
			->with('partner')
			->with('car')
			->searchLike('users.id', $id)
			->searchLike('users.email', $email)
			->searchLike('users.full_name', $full_name)
			->orderBy($columns[$sort], $order)
			->paginate($per_page);

		$this->data['breadcrumb'][] = trans('tn.manage');
		return View::make($this->prefix.'.'.$this->main_route.'.index', $this->data);
	}

	public function getAdd()
	{
		$this->data['groups_manage'] = Sentry::findAllGroups();
		$this->data['partners'] = Partner::getSelect();
		$this->data['cars'] = Car::getSelect();
		$this->data['types'] = Config::get('user.type');

		$this->data['breadcrumb'][] = trans('tn.add');
		return View::make($this->prefix.'.'.$this->main_route.'.add', $this->data);
	}

	public function postAdd()
	{
		$rules = [
			'email'                 => 'required|email',
			'password'              => 'required',
			'password_confirmation' => 'required|same:password',
		];
		$attributes = [
			'email'                 => trans('tn.email'),
			'password'              => trans('tn.password'),
			'password_confirmation' => trans('tn.password_confirmation'),
		];
		$validator = Validator::make(Input::all(), $rules);
		$validator->setAttributeNames($attributes);

		if ($validator->fails())
		{
			$messages = $validator->messages();
			$error = $this->getValidatorError($messages);
			Notification::error($error);

			return Redirect::to(URL::previous());
		}

		try
		{
			$user_data = [
				'email'     => Input::get('email'),
				'password'  => Input::get('password'),
				'full_name' => Input::get('full_name'),
				'activated' => true,
				'type'      => Input::get('type'),
			];

			if (Input::get('type') == 2)
			{
				$user_data['partner_id'] = Input::get('partner_id');
			}
			elseif (Input::get('type') == 3)
			{
				$user_data['car_id'] = Input::get('car_id');
			}

			$user = Sentry::createUser($user_data);

			if ($user)
			{
				$groups = Input::has('groups') ? Input::get('groups') : [];
				$groups_manage = Sentry::findAllGroups();
				foreach ($groups_manage as $group)
				{
					if (array_key_exists($group->id, $groups))
					{
						$user->addGroup($group);
					}
				}
				Notification::success(trans('tn.succeeded'));
			}
			else
			{
				Notification::success(trans('tn.failed'));
			}
			$redirect = URL::route($this->prefix.'.'.$this->main_route);
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
			Notification::error(trans('tn.email_field_required'));
			$redirect = URL::previous();
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
			Notification::error(trans('tn.password_field_required'));
			$redirect = URL::previous();
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
			Notification::error(trans('tn.email_exists'));
			$redirect = URL::previous();
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
			Notification::error(trans('tn.group_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function getEdit($id)
	{
		try
		{
			$this->data['item'] = Sentry::findUserById($id);
			$this->data['groups_manage'] = Sentry::findAllGroups();
			$this->data['partners'] = Partner::getSelect();
			$this->data['cars'] = Car::getSelect();
			$this->data['types'] = Config::get('user.type');

			$this->data['breadcrumb'][] = trans('tn.edit');
			return View::make($this->prefix.'.'.$this->main_route.'.edit', $this->data);
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function postEdit($id)
	{
		$rules = [
			'email'                 => 'required|email',
			'password_confirmation' => 'same:password',
		];
		$attributes = [
			'email'                 => trans('tn.email'),
			'password_confirmation' => trans('tn.password_confirmation'),
		];
		$validator = Validator::make(Input::all(), $rules);
		$validator->setAttributeNames($attributes);

		if ($validator->fails())
		{
			$messages = $validator->messages();
			$error = $this->getValidatorError($messages);
			Notification::error($error);

			return Redirect::to(URL::previous());
		}

		try
		{
			$user = Sentry::findUserById($id);

			$groups = Input::has('groups') ? Input::get('groups') : [];
			$groups_manage = Sentry::findAllGroups();
			foreach ($groups_manage as $group)
			{
				if (array_key_exists($group->id, $groups))
				{
					$user->addGroup($group);
				}
				else
				{
					$user->removeGroup($group);
				}
			}

			if (Input::has('password'))
			{
				$user->password = Input::get('password');
			}
			$user->email     = Input::get('email');
			$user->full_name = Input::get('full_name');
			$user->type      = Input::get('type');

			$user->partner_id = 0;
			$user->car_id     = 0;
			if (Input::get('type') == 2)
			{
				$user->partner_id = Input::get('partner_id');
			}
			elseif (Input::get('type') == 3)
			{
				$user->car_id = Input::get('car_id');
			}

			if ($user->save())
			{
				Notification::success(trans('tn.succeeded'));
			}
			else
			{
				Notification::error(trans('tn.failed'));
			}
			$redirect = URL::route($this->prefix.'.'.$this->main_route);
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
			Notification::error(trans('tn.email_exists'));
			$redirect = URL::previous();
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

}
