<?php

class AdminLogController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = 'LogM';
		$this->data['main_route'] = $this->main_route = 'log';
		$this->data['page_title'] = trans('tn.log');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.admin'), trans('tn.log')];
	}

	public function getIndex()
	{
		// filter
		$filter = ['id', 'user_id', 'route', 'type', 'action', 'url', 'ip', 'date_from', 'date_to'];
		$columns = [
			'id'     => 'logs.id',
			'user'   => 'users.full_name',
			'type'   => 'logs.type',
			'action' => 'logs.action',
			'url'    => 'logs.url',
			'at'     => 'logs.created_at',
		];
		$this->data['filter'] = $this->initFilter($filter, $columns);
		extract($this->data['filter']);
		// end filter

		$this->data['users'] = User::getSelect(['all']);

		$this->data['items'] = LogM::select('logs.*')
			->leftJoin('users', 'users.id', '=', 'logs.user_id')
			->with('user')
			->searchLike('logs.id', $id)
			->search('logs.user_id', $user_id)
			->searchLike('logs.route', $route)
			->searchLike('logs.type', $type)
			->searchLike('logs.action', $action)
			->searchLike('logs.url', $url)
			->searchLike('logs.ip', $ip)
			->searchBetween('logs.created_at', fn_datetime($date_from.' 00:00:00', 'd-m-Y H:i:s', 'Y-m-d  H:i:s', null), fn_datetime($date_to.' 23:59:59', 'd-m-Y H:i:s', 'Y-m-d H:i:s', null))
			->orderBy($columns[$sort], $order)
			->paginate($per_page);

		return View::make($this->prefix.'.'.$this->main_route.'.index', $this->data);
	}

}
