<?php

class AdminController extends BackendController {

	public function __construct()
	{
		parent::__construct();

		$this->beforeFilter('@auth');
		$this->beforeFilter('@account_type');
		$this->beforeFilter('@permission');
		$this->afterFilter('@log');

		$this->beforeFilter('csrf', ['on' => 'post']);

		$this->data['prefix'] = $this->prefix = Config::get('admin.prefix');

		parent::_init();

		$this->data['notifications'] = BookingNotification::getList($this->prefix);
		$this->data['driver_works'] = Driver::getWorks();
		$this->data['car_works'] = Car::getWorks();
	}

	// kiểm tra loại tk sau khi đăng nhập
	public function account_type($route, $request)
	{
		if (Sentry::check())
		{
			$user = Sentry::getUser();
			if ($user->type != Config::get('user.type_id.admin'))
			{
				return $this->view404();
			}
		}
	}

}
