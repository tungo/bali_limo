<?php

class CarSystemController extends CarController {

	public function getIndex()
	{
		return Redirect::to(URL::route($this->prefix.'.booking'));
	}

}
