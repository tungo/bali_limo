<?php

class CarBookingController extends CarController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = 'Booking';
		$this->data['main_route'] = $this->main_route = 'booking';
		$this->data['page_title'] = trans('tn.booking');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.booking')];
	}

	public function getIndex()
	{
		// filter
		$filter = [
			'date_from',
			'date_to',
			'time_from',
			'time_to',
			'partner_id',
			'customer_name',
			'customer_amount',
			'type',
			'flight',
			'cartype',
			'status_id',
			'driver_id',
			'car_id',
			'total_from',
			'total_to',

			'ids',
		];
		$columns = [
			'id'              => 'bookings.id',
			'date'            => 'bookings.date',
			'time'            => 'bookings.time',
			'partner'         => 'partners.name',
			'customer_name'   => 'bookings.customer_name',
			'customer_amount' => 'bookings.customer_amount',
			'type'            => 'bookings.type',
			'flight'          => 'bookings.flight',
			'cartype'         => 'bookings.cartype',
			'remark'          => 'bookings.remark',
			'status'          => 'booking_statuses.name',
			'driver'          => 'drivers.full_name',
			'car'             => 'cars.number',
			'total'           => 'bookings.total',
		];
		$this->data['filter'] = $this->initFilter($filter, $columns, ['except' => ['ids'], 'default_sort' => 'date']);
		extract($this->data['filter']);
		// end filter

		$this->data['types'] = Booking::getTypes(['all']);
		$this->data['partners'] = Partner::getSelect(['all']);
		$this->data['cars'] = Car::getSelect(['all']);
		$this->data['drivers'] = Driver::getSelect(['all']);
		$this->data['statuses'] = BookingStatus::getSelect(['all']);

		$this->data['items'] = Booking::select('bookings.*')
			->leftJoin('partners', 'bookings.partner_id', '=', 'partners.id')
			->leftJoin('drivers', 'bookings.driver_id', '=', 'drivers.id')
			->leftJoin('cars', 'bookings.car_id', '=', 'cars.id')
			->leftJoin('booking_statuses', 'bookings.status_id', '=', 'booking_statuses.id')
			->with('partner')
			->with('driver')
			->with('car')
			->with('status')
			->searchBetween('bookings.date', null, date('Y-m-d'))
			->searchBetween('bookings.time', fn_datetime($time_from, 'H:i', 'H:i:s', null), fn_datetime($time_to, 'H:i', 'H:i:s', null))
			->search('bookings.partner_id', $partner_id)
			->searchLike('bookings.customer_name', $customer_name)
			->search('bookings.customer_amount', $customer_amount)
			->search('bookings.type', $type)
			->searchLike('bookings.flight', $flight)
			->searchLike('bookings.cartype', $cartype)
			->searchIn('bookings.status_id', [Config::get('booking.status_id.car_requested'), Config::get('booking.status_id.on_duty'), Config::get('booking.status_id.issue'), Config::get('booking.status_id.finished')])
			->search('bookings.driver_id', $driver_id)
			->search('bookings.car_id', $this->logged_in_user->car_id)
			->searchBetween('bookings.total', $total_from, $total_to)
			->searchIn('bookings.id', explode(',', $ids))
			->orderBy($columns[$sort], $order)
			->orderBy('bookings.time', 'asc')
			->get();

		$this->data['breadcrumb'][] = trans('tn.manage');
		return View::make($this->prefix.'.'.$this->main_route.'.index', $this->data);
	}

	public function postOnDuty($id)
	{
		return $this->updateStatus($id, 'on_duty');
	}

	public function postIssue($id)
	{
		return $this->updateStatus($id, 'issue');
	}

	public function getFinished($id)
	{
		try
		{
			$this->data['item'] = Booking::with('partner')->with('driver')->with('car')->with('status')->findOrFail($id);

			if (!in_array($this->data['item']->status_id, Config::get('booking.may.finished')))
			{
				Notification::error(trans('tn.items_unable_to_update'));
				return Redirect::to(URL::previous());
			}

			$this->data['types'] = Booking::getTypes();

			$file_location = public_path('files/signature');
			$this->data['signature_path'] = public_path('files/signature');
			$this->data['signature_url'] =  FILES_URL.'/signature';
			$this->data['customer_signature'] =  $id.'_customer.png';
			$this->data['partner_signature'] =  $id.'_partner.png';

			if ($this->data['item']->car_id != $this->logged_in_user->car_id)
			{
				Notification::error(trans('tn.info_not_found'));
				return Redirect::to(URL::previous());
			}

			$this->data['notification_allowed'] = false;

			return View::make($this->prefix.'.'.$this->main_route.'.finished', $this->data);
		}
		catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function postFinished($id)
	{
		return $this->updateStatus($id, 'finished', false, URL::route($this->prefix.'.'.$this->main_route));
	}

	public function postSign($id)
	{
		try
		{
			$this->data['item'] = Booking::with('partner')->with('driver')->with('car')->with('status')->findOrFail($id);

			if ($this->data['item']->car_id != $this->logged_in_user->car_id)
			{
				echo json_encode(['result' => 'error', 'error' => trans('tn.info_not_found')]);
				exit;
			}

			if (!Input::has('type') || !in_array(Input::get('type'), ['customer', 'partner']))
			{
				echo json_encode(['result' => 'error', 'error' => trans('tn.wrong_data')]);
				exit;
			}

			$file_location = public_path('files/signature');
			$signature_type = Input::get('type');
			$file_name = $id.'_'.$signature_type.'.png';
			$file = $file_location.'/'.$file_name;

			if (File::exists($file))
			{
				echo json_encode(['result' => 'error', 'error' => trans('tn.already_signed')]);
				exit;
			}

			$data_uri = Input::get('signature');
			$data_pieces = explode(',', $data_uri);
			$encoded_image = $data_pieces[1];
			$decoded_image = base64_decode($encoded_image);

			$bytes_written = File::put($file, $decoded_image);
			if ($bytes_written === false)
			{
				echo json_encode(['result' => 'error', 'error' => trans('tn.fail')]);
				exit;
			}

			echo json_encode(['result' => 'success', 'file' => URL::asset('public/files/signature/'.$file_name)]);
			exit;
		}
		catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
		{
			echo json_encode(['result' => 'error', 'error' => trans('tn.info_not_found')]);
			exit;
		}
	}

	public function postReview($id)
	{
		try
		{
			$this->data['item'] = Booking::with('partner')->with('driver')->with('car')->with('status')->findOrFail($id);

			if ($this->data['item']->car_id != $this->logged_in_user->car_id)
			{
				echo json_encode(['result' => 'error', 'error' => trans('tn.info_not_found')]);
				exit;
			}

			$comment = Input::has('comment') ? Input::get('comment') : '';
			$rating = Input::has('rating') ? Input::get('rating') : 0;

			$data = [
				'comment' => $comment,
				'rating' => $rating,
			];

			try
			{
				Booking::insertData($id, Config::get('booking.data_types.review'), $data);

				echo json_encode(['result' => 'success']);
				exit;
			}
			catch (\Illuminate\Database\QueryException $e)
			{
				echo json_encode(['result' => 'error', 'error' => trans('tn.fail')]);
				exit;
			}
		}
		catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
		{
			echo json_encode(['result' => 'error', 'error' => trans('tn.info_not_found')]);
			exit;
		}
	}

	// ----- ----- ----- ----- -----

	public function updateStatus($id, $status, $reason = false, $redirect = false)
	{
		try
		{
			$item = Booking::findOrFail($id);

			if (!in_array($item->status_id, Config::get('booking.may.'.$status)))
			{
				Notification::error(trans('tn.items_unable_to_update'));
				return Redirect::to(URL::previous());
			}

			if ($item->updateStatus($status))
			{
				Notification::success(trans('tn.succeeded'));

				$this->logUpdateStatus([$id], $status);
				$this->notificationUpdateStatus([$id], $status);
				$this->carworkUpdateStatus([$id], $status);
			}
			else
			{
				Notification::error(trans('tn.failed'));
			}
			if (!$redirect)
			{
				$redirect = URL::previous();
			}
		}
		catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function logUpdateStatus($ids, $status, $log_data = [], $options = [])
	{
		return Booking::logUpdateStatus($ids, Config::get('booking.status_id.'.$status), $log_data, $options);
	}

	public function notificationUpdateStatus($ids, $status, $noti_data = [])
	{
		$partners = [];
		$error_ids = [];
		$status_id = Config::get('booking.status_id.'.$status);

		foreach ($ids as $id)
		{
			try
			{
				$item = Booking::findOrFail($id);

				if ($item->partner_id)
				{
					$partners[$item->partner_id][] = $id;
				}
			}
			catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
			{
				$error_ids[] = $id;
			}
		}

		if (!empty($error_ids))
		{
			$noti_data['error_ids'] = implode(',', $error_ids);
		}

		$general_event_data = [
			'status'   => trans('tn.'.$status),
			'color'    => Config::get('booking.color.'.$status_id),
			'user'     => $this->logged_in_user->full_name,
			'datetime' => date('d-m-Y H:i:s'),
		];

		if (!empty($ids))
		{
			$prefix = Config::get('admin.prefix');
			$user_type = Config::get('user.type_id.admin');
			$event = Config::get('admin.event.status_update');

			Booking::notificationUpdateStatus($ids, $status_id, $user_type, false, $noti_data);

			$event_data = $general_event_data + [
				'type' => $user_type,
				'ids'  => $ids,
				'link' => URL::route($prefix.'.booking', ['ids' => implode(',',  $ids)]),
			];
			Event::fire($event, array(json_encode($event_data)));

			$prefix = Config::get('car.prefix');
			$user_type = Config::get('user.type_id.car');
			$event = Config::get('car.event.status_update');

			Booking::notificationUpdateStatus($ids, $status_id, $user_type, $this->user_car_id, $noti_data);

			$event_data = $general_event_data + [
				'type'    => $user_type,
				'type_id' => $this->user_car_id,
				'ids'     => $ids,
				'link'    => URL::route($prefix.'.booking', ['ids' => implode(',',  $ids)]),
			];
			Event::fire($event, array(json_encode($event_data)));
		}

		if (!empty($partners))
		{
			$prefix = Config::get('partner.prefix');
			$user_type = Config::get('user.type_id.partner');
			$event = Config::get('partner.event.status_update');

			foreach ($partners as $partner_id => $booking_ids)
			{
				Booking::notificationUpdateStatus($booking_ids, $status_id, $user_type, $partner_id, $noti_data);

				$event_data = $general_event_data + [
					'type'    => $user_type,
					'type_id' => $partner_id,
					'ids'     => $booking_ids,
					'link'    => URL::route($prefix.'.booking', ['ids' => implode(',',  $booking_ids)]),
				];
				Event::fire($event, array(json_encode($event_data)));
			}
		}
	}

	public function carworkUpdateStatus($ids, $status)
	{
		$car_works = Config::get('booking.car_works');
		if (in_array($status, $car_works['y']))
		{
			$event_data = ['on_duty' => 1];
		}
		elseif (in_array($status, $car_works['n']))
		{
			$event_data = ['on_duty' => 0];
		}
		else
		{
			return false;
		}
		$event_data['ids'] = Car::getIdsByBookings($ids);
		Event::fire(Config::get('admin.event.car_work'), array(json_encode($event_data)));
	}

}
