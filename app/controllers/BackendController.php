<?php

class BackendController extends BaseController {

	public $data = [];
	public $main_model = '';
	public $main_route = '';
	public $prefix = '';
	public $path = '';
	public $logged_in_user = null;

	public function __construct()
	{

	}

	public function _init()
	{
		$this->data['path'] = $this->path = Config::get($this->prefix.'.path');
		$this->data['theme'] = 'themes.'.Config::get($this->prefix.'.theme');
		$this->data['layout'] = $this->data['theme'].'.'.Config::get($this->prefix.'.layout');
		$this->data['page_title'] = '';
		$this->data['navigation'] = Config::get($this->prefix.'.navigation');
		$this->data['websocket_server'] = URL::to('/').':'.Config::get('bali.websocket_port');

		if (Sentry::check())
		{
			$user = Sentry::getUser();
			$user->level = User::getLevel($user->groups);
			$this->data['logged_in_user'] = $this->logged_in_user = $user;
		}
	}

	// kiểm tra đăng nhập
	public function auth($route, $request)
	{
		$action = $route->getAction();
		$as = preg_replace('/'.$this->prefix.'\./', '', $action['as'], 1);
		if (in_array($as, Config::get($this->prefix.'.routes_free_auth')))
		{
			return;
		}

		if (!Sentry::check())
		{
			return Redirect::guest($this->path.'/login');
		}
	}

	// kiểm tra quyền hạn
	public function permission($route, $request)
	{
		$action = $route->getAction();
		$as = preg_replace('/'.$this->prefix.'\./', '', $action['as'], 1);
		if (in_array($as, Config::get($this->prefix.'.routes_free_permission')))
		{
			return;
		}

		// && !$user->inGroup(Sentry::findGroupByName('Developer'))
		$user = Sentry::getUser();
		if (!$user->hasAccess($as) && $user->id != 1)
		{
			Notification::error(trans('tn.can_not_access'));
			return Redirect::to($this->path);
		}
	}

	public function log($route, $request, $response)
	{
		$action = $route->getAction();
		$as = preg_replace('/'.$this->prefix.'\./', '', $action['as'], 1);
		if (in_array($as, Config::get('bali.actions')))
		{
			$input = Input::all();
			unset($input['password']);
			unset($input['password_confirmation']);
			unset($input['file']);
			unset($input['excel']);

			$route = explode('.', $action['as']);
			$data = [
				'user_id' => (!empty($this->logged_in_user)) ? $this->logged_in_user->id : 0,
				'route'   => $action['as'],
				'url'     => Request::url(),
				'type'    => isset($route[1]) ? $route[1] : '',
				'input'   => serialize($input),
				'ip'      => Request::getClientIp(),
				'action'  => isset($route[2]) ? $route[2] : '',
			];
			$log = new LogM($data);
			$log->save();
		}
	}

	// hàm xử lý các biến trong công cụ quản lý
	protected function initFilter($filter, $columns, $options = [])
	{
		$search = null;
		$except = isset($options['except']) ? $options['except'] : [];
		foreach ($filter as $input)
		{
			$$input = (Input::has($input)) ? Input::get($input) : null;
			if (in_array($input, $except)) continue;
			if (Input::has($input)) $search = 1;
		}

		$default_sort = (isset($options['default_sort'])) ? $options['default_sort'] : 'id';

		$sort = (array_key_exists(Input::get('sort'), $columns)) ? Input::get('sort') : $default_sort;
		$order = (Input::get('order') === 'asc') ? 'asc' : 'desc';

		$per_page = Input::has('per_page')
			? (Input::get('per_page') <= 100 ? Input::get('per_page') : 100)
			: (Session::has('per_page') ? Session::get('per_page') : Config::get($this->prefix.'.items_per_page'));
		Session::put('per_page', $per_page);

		array_push($filter, 'sort', 'order', 'per_page', 'search');
		return compact($filter);
	}

	public function getAdd()
	{
		$this->data['breadcrumb'][] = trans('tn.add');
		return View::make($this->prefix.'.'.$this->main_route.'.add', $this->data);
	}

	public function postAdd()
	{
		$model = $this->main_model;
		$item = new $model(Input::all());
		if ($item->save())
		{
			Notification::success(trans('tn.succeeded'));
		}
		else
		{
			Notification::error(trans('tn.failed'));
		}
		$redirect = URL::route($this->prefix.'.'.$this->main_route);
		return Redirect::to($redirect);
	}

	public function getView($id)
	{
		try
		{
			$model = $this->main_model;
			$this->data['item'] = $model::findOrFail($id);

			$this->data['breadcrumb'][] = trans('tn.view');
			return View::make($this->prefix.'.'.$this->main_route.'.view', $this->data);
		}
		catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function getEdit($id)
	{
		try
		{
			$model = $this->main_model;
			$this->data['item'] = $model::findOrFail($id);

			$this->data['breadcrumb'][] = trans('tn.edit');
			return View::make($this->prefix.'.'.$this->main_route.'.edit', $this->data);
		}
		catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function postEdit($id)
	{
		try
		{
			$model = $this->main_model;
			$item = $model::findOrFail($id);
			if ($item->update(Input::all()))
			{
				Notification::success(trans('tn.succeeded'));
			}
			else
			{
				Notification::error(trans('tn.failed'));
			}
			$redirect = URL::route($this->prefix.'.'.$this->main_route);
		}
		catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function postSave()
	{
		try
		{
			$fields = !empty($this->data['fields']) ? $this->data['fields'] : ['position'];
			if ($this->updateTable('field', $fields, 'hid'))
			{
				Notification::success(trans('tn.succeeded'));
			}
			else
			{
				Notification::error(trans('tn.failed'));
			}
			$redirect = URL::previous();
		}
		catch (TnExceptions\InfoNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function getDelete($id)
	{
		try
		{
			$model = $this->main_model;
			$item = $model::findOrFail($id);
			if ($item->delete())
			{
				Notification::success(trans('tn.succeeded'));
			}
			else
			{
				Notification::error(trans('tn.failed'));
			}
			$redirect = URL::previous();
		}
		catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		catch (TnExceptions\RelationRecordExistException $e)
		{
			Notification::error(trans('tn.relation_record_exist').': '.trans('tn.'.$e->getMessage()));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function postDelete()
	{
		try
		{
			if ($this->updateTable('delete'))
			{
				Notification::success(trans('tn.succeeded'));
			}
			else
			{
				Notification::error(trans('tn.failed'));
			}
			$redirect = URL::previous();
		}
		catch (TnExceptions\InfoNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function getEnable($id)
	{
		try
		{
			$model = $this->main_model;
			$item = $model::findOrFail($id);
			if ($item->enable())
			{
				Notification::success(trans('tn.succeeded'));
			}
			else
			{
				Notification::error(trans('tn.failed'));
			}
			$redirect = URL::previous();
		}
		catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function getDisable($id)
	{
		try
		{
			$model = $this->main_model;
			$item = $model::findOrFail($id);
			if ($item->disable())
			{
				Notification::success(trans('tn.succeeded'));
			}
			else
			{
				Notification::error(trans('tn.failed'));
			}
			$redirect = URL::previous();
		}
		catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function postEnable()
	{
		try
		{
			if ($this->updateTable('data', ['status' => 1]))
			{
				Notification::success(trans('tn.succeeded'));
			}
			else
			{
				Notification::error(trans('tn.failed'));
			}
			$redirect = URL::previous();
		}
		catch (TnExceptions\InfoNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function postDisable()
	{
		try
		{
			if ($this->updateTable('data', ['status' => 0]))
			{
				Notification::success(trans('tn.succeeded'));
			}
			else
			{
				Notification::error(trans('tn.failed'));
			}
			$redirect = URL::previous();
		}
		catch (TnExceptions\InfoNotFoundException $e)
		{
			Notification::error(trans('tn.info_not_found'));
			$redirect = URL::previous();
		}
		return Redirect::to($redirect);
	}

	public function getValidatorError($messages)
	{
		$html = '<ul>';
		foreach ($messages->all('<li>:message</li>') as $message)
		{
			$html .= $message;
		}
		$html .= '</ul>';
		return $html;
	}

	protected function updateTable($type = null, $data = null, $ids_input = 'cid')
	{
		$ids = Input::has($ids_input) ? Input::get($ids_input) : [];

		if (!is_array($ids) || !count($ids))
		{
			throw new TnExceptions\InfoNotFoundException($ids_input);
			return false;
		}

		$model = $this->main_model;

		// nếu loại là data thì update theo data
		if ($type == 'data')
		{
			if ($model::whereIn('id', $ids)->update($data))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		// nếu loại là field thì update theo input được post
		elseif ($type == 'field')
		{
			foreach ($ids as $id)
			{
				$data_update = [];
				foreach ($data as $field)
				{
					$data_update[$field] = Input::get($field.'.'.$id);
				}
				$model::find($id)->update($data_update);
			}
			return true;
		}
		elseif ($type == 'delete')
		{
			if ($model::destroy($ids))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public function view404()
	{
		// return View::make($this->data['theme'].'.404');
		return Response::view('error/404');
	}
}
