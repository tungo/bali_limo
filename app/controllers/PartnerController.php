<?php

class PartnerController extends BackendController {

	public $user_partner_id = 0;

	public function __construct()
	{
		parent::__construct();

		$this->beforeFilter('@auth');
		$this->beforeFilter('@account_type');
		$this->beforeFilter('@permission');
		$this->afterFilter('@log');

		$this->beforeFilter('csrf', ['on' => 'post']);

		$this->data['prefix'] = $this->prefix = Config::get('partner.prefix');

		parent::_init();

		if (!empty($this->logged_in_user))
		{
			$this->user_partner_id = $this->data['user_partner_id'] = $this->logged_in_user->partner_id;
		}
		$this->data['notifications'] = BookingNotification::getList($this->prefix, $this->user_partner_id);
	}

	// kiểm tra loại tk sau khi đăng nhập
	public function account_type($route, $request)
	{
		if (Sentry::check())
		{
			$user = Sentry::getUser();
			if (empty($user->partner_id) || $user->type != Config::get('user.type_id.partner'))
			{
				return $this->view404();
			}
			if ( ! Partner::checkActive($user->partner_id))
			{
				return $this->view404();
			}
		}
	}

}
