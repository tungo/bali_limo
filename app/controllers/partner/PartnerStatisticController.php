<?php

class PartnerStatisticController extends PartnerController {

	public function __construct()
	{
		parent::__construct();
		$this->main_model = '';
		$this->data['main_route'] = $this->main_route = 'statistic';
		$this->data['page_title'] = trans('tn.statistic');
		$this->data['breadcrumb'] = [trans('tn.homepage'), trans('tn.statistic')];
	}

	public function getIndex()
	{
		$all_item = Booking::select()
			->search('bookings.partner_id', $this->logged_in_user->partner_id)
			->searchIn('bookings.status_id', array_keys(Config::get('booking.done')));

		$this->data['all']['count'] = $all_item->count();
		$this->data['all']['total'] = $all_item->sum('total');


		$today = date('Y-m-d');
		$today_item = Booking::select()
			->search('bookings.partner_id', $this->logged_in_user->partner_id)
			->searchBetween('bookings.done_at', $today.' 00:00:00', $today.' 23:59:59')
			->searchIn('bookings.status_id', array_keys(Config::get('booking.done')));

		$this->data['today']['count'] = $today_item->count();
		$this->data['today']['total'] = $today_item->sum('total');


		$yesterday = date("Y-m-d", time() - 60 * 60 * 24);
		$yesterday_item = Booking::select()
			->search('bookings.partner_id', $this->logged_in_user->partner_id)
			->searchBetween('bookings.done_at', $yesterday.' 00:00:00', $yesterday.' 23:59:59')
			->searchIn('bookings.status_id', array_keys(Config::get('booking.done')));

		$this->data['yesterday']['count'] = $yesterday_item->count();
		$this->data['yesterday']['total'] = $yesterday_item->sum('total');


		$day = date('w');
		$week_start = date('Y-m-d', strtotime('-'.$day.' days'));
		$week_end = date('Y-m-d', strtotime('+'.(6 - $day).' days'));

		$week_item = Booking::select()
			->search('bookings.partner_id', $this->logged_in_user->partner_id)
			->searchBetween('bookings.done_at', $week_start.' 00:00:00', $week_end.' 23:59:59')
			->searchIn('bookings.status_id', array_keys(Config::get('booking.done')));

		$this->data['week']['count'] = $week_item->count();
		$this->data['week']['total'] = $week_item->sum('total');


		$this_month = date('m-Y');
		list($start_month, $end_month) = fn_datetime_get_month_date($this_month);

		$month_item = Booking::select()
			->search('bookings.partner_id', $this->logged_in_user->partner_id)
			->searchBetween('bookings.done_at', $start_month.' 00:00:00', $end_month.' 23:59:59')
			->searchIn('bookings.status_id', array_keys(Config::get('booking.done')));

		$this->data['month']['count'] = $month_item->count();
		$this->data['month']['total'] = $month_item->sum('total');


		$total_this_month = Booking::select(DB::raw('sum(total) as sum_total, day(date) as day'))
			->search('bookings.partner_id', $this->logged_in_user->partner_id)
			->searchBetween('bookings.date', $start_month, $end_month)
			->searchIn('bookings.status_id', array_keys(Config::get('booking.done')))
			->groupBy('day')
			->get();

		$last_date_this_month = date('t');
		$chart_total_this_month = array_fill(1, $last_date_this_month, 0);

		foreach ($total_this_month as $item)
		{
			$chart_total_this_month[$item->day] = $item->sum_total;
		}
		$this->data['chart_total_this_month'] = $chart_total_this_month;


		$this_year = date('Y');
		$start_year = $this_year.'-01-01 00:00:00';
		$end_year = $this_year.'-12-31 00:00:00';

		$year_item = Booking::select()
			->search('bookings.partner_id', $this->logged_in_user->partner_id)
			->searchBetween('bookings.done_at', $start_year, $end_year)
			->searchIn('bookings.status_id', array_keys(Config::get('booking.done')));

		$this->data['year']['count'] = $year_item->count();
		$this->data['year']['total'] = $year_item->sum('total');


		$total_this_year = Booking::select(DB::raw('sum(total) as sum_total, month(date) as month'))
			->search('bookings.partner_id', $this->logged_in_user->partner_id)
			->searchBetween('bookings.date', $start_year, $end_year)
			->searchIn('bookings.status_id', array_keys(Config::get('booking.done')))
			->groupBy('month')
			->get();

		$chart_total_this_year = array_fill(1, 12, 0);

		foreach ($total_this_year as $item)
		{
			$chart_total_this_year[$item->month] = $item->sum_total;
		}
		$this->data['chart_total_this_year'] = $chart_total_this_year;


		$this->data['breadcrumb'][] = trans('tn.general');
		return View::make($this->prefix.'.'.$this->main_route.'.index', $this->data);
	}

	public function getDetail()
	{
		// filter
		$filter = [
			'date_from',
			'date_to',
			'status_id',

			'ids',
		];
		$columns = [
			'id'              => 'bookings.id',
			'date'            => 'bookings.date',
			'partner'         => 'partners.name',
			'customer_name'   => 'bookings.customer_name',
			'customer_amount' => 'bookings.customer_amount',
			'type'            => 'bookings.type',
			'time'            => 'bookings.time',
			'flight'          => 'bookings.flight',
			'cartype'         => 'bookings.cartype',
			'remark'          => 'bookings.remark',
			'status'          => 'bookings_statuses.name',
			'driver'          => 'drivers.full_name',
			'car'             => 'cars.number',
			'total'           => 'bookings.total',
		];
		$this->data['filter'] = $this->initFilter($filter, $columns, ['except' => ['ids'], 'default_sort' => 'date']);
		extract($this->data['filter']);
		// end filter

		if (empty($date_from))
		{
			$this->data['filter']['date_from'] = $date_from = date('d-m-Y');
		}
		if (empty($date_to))
		{
			$this->data['filter']['date_to'] = $date_to = date('d-m-Y');
		}
		$this->data['per_page'] = $per_page = 30;

		$this->data['types'] = Booking::getTypes(['all']);
		$this->data['partners'] = Partner::getSelect(['all']);
		$this->data['cars'] = Car::getSelect(['all']);
		$this->data['drivers'] = Driver::getSelect(['all']);
		$this->data['statuses'] = BookingStatus::getDoneSelect(['all']);

		$regular = Booking::select('bookings.*')
			->leftJoin('partners', 'bookings.partner_id', '=', 'partners.id')
			->leftJoin('drivers', 'bookings.driver_id', '=', 'drivers.id')
			->leftJoin('cars', 'bookings.car_id', '=', 'cars.id')
			->leftJoin('booking_statuses', 'bookings.status_id', '=', 'booking_statuses.id')
			->with('partner')
			->with('driver')
			->with('car')
			->with('status')
			->searchBetween('bookings.done_at', fn_datetime_start_day($date_from), fn_datetime_end_day($date_to))
			->search('bookings.status_id', $status_id)
			->search('bookings.partner_id', $this->logged_in_user->partner_id)
			->searchIn('bookings.id', explode(',', $ids))
			->searchIn('bookings.status_id', [Config::get('booking.status_id.cancel'), Config::get('booking.status_id.no_show'), Config::get('booking.status_id.completed'), Config::get('booking.status_id.free_of_charge')])
			->orderBy($columns[$sort], $order);

		$this->data['items'] = $regular->get();
		$this->data['statistic'] = Booking::calcStatistic($this->data['items']);

		$this->data['breadcrumb'][] = trans('tn.detail');
		return View::make($this->prefix.'.'.$this->main_route.'.detail', $this->data);
	}

}
