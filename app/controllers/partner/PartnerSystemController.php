<?php

class PartnerSystemController extends PartnerController {

	public function __construct()
	{
		parent::__construct();
		$this->data['main_route'] = $this->main_route = 'system';
	}

	public function getIndex()
	{
		$this->data['main_route'] = $this->main_route = 'dashboard';
		$this->data['page_title'] = trans('tn.dashboard');

		$today = date('Y-m-d');

		$this->data['new_booking'] = Booking::select()
			->search('bookings.partner_id', $this->logged_in_user->partner_id)
			->searchBetween('bookings.created_at', $today.' 00:00:00', $today.' 23:59:59')
			->count();

		$this->data['booking_proceed'] = Booking::select()
			->search('bookings.partner_id', $this->logged_in_user->partner_id)
			->search('bookings.date', $today)
			->count();

		$this->data['booking_done'] = Booking::select()
			->search('bookings.partner_id', $this->logged_in_user->partner_id)
			->searchBetween('bookings.done_at', $today.' 00:00:00', $today.' 23:59:59')
			->count();

		$this_month = date('m-Y');
		list($start_month, $end_month) = fn_datetime_get_month_date($this_month);

		$bookings_this_month = Booking::select(DB::raw('count(*) as number, day(date) as day'))
			->search('bookings.partner_id', $this->logged_in_user->partner_id)
			->searchBetween('bookings.date', $start_month, $end_month)
			->groupBy('day')
			->get();

		$last_date_this_month = date('t');
		$chart_bookings_this_month = array_fill(1, $last_date_this_month, 0);

		foreach ($bookings_this_month as $item)
		{
			$chart_bookings_this_month[$item->day] = $item->number;
		}
		$this->data['chart_bookings_this_month'] = $chart_bookings_this_month;

		$process_bookings = Booking::select(DB::raw('count(*) as number, status_id'))
			->search('bookings.partner_id', $this->logged_in_user->partner_id)
			->whereNotIn('status_id', [Config::get('booking.status_id.cancel'), Config::get('booking.status_id.no_show'), Config::get('booking.status_id.completed'), Config::get('booking.status_id.free_of_charge')])
			->groupBy('status_id')
			->get();

		$chart_process_bookings = [];
		foreach ($process_bookings as $item)
		{
			$chart_item = new StdClass();
			$chart_item->value = $item->number;
			$chart_item->color = Config::get('booking.color_hex.'.$item->status_id);
			$chart_item->label = trans('tn.'.Config::get('booking.statuses.'.$item->status_id));

			$chart_process_bookings[] = $chart_item;
		}
		$this->data['chart_process_bookings'] = $chart_process_bookings;

		$this->data['breadcrumb'] = [trans('tn.homepage')];
		return View::make($this->prefix.'.system.index', $this->data);
	}

}
