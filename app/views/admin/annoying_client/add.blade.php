<?php
$input_class = ['class' => 'form-control'];

$customer = Form::text('customer', '', $input_class);
$partner_id  = Form::select('partner_id', $partners, null, $input_class);

$status = Form::hidden('status', '0').'<label class="checkbox-inline">'.Form::checkbox('status', '1', true).' '.trans('tn.active').'</label>';
$position = Form::text('position', '', $input_class);

$inputs = [
	'customer' => $customer,
	'partner'  => $partner_id,
	'status'   => $status,
	'position' => $position,
];
$parser = fn_init_parser($inputs);

$parsers[] = $parser;

$tabs = [
	[
		'id'     => 'general',
		'name'   => trans('tn.general'),
		'parser' => $parsers[0],
	],
];

?>

@extends($layout)

@section('main')
	@include($theme.'.partials.update-form', ['tabs' => $tabs])
@stop
