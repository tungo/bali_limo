@extends($layout)

@section('main')

@include($theme.'.partials.table-form-toolbar', ['toolbar' => ['add', 'delete', 'enable', 'disable', 'save']])

<?php
$input_class = ['class' => 'form-control'];
$search = [
	[
		// 'id'       => [2, 0, Form::text('id', $filter['id'], $input_class)],
		'customer' => [3, 0, Form::text('customer', $filter['customer'], $input_class)],
		'partner'  => [3, 0, Form::select('partner_id', $partners, $filter['partner_id'], $input_class)],
	],
];
?>
@include($theme.'.partials.search-form', ['search' => $search])

{{ Form::open(['id' => 'table-form', 'name' => 'table-form']) }}
	<table class="table table-bordered table-form">
	<?php
		$query_strings = array_except(Input::query(), ['sort', 'order']);
		$query_strings['order'] = ($filter['order'] == 'desc') ? 'asc' : 'desc';
		$options['image'] = ($filter['order'] == 'desc') ? '<i class="icon-sort-down"></i>' : '<i class="icon-sort-up"></i>';
		$options['link'] = URL::route($prefix.'.'.$main_route, $query_strings);

		$id       = fn_sort_link(trans('tn.id'), 'id', $filter, $options);
		$customer = fn_sort_link(trans('tn.customer'), 'customer', $filter, $options);
		$partner  = fn_sort_link(trans('tn.partner'), 'partner', $filter, $options);
		$status   = fn_sort_link(trans('tn.status'), 'status', $filter, $options);
		$position = fn_sort_link(trans('tn.position'), 'position', $filter, $options);
	?>
		<thead>
			<tr>
				<th class="text-center" width="20">{{ Form::checkbox('check_all') }}</th>
				<!-- <th class="text-center">{{ $id }}</th> -->
				<!-- <th class="text-center">{{ $position }}</th> -->
				<th>{{ $customer }}</th>
				<th class="text-center">{{ $partner }}</th>
				<th class="text-center">{{ $status }}</th>
				<th class="text-center">&nbsp;</th>
			</tr>
		</thead>

		<tbody>
		<?php
			list($status, $position, $control) = fn_theme_pre_table_form($logged_in_user, $main_route);
		?>
		@foreach ($items as $item)
		<?php
			$edit_url = URL::route($prefix.'.'.$main_route.'.edit', [$item->id]);
			$delete_url = URL::route($prefix.'.'.$main_route.'.delete', [$item->id]);

			$cid = Form::checkbox('cid[]', $item->id).Form::hidden('hid[]', $item->id);
			$status_route = ($item->status == '1') ? 'disable' : 'enable';
			$item_status = preg_replace('/\[url\]/', URL::route($prefix.'.'.$main_route.'.'.$status_route, [$item->id]), $status[$item->status]);
			$item_position = preg_replace(['/\[id\]/', '/\[value\]/'], [$item->id, $item->position], $position);
			$item_control = preg_replace(['/\[edit_url\]/', '/\[delete_url\]/'], [$edit_url, $delete_url], $control);
		?>
			<tr>
				<td class="text-center">{{ $cid }}</td>
				<!-- <td class="text-center">{{ $item->id }}</td> -->
				<!-- <td class="text-center">{{ $item_position }}</td> -->
				<td>{{ $item->customer }}</td>
				<td class="text-center">{{ $item->partner->name }}</td>
				<td class="text-center">{{ $item_status }}</td>
				<td class="text-center">{{ $item_control }}</td>
			</tr>
		@endforeach
		</tbody>
	</table>

	@include($theme.'.partials.table-form-footer')

{{ Form::close() }}

@stop
