@extends($layout)

@section('main')

@include($theme.'.partials.table-form-toolbar', ['toolbar' => ['add', 'delete']])

<?php
$input_class = ['class' => 'form-control'];
$search = [
	[
		// 'id'    => [2, 0, Form::text('id', $filter['id'], $input_class)],
		'email' => [3, 0, Form::text('email', $filter['email'], $input_class)],
	],
];
?>
@include($theme.'.partials.search-form', ['search' => $search])

{{ Form::open(['id' => 'table-form', 'name' => 'table-form']) }}
	<table class="table table-bordered table-form">
	<?php
		$query_strings = array_except(Input::query(), ['sort', 'order']);
		$query_strings['order'] = ($filter['order'] == 'desc') ? 'asc' : 'desc';
		$options['image'] = ($filter['order'] == 'desc') ? '<i class="icon-sort-down"></i>' : '<i class="icon-sort-up"></i>';
		$options['link'] = URL::route($prefix.'.'.$main_route, $query_strings);

		$id        = fn_sort_link(trans('tn.id'), 'id', $filter, $options);
		$email     = fn_sort_link(trans('tn.email'), 'email', $filter, $options);
		$full_name = fn_sort_link(trans('tn.full_name'), 'full_name', $filter, $options);
		$type      = fn_sort_link(trans('tn.type'), 'type', $filter, $options);
		$partner   = fn_sort_link(trans('tn.partner'), 'partner', $filter, $options);
		$car       = fn_sort_link(trans('tn.car'), 'car', $filter, $options);
		$group     = trans('tn.group');
	?>
		<thead>
			<tr>
				<th class="text-center" width="20">{{ Form::checkbox('check_all') }}</th>
				<!-- <th class="text-center">{{ $id }}</th> -->
				<th>{{ $email }}</th>
				<th>{{ $full_name }}</th>
				<th class="text-center">{{ $type }}</th>
				<th class="text-center">{{ $partner }}</th>
				<th class="text-center">{{ $car }}</th>
				<th class="text-left">{{ $group }}</th>
				<th class="text-center">&nbsp;</th>
			</tr>
		</thead>

		<tbody>
		<?php
			list($status, $position, $control) = fn_theme_pre_table_form($logged_in_user, $main_route);
		?>
		@foreach ($items as $item)
		<?php
			$edit_url = URL::route($prefix.'.'.$main_route.'.edit', [$item->id]);
			$delete_url = URL::route($prefix.'.'.$main_route.'.delete', [$item->id]);

			$cid = Form::checkbox('cid[]', $item->id).Form::hidden('hid[]', $item->id);
			$item_control = preg_replace(['/\[edit_url\]/', '/\[delete_url\]/'], [$edit_url, $delete_url], $control);

			$item_partner = count($item->partner) ? $item->partner->name : '';
			$item_car = count($item->car) ? $item->car->number : '';
			$item_groups = $item->groups->lists('name');
		?>
			<tr>
				<td class="text-center">{{ $cid }}</td>
				<!-- <td class="text-center">{{ $item->id }}</td> -->
				<td>{{ $item->email }}</td>
				<td>{{ $item->full_name }}</td>
				<td class="text-center">{{ trans('tn.'.$types[$item->type]) }}</td>
				<td class="text-center">{{ $item_partner }}</td>
				<td class="text-center">{{ $item_car }}</td>
				<td class="text-left">{{ implode(', ', $item_groups) }}</td>
				<td class="text-center">{{ $item_control }}</td>
			</tr>
		@endforeach
		</tbody>
	</table>

	@include($theme.'.partials.table-form-footer')

{{ Form::close() }}

@stop
