<?php
$input_class = ['class' => 'form-control'];

$email                 = Form::text('email', '', $input_class);
$password              = Form::password('password', $input_class);
$password_confirmation = Form::password('password_confirmation', $input_class);
$full_name             = Form::text('full_name', '', $input_class);

$inputs = [
	'email'                 => $email,
	'password'              => $password,
	'password_confirmation' => $password_confirmation,
	'full_name'             => $full_name,
];
$parser = fn_init_parser($inputs);

$parsers[] = $parser;

$type = '';
foreach ($types as $key => $value)
{
	$checked = $key == 0 ? true : false;
	$type .= '<label class="radio-inline">'.Form::radio('type', $key, $checked, ['class' => 'type_show']).' '.trans('tn.'.$value).'</label>';
}

$input_class = ['class' => 'form-control', 'disabled' => 'disabled'];
$partner_id = Form::select('partner_id', $partners, null, $input_class);
$car_id     = Form::select('car_id', $cars, null, $input_class);

$type_html = '
<div class="form-group">
	<label for="type" class="col-sm-2 control-label">'.trans('tn.type').'</label>
	<div class="col-sm-6">'.$type.'</div>
</div>
<div class="form-group" style="display: none;" id="type_2">
	<label for="partner_id" class="col-sm-2 control-label">'.trans('tn.partner').'</label>
	<div class="col-sm-6">'.$partner_id.'</div>
</div>
<div class="form-group" style="display: none;" id="type_3">
	<label for="car_id" class="col-sm-2 control-label">'.trans('tn.car').'</label>
	<div class="col-sm-6">'.$car_id.'</div>
</div>
';

$group_html = '<div class="form-group">';
foreach ($groups_manage as $group)
{
	$group_html .= '
	<div class="col-sm-2 col-sm-offset-1">
		<div class="checkbox">
			<label>
				<input type="checkbox" name="groups['.$group->id.']"> '.$group->name.'
			</label>
		</div>
	</div>';
}
$group_html .= '</div>';

$tabs = [
	[
		'id'     => 'general',
		'name'   => trans('tn.general'),
		'parser' => $parsers[0],
	],
	[
		'id'     => 'type',
		'name'   => trans('tn.type'),
		'type' => 'html',
		'html' => $type_html,
	],
	[
		'id'   => 'group',
		'name' => trans('tn.group'),
		'type' => 'html',
		'html' => $group_html,
	],
];

?>

@extends($layout)

@section('main')
	@include($theme.'.partials.update-form', ['tabs' => $tabs])

	<script type="text/javascript">
	$(document).ready(function(){
		$('input[type="radio"][name="type"]').click(function(){
			$('.form-group[id^="type_"]').hide();
			$('.form-group[id^="type_"] :input').prop('disabled', true);
			$('.form-group#type_'+$(this).val()).show();
			$('.form-group#type_'+$(this).val()+' :input').prop('disabled', false);
		});
		$('input[type="radio"][name="type"]:checked').trigger('click');
	});
	</script>
@stop
