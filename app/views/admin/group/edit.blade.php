<?php
$input_class = ['class' => 'form-control'];

$name  = Form::text('name', $item->name, $input_class);
$level = Form::text('level', $item->level, $input_class);

$inputs = [
	'name' => $name,
	'level' => $level,
];
$parser = fn_init_parser($inputs);

$parsers[] = $parser;

$permissions_html = '
<div class="form-group">
	<div class="col-sm-12">
		<div class="checkbox">
			<label>
				<input type="checkbox" name="all_permissions" class="check-all-data-items" data-item="permissions" checked="checked"> '.trans('tn.check_all_permissions').'
			</label>
		</div>
	</div>
</div>
';

foreach ($permissions_manage as $per_title => $pers)
{
	$permissions_html .= '
<div class="form-group">
	<div class="col-sm-12">
	  <label>'.trans('tn.'.$per_title).'</label>
	</div>';

	foreach ($pers as $per)
	{
		$checked = !empty($permissions[$per]) ? 'checked="checked"' : '';
		$permissions_html .= '
	<div class="col-sm-3">
		<div class="checkbox">
			<label>
				<input type="checkbox" name="permissions['.$per.']" class="check-data-item" data-item="permissions" '.$checked.'> '.trans('tn.'.$per).'
			</label>
		</div>
	</div>';
	}

	$permissions_html .= '
</div>';
}

$tabs = [
	[
		'id'     => 'general',
		'name'   => trans('tn.general'),
		'parser' => $parsers[0],
	],
	[
		'id'   => 'permission',
		'name' => trans('tn.permission'),
		'type' => 'html',
		'html' => $permissions_html,
	],
];

?>

@extends($layout)

@section('main')
	@include($theme.'.partials.update-form', ['tabs' => $tabs])
@stop
