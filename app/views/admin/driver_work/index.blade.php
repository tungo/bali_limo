@extends($layout)

@section('main')

@include($theme.'.partials.table-form-toolbar', ['toolbar' => ['save']])

<?php
$input_class = ['class' => 'form-control'];
$search = [
	[
		// 'id'         => [2, 0, Form::text('id', $filter['id'], $input_class)],
		'full_name'  => [2, 0, Form::text('full_name', $filter['full_name'], $input_class)],
	],
	[
		'time'       => [2, 0, Form::text('time', $filter['time'], $input_class)],
		'car'        => [2, 0, Form::text('car', $filter['car'], $input_class)],
	],
];
?>
@include($theme.'.partials.search-form', ['search' => $search])

{{ Form::open(['id' => 'table-form', 'name' => 'table-form']) }}
	<table class="table table-bordered table-form">
	<?php
		$query_strings = array_except(Input::query(), ['sort', 'order']);
		$query_strings['order'] = ($filter['order'] == 'desc') ? 'asc' : 'desc';
		$options['image'] = ($filter['order'] == 'desc') ? '<i class="icon-sort-down"></i>' : '<i class="icon-sort-up"></i>';
		$options['link'] = URL::route($prefix.'.'.$main_route, $query_strings);

		$id        = fn_sort_link(trans('tn.id'), 'id', $filter, $options);
		$full_name = fn_sort_link(trans('tn.full_name'), 'full_name', $filter, $options);
		$time      = fn_sort_link(trans('tn.time'), 'time', $filter, $options);
		$car       = fn_sort_link(trans('tn.car'), 'car', $filter, $options);
		$status    = fn_sort_link(trans('tn.status'), 'status', $filter, $options);
		$position  = fn_sort_link(trans('tn.position'), 'position', $filter, $options);
	?>
		<thead>
			<tr>
				<!-- <th class="text-center">{{ $id }}</th> -->
				<th>{{ $full_name }}</th>
				<th class="text-center">{{ $time }}</th>
				<th>{{ $car }}</th>
			</tr>
		</thead>

		<tbody>
		<?php
			$time = '[value]';
			$car = '[value]';
			if ($logged_in_user->hasAccess($main_route.'.save'))
			{
				$time = Form::text('time[[id]]', '[value]', ['class' => 'form-control input-sm text-center']);
				$car = Form::text('car[[id]]', '[value]', ['class' => 'form-control input-sm']);
			}
		?>
		@foreach ($items as $item)
		<?php
			$cid = Form::hidden('hid[]', $item->id);
			$item_time = preg_replace(['/\[id\]/', '/\[value\]/'], [$item->id, $item->time], $time);
			$item_car = preg_replace(['/\[id\]/', '/\[value\]/'], [$item->id, $item->car], $car);
		?>
			<tr>
				{{ $cid }}
				<!-- <td class="text-center">{{ $item->id }}</td> -->
				<td>{{ $item->full_name }}</td>
				<td class="text-center">{{ $item_time }}</td>
				<td>{{ $item_car }}</td>
			</tr>
		@endforeach
		</tbody>
	</table>

	@include($theme.'.partials.table-form-footer')

{{ Form::close() }}

@stop
