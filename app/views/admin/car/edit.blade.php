<?php
$input_class = ['class' => 'form-control'];

$number = Form::text('number', $item->number, $input_class);
$cartype_id = Form::select('cartype_id', $cartypes, $item->cartype_id, $input_class);

$status = Form::hidden('status', '0').'<label class="checkbox-inline">'.Form::checkbox('status', '1', $item->status).' '.trans('tn.active').'</label>';
$position = Form::text('position', $item->position, $input_class);

$inputs = [
	'car_number'   => $number,
	'cartype'      => $cartype_id,
	'status'       => $status,
	'position'     => $position,
];
$parser = fn_init_parser($inputs);

$parsers[] = $parser;

$tabs = [
	[
		'id'     => 'general',
		'name'   => trans('tn.general'),
		'parser' => $parsers[0],
	],
];

?>

@extends($layout)

@section('main')
	@include($theme.'.partials.update-form', ['tabs' => $tabs])
@stop
