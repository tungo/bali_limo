<?php
$title = '<div class="input-view">'.$item->title.'</div>';
$content = '<div class="input-view">'.$item->content.'</div>';

$inputs = [
	'title'    => $title,
	'content'  => $content,
];
$parser = fn_init_parser($inputs);

$parsers[] = $parser;

$tabs = [
	[
		'id'     => 'general',
		'name'   => trans('tn.general'),
		'parser' => $parsers[0],
	],
];

?>

@extends($layout)

@section('main')
	@include($theme.'.partials.view-form', ['tabs' => $tabs])
@stop
