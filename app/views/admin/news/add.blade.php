<?php
$input_class = ['class' => 'form-control'];

$title = Form::text('title', '', $input_class);
$content = Form::textarea('content', '', $input_class);
$hot = Form::hidden('hot', '0').'<label class="checkbox-inline">'.Form::checkbox('hot', '1', false).'&nbsp;</label>';

// $status = '<label class="checkbox-inline">'.Form::checkbox('status', '1', true).' '.trans('tn.active').'</label>';
// $position = Form::text('position', '', $input_class);

$inputs = [
	'title'    => $title,
	'content'  => $content,
	'hot'      => $hot,
	// 'status'   => $status,
	// 'position' => $position,
];
$parser = fn_init_parser($inputs);

$parsers[] = $parser;

$group_html = '<div class="form-group">';
foreach ($groups_manage as $group)
{
	$group_html .= '
	<div class="col-sm-10 col-sm-offset-1">
		<div class="checkbox">
			<label>
				<input type="hidden" name="groups['.$group->id.']" value="0">
				<input type="checkbox" name="groups['.$group->id.']" value="1" checked="checked"> '.$group->name.'
			</label>
		</div>
	</div>';
}
$group_html .= '</div>';

$tabs = [
	[
		'id'     => 'general',
		'name'   => trans('tn.general'),
		'parser' => $parsers[0],
	],
	[
		'id'   => 'group',
		'name' => trans('tn.group'),
		'type' => 'html',
		'html' => $group_html,
	],
];

?>

@extends($layout)

@section('main')
	@include($theme.'.partials.update-form', ['tabs' => $tabs])

{{ HTML::style(THEMES_URL.'/default/css/bootstrap3-wysihtml5.min.css') }}
{{ HTML::script(THEMES_URL.'/default/js/bootstrap3-wysihtml5.all.min.js') }}
<script type="text/javascript">
$(document).ready(function() {
	$('textarea[name="content"]').wysihtml5({
	  toolbar: {
	    "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
	    "emphasis": true, //Italics, bold, etc. Default true
	    "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
	    "html": false, //Button which allows you to edit the generated HTML. Default false
	    "link": true, //Button to insert a link. Default true
	    "image": true, //Button to insert an image. Default true,
	    "color": true, //Button to change color of font  
	    "blockquote": true, //Blockquote  
	    "size": 'sm' //default: none, other options are xs, sm, lg
	  }
	});
});
</script>

@stop
