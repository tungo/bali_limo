@extends($layout)

@section('main')

@include($theme.'.partials.table-form-toolbar', ['toolbar' => ['add', 'delete']])

<?php
$input_class = ['class' => 'form-control'];
$search = [
	[
		// 'id'    => [2, 0, Form::text('id', $filter['id'], $input_class)],
		'title' => [3, 0, Form::text('title', $filter['title'], $input_class)],
	],
];
?>
@include($theme.'.partials.search-form', ['search' => $search])

{{ Form::open(['id' => 'table-form', 'name' => 'table-form']) }}
	<table class="table table-bordered table-form">
	<?php
		$query_strings = array_except(Input::query(), ['sort', 'order']);
		$query_strings['order'] = ($filter['order'] == 'desc') ? 'asc' : 'desc';
		$options['image'] = ($filter['order'] == 'desc') ? '<i class="icon-sort-down"></i>' : '<i class="icon-sort-up"></i>';
		$options['link'] = URL::route($prefix.'.'.$main_route, $query_strings);

		$id         = fn_sort_link(trans('tn.id'), 'id', $filter, $options);
		$title      = fn_sort_link(trans('tn.title'), 'title', $filter, $options);
		$user       = fn_sort_link(trans('tn.user'), 'user', $filter, $options);
		$created_at = fn_sort_link(trans('tn.created_at'), 'created_at', $filter, $options);
		$status     = fn_sort_link(trans('tn.status'), 'status', $filter, $options);
		$position   = fn_sort_link(trans('tn.position'), 'position', $filter, $options);
	?>
		<thead>
			<tr>
				<th class="text-center" width="20">{{ Form::checkbox('check_all') }}</th>
				<!-- <th class="text-center">{{ $id }}</th> -->
				<th>{{ $title }}</th>
				<th>{{ $user }}</th>
				<th class="text-center">{{ $created_at }}</th>
				<!-- <th class="text-center">{{ $status }}</th>
				<th class="text-center">{{ $position }}</th> -->
				<th class="text-center">&nbsp;</th>
			</tr>
		</thead>

		<tbody>
		<?php
			list($status, $position, $control) = fn_theme_pre_table_form($logged_in_user, $main_route);
		?>
		@foreach ($items as $item)
		<?php
			$view_url = URL::route($prefix.'.'.$main_route.'.view', [$item->id]);
			$edit_url = URL::route($prefix.'.'.$main_route.'.edit', [$item->id]);
			$delete_url = URL::route($prefix.'.'.$main_route.'.delete', [$item->id]);

			$cid = Form::checkbox('cid[]', $item->id).Form::hidden('hid[]', $item->id);
			$status_route = ($item->status == '1') ? 'disable' : 'enable';
			$item_status = preg_replace('/\[url\]/', URL::route($prefix.'.'.$main_route.'.'.$status_route, [$item->id]), $status[$item->status]);
			$item_position = preg_replace(['/\[id\]/', '/\[value\]/'], [$item->id, $item->position], $position);
			$item_control = preg_replace(['/\[edit_url\]/', '/\[delete_url\]/'], [$edit_url, $delete_url], $control);

			$item_user = count($item->user) ? $item->user->full_name : '';
			$item_hot = $item->hot == 1 ? ' <span class="label label-danger">'.trans('tn.hot').'</span>' : '';
		?>
			<tr>
				<td class="text-center">{{ $cid }}</td>
				<!-- <td class="text-center">{{ $item->id }}</td> -->
				<td>{{ HTML::link($view_url, $item->title).$item_hot }}</td>
				<td>{{ $item_user }}</td>
				<td class="text-center">{{ fn_datetime_get($item->created_at) }}</td>
				<!-- <td class="text-center">{{ $item_status }}</td>
				<td class="text-center">{{ $item_position }}</td> -->
				<td class="text-center">{{ $item_control }}</td>
			</tr>
		@endforeach
		</tbody>
	</table>

	@include($theme.'.partials.table-form-footer')

{{ Form::close() }}

@stop
