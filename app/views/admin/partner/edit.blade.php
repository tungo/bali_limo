<?php
$input_class = ['class' => 'form-control'];

$name             = Form::text('name', $item->name, $input_class);
$partner_type_id  = Form::select('partner_type_id', $partner_types, $item->partner_type_id, $input_class);
$rate_pick_up     = Form::text('rate_pick_up', fn_hide_price($item->rate_pick_up), $input_class);
$rate_drop_off    = Form::text('rate_drop_off', fn_hide_price($item->rate_drop_off), $input_class);
$rate_no_show_pick_up     = Form::text('rate_no_show_pick_up', fn_hide_price($item->rate_no_show_pick_up), $input_class);
$rate_no_show_drop_off    = Form::text('rate_no_show_drop_off', fn_hide_price($item->rate_no_show_drop_off), $input_class);
$text_color       = '<div class="input-group color"><span class="input-group-addon"><i></i></span>'.Form::text('text_color', $item->text_color, $input_class).'</div>';
$background_color = '<div class="input-group color"><span class="input-group-addon"><i></i></span>'.Form::text('background_color', $item->background_color, $input_class).'</div>';
$address          = Form::text('address', $item->address, $input_class);
$phone            = Form::text('phone', $item->phone, $input_class);
$email            = Form::email('email', $item->email, $input_class);

$status = Form::hidden('status', '0').'<label class="checkbox-inline">'.Form::checkbox('status', '1', $item->status).' '.trans('tn.active').'</label>';
$position = Form::text('position', $item->position, $input_class);

$inputs = [
	'name'                  => $name,
	'partner_type'          => $partner_type_id,
	'rate_pick_up'          => $rate_pick_up,
	'rate_drop_off'         => $rate_drop_off,
	'rate_no_show_pick_up'  => $rate_no_show_pick_up,
	'rate_no_show_drop_off' => $rate_no_show_drop_off,
	'text_color'            => $text_color,
	'background_color'      => $background_color,
	'address'               => $address,
	'phone'                 => $phone,
	'email'                 => $email,
	'status'                => $status,
	'position'              => $position,
];
$parser = fn_init_parser($inputs);

$parsers[] = $parser;

$tabs = [
	[
		'id'     => 'general',
		'name'   => trans('tn.general'),
		'parser' => $parsers[0],
	],
];

?>

@extends($layout)

@section('main')
	@include($theme.'.partials.update-form', ['tabs' => $tabs])

<script>
$(document).ready(function(){
	$('.color').colorpicker();
});
</script>

@stop
