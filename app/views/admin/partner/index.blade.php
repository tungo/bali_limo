@extends($layout)

@section('main')

@include($theme.'.partials.table-form-toolbar', ['toolbar' => ['add', 'delete', 'enable', 'disable', 'save']])

<?php
$input_class = ['class' => 'form-control'];
$search = [
	[
		// 'id'           => [2, 0, Form::text('id', $filter['id'], $input_class)],
		'name'         => [3, 0, Form::text('name', $filter['name'], $input_class)],
		'partner_type' => [3, 0, Form::select('partner_type_id', $partner_types, $filter['partner_type_id'], ['class' => 'form-control'])],
	],
	[
		'phone'   => [2, 0, Form::text('phone', $filter['phone'], $input_class)],
		'address' => [3, 0, Form::text('address', $filter['address'], $input_class)],
		'email'   => [2, 0, Form::text('email', $filter['email'], $input_class)],
	],
];
?>
@include($theme.'.partials.search-form', ['search' => $search])

{{ Form::open(['id' => 'table-form', 'name' => 'table-form']) }}
	<table class="table table-bordered table-form">
	<?php
		$query_strings = array_except(Input::query(), ['sort', 'order']);
		$query_strings['order'] = ($filter['order'] == 'desc') ? 'asc' : 'desc';
		$options['image'] = ($filter['order'] == 'desc') ? '<i class="icon-sort-down"></i>' : '<i class="icon-sort-up"></i>';
		$options['link'] = URL::route($prefix.'.'.$main_route, $query_strings);

		$id                    = fn_sort_link(trans('tn.id'), 'id', $filter, $options);
		$name                  = fn_sort_link(trans('tn.name'), 'name', $filter, $options);
		$partner_type          = fn_sort_link(trans('tn.partner_type'), 'partner_type', $filter, $options);
		$rate_pick_up          = fn_sort_link(trans('tn.rate_pick_up'), 'rate_pick_up', $filter, $options);
		$rate_drop_off         = fn_sort_link(trans('tn.rate_drop_off'), 'rate_drop_off', $filter, $options);
		$rate_no_show_pick_up  = fn_sort_link(trans('tn.rate_no_show_pick_up'), 'rate_no_show_pick_up', $filter, $options);
		$rate_no_show_drop_off = fn_sort_link(trans('tn.rate_no_show_drop_off'), 'rate_no_show_drop_off', $filter, $options);
		$address               = fn_sort_link(trans('tn.address'), 'address', $filter, $options);
		$phone                 = fn_sort_link(trans('tn.phone'), 'phone', $filter, $options);
		$email                 = fn_sort_link(trans('tn.email'), 'email', $filter, $options);
		$status                = fn_sort_link(trans('tn.status'), 'status', $filter, $options);
		$position              = fn_sort_link(trans('tn.position'), 'position', $filter, $options);
	?>
		<thead>
			<tr>
				<th class="text-center" width="20">{{ Form::checkbox('check_all') }}</th>
				<!-- <th class="text-center">{{ $id }}</th> -->
				<th class="text-center">{{ $position }}</th>
				<th>{{ $name }}</th>
				<th>{{ $partner_type }}</th>
				<th class="text-right">{{ $rate_pick_up }}</th>
				<th class="text-right">{{ $rate_drop_off }}</th>
				<th class="text-right">{{ $rate_no_show_pick_up }}</th>
				<th class="text-right">{{ $rate_no_show_drop_off }}</th>
				<th>{{ $address }}</th>
				<th>{{ $phone }}</th>
				<th>{{ $email }}</th>
				<th class="text-center">{{ $status }}</th>
				<th class="text-center">&nbsp;</th>
			</tr>
		</thead>

		<tbody>
		<?php
			list($status, $position, $control) = fn_theme_pre_table_form($logged_in_user, $main_route);
		?>
		@foreach ($items as $item)
		<?php
			$edit_url = URL::route($prefix.'.'.$main_route.'.edit', [$item->id]);
			$delete_url = URL::route($prefix.'.'.$main_route.'.delete', [$item->id]);

			$cid = Form::checkbox('cid[]', $item->id).Form::hidden('hid[]', $item->id);
			$status_route = ($item->status == '1') ? 'disable' : 'enable';
			$item_status = preg_replace('/\[url\]/', URL::route($prefix.'.'.$main_route.'.'.$status_route, [$item->id]), $status[$item->status]);
			$item_position = preg_replace(['/\[id\]/', '/\[value\]/'], [$item->id, $item->position], $position);
			$item_control = preg_replace(['/\[edit_url\]/', '/\[delete_url\]/'], [$edit_url, $delete_url], $control);

			$item_partner_type = count($item->partner_type) ? $item->partner_type->name : '';
		?>
			<tr style="background-color: {{ $item->background_color }}; color: {{ $item->text_color }};">
				<td class="text-center">{{ $cid }}</td>
				<!-- <td class="text-center">{{ $item->id }}</td> -->
				<td class="text-center">{{ $item_position }}</td>
				<td>{{ $item->name }}</td>
				<td>{{ $item_partner_type }}</td>
				<td class="text-right">{{ fn_hide_price($item->rate_pick_up) }}</td>
				<td class="text-right">{{ fn_hide_price($item->rate_drop_off) }}</td>
				<td class="text-right">{{ fn_hide_price($item->rate_no_show_pick_up) }}</td>
				<td class="text-right">{{ fn_hide_price($item->rate_no_show_drop_off) }}</td>
				<td>{{ $item->address }}</td>
				<td>{{ $item->phone }}</td>
				<td>{{ $item->email }}</td>
				<td class="text-center">{{ $item_status }}</td>
				<td class="text-center">{{ $item_control }}</td>
			</tr>
		@endforeach
		</tbody>
	</table>

	@include($theme.'.partials.table-form-footer')

{{ Form::close() }}

@stop
