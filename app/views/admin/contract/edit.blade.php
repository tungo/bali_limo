<?php
$input_class = ['class' => 'form-control'];

$full_name           = Form::text('full_name', $item->full_name, $input_class);
$office              = Form::text('office', $item->office, $input_class);
$birthday            = Form::text('birthday', $item->birthday, $input_class);
$birthplace          = Form::text('birthplace', $item->birthplace, $input_class);
$address             = Form::text('address', $item->address, $input_class);
$identity_card_no    = Form::text('identity_card_no', $item->identity_card_no, $input_class);
$identity_card_time  = Form::text('identity_card_time', $item->identity_card_time, $input_class);
$identity_card_place = Form::text('identity_card_place', $item->identity_card_place, $input_class);
$contract_no         = Form::text('contract_no', $item->contract_no, $input_class);
$contract_time       = Form::text('contract_time', $item->contract_time, $input_class);
$contract_place      = Form::text('contract_place', $item->contract_place, $input_class);
$contract_from       = Form::text('contract_from', $item->contract_from, $input_class);
$contract_to         = Form::text('contract_to', $item->contract_to, $input_class);
$salary_basic        = Form::text('salary_basic', $item->salary_basic, $input_class);

$status = Form::hidden('status', '0').'<label class="checkbox-inline">'.Form::checkbox('status', '1', $item->status).' '.trans('tn.active').'</label>';
$position = Form::text('position', $item->position, $input_class);

$inputs = [
	'full_name'           => $full_name,
	'office'              => $office,
	'birthday'            => $birthday,
	'birthplace'          => $birthplace,
	'address'             => $address,
	'identity_card_no'    => $identity_card_no,
	'identity_card_time'  => $identity_card_time,
	'identity_card_place' => $identity_card_place,
	'contract_no'         => $contract_no,
	'contract_place'      => $contract_place,
	'contract_time'       => $contract_time,
	'contract_from'       => $contract_from,
	'salary_basic'        => $salary_basic,
	'status'              => $status,
	'position'            => $position,
];
$parser = fn_init_parser($inputs);

$parsers[] = $parser;

$tabs = [
	[
		'id'     => 'general',
		'name'   => trans('tn.general'),
		'parser' => $parsers[0],
	],
];

?>

@extends($layout)

@section('main')
	@include($theme.'.partials.update-form', ['tabs' => $tabs])
@stop
