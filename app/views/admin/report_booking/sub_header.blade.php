<div class="sub-header">
  <div class="row">
    <div class="col-sm-2">
      <a class="btn btn-success btn-header" href="{{ URL::route($prefix.'.'.$main_route.'.chi_tiet') }}">{{ trans('tn.bang_ke_chi_tiet') }}</a>
    </div>
    <div class="col-sm-2">
      <a class="btn btn-info btn-header" href="{{ URL::route($prefix.'.'.$main_route.'.ke_toan') }}">{{ trans('tn.bao_cao_ke_toan') }}</a>
    </div>
    <div class="col-sm-2">
      <a class="btn btn-warning btn-header" href="{{ URL::route($prefix.'.'.$main_route.'.xe') }}">{{ trans('tn.chi_phi_theo_xe') }}</a>
    </div>
    <div class="col-sm-2">
      <a class="btn btn-danger btn-header" href="{{ URL::route($prefix.'.'.$main_route.'.doanh_thu') }}">{{ trans('tn.bao_cao_doanh_thu') }}</a>
    </div>
    <div class="col-sm-2">
      <a class="btn btn-light-green btn-header" href="{{ URL::route($prefix.'.'.$main_route.'.thong_ke') }}">{{ trans('tn.bao_cao_thong_ke') }}</a>
    </div>
  </div>
</div>
