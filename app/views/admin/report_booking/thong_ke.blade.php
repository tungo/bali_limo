@extends($layout)

@section('main')

@include($prefix.'.'.$main_route.'.sub_header')

<?php
  $query_strings = array_except(Input::query(), ['sort', 'order']);
?>
<?php ob_start(); ?>
@if ($logged_in_user->hasAccess($main_route.'.thong_ke_export'))
  <a class="btn btn-success" href="{{ URL::route($prefix.'.'.$main_route.'.thong_ke_export', $query_strings) }}"><i class="icon-doc-text"></i> {{ trans('tn.export_excel') }}</a>
@endif
<?php $toolbar = ob_get_clean(); ?>
@include($theme.'.partials.table-form-toolbar', ['toolbar' => $toolbar, 'no_search' => true, 'no_per_page' => true])

<?php
$input_class = ['class' => 'form-control'];
$search = [
  [
    'date'    => [5, 0, '<span class="input-group-addon default">'.trans('tn.from').'</span><div class="datetimepicker-wrapper">'.Form::text('date_from', $filter['date_from'], ['class' => 'form-control dtpicker text-center']).'</div><span class="input-group-addon default">'.trans('tn.to').'</span><div class="datetimepicker-wrapper">'.Form::text('date_to', $filter['date_to'], ['class' => 'form-control dtpicker text-center']).'</div>'],
  ],
];
?>
@include($theme.'.partials.search-form', ['search' => $search, 'no_hide' => true])

<script type="text/javascript">
$(document).ready(function(){
  $('.dtpicker').datetimepicker({
    format: 'DD-MM-YYYY',
    widgetPositioning: {horizontal: 'left'}
  });
  $('.tmpicker').datetimepicker({
    format: 'HH:mm',
    widgetPositioning: {horizontal: 'left'}
  });
});
</script>

{{ Form::open(['id' => 'table-form', 'name' => 'table-form']) }}
  <table class="table table-bordered table-form">
  <?php
    $query_strings = array_except(Input::query(), ['sort', 'order']);
    $query_strings['order'] = ($filter['order'] == 'desc') ? 'asc' : 'desc';
    $options['image'] = ($filter['order'] == 'desc') ? '<i class="icon-sort-down"></i>' : '<i class="icon-sort-up"></i>';
    $options['link'] = URL::route($prefix.'.'.$main_route, $query_strings);

    $stt             = 'STT';
    $ngay_thang      = 'Ngày tháng';
    $so_confrom      = 'Số confrom';
    $ten_khach       = 'Tên khách';
    $doi_tac         = 'Đối tác';
    $hanh_trinh      = 'Hành trình';
    $so_tien_usd     = 'Số tiền<br>(USD)';
    $so_tien_vnd     = 'Số tiền<br>(VND)';
    $loai_xe_bo_phan = 'Loại xe / Bộ phận';
    $tai_xe          = 'Tài xế';
    $so_xe           = 'Số xe';
    $ghi_chu         = 'Ghi chú';
  ?>
    <thead>
      <tr>
        <th class="text-center">{{ $stt }}</th>
        <th class="text-center">{{ $ngay_thang }}</th>
        <th class="text-center">{{ $so_confrom }}</th>
        <th class="text-center" width="15%">{{ $ten_khach }}</th>
        <th class="text-center">{{ $doi_tac }}</th>
        <th class="text-center" colspan="2" width="20%">{{ $hanh_trinh }}</th>
        <th class="text-center">{{ $so_tien_usd }}</th>
        <th class="text-center">{{ $so_tien_vnd }}</th>
        <th class="text-center" colspan="2">{{ $loai_xe_bo_phan }}</th>
        <th class="text-center">{{ $tai_xe }}</th>
        <th class="text-center">{{ $so_xe }}</th>
        <th class="text-center">{{ $ghi_chu }}</th>
      </tr>
    </thead>

    <tbody>
    <?php
      $i = 0;
      $report['ty_gia'] = '';
    ?>
    @foreach ($items as $item)
    <?php
      $i++;

      $item_partner = count($item->partner) ? $item->partner->name : '';
      $item_driver  = count($item->driver) ? $item->driver->full_name : '';
      $item_car     = count($item->car) ? $item->car->number : '';
    ?>
      <tr>
        <td class="text-center">{{ $i }}</td>
        <td class="text-center">{{ fn_datetime($item->date, 'Y-m-d', 'd-m-Y', null) }}</td>
        <td class="text-center">{{ $item->id }}</td>
        <td>{{ $item->customer_name }}</td>
        <td>{{ $item_partner }}</td>
        <td>{{ $item->flight }}</td>
        <td class="text-center no-wrap">{{ $types[$item->type] }}</td>
        <td class="text-right">{{ fn_hide_price($item->total, 'FOC') }}</td>
        <td class="text-right"><!-- {{ $item->total * $report['ty_gia'] }} --></td>
        <td class="text-center">{{ $item->cartype }}</td>
        <td class="text-center"></td>
        <td>{{ $item_driver }}</td>
        <td class="text-center">{{ $item_car }}</td>
        <td class="text-center"></td>
      </tr>
    @endforeach
    <?php
      $report['total_vnd'] = $report['total'] * $report['ty_gia'];
      $report['vat_vnd'] = $report['vat'] * $report['ty_gia'];
    ?>
      <tr>
        <td class="text-center"><b>A</b></td>
        <td class="text-center" colspan="5"><b>Tổng cộng ({{ $i }} chuyến)</b></td>
        <td class="text-center"></td>
        <td class="text-right">{{ fn_hide_price($report['total']) }}</td>
        <td class="text-right"><!-- {{ $report['total_vnd'] }} --></td>
        <td class="text-right" colspan="2"><b>Tỷ giá</b></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
      </tr>
      <tr>
        <td class="text-center"><b>B</b></td>
        <td class="text-center" colspan="5"><b>Thuế GTGT (10%)</b></td>
        <td class="text-center"></td>
        <td class="text-right">{{ fn_hide_price($report['vat']) }}</td>
        <td class="text-right"><!-- {{ $report['vat_vnd'] }} --></td>
        <td class="text-right" colspan="2">{{ $report['ty_gia']}}</td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
      </tr>
      <tr>
        <td class="text-center"><b>C</b></td>
        <td class="text-center" colspan="5"><b>Tổng giá trị thành tiền</b></td>
        <td class="text-center"></td>
        <td class="text-right">{{ fn_hide_price($report['total'] + $report['vat']) }}</td>
        <td class="text-right"><!-- {{ $report['total_vnd'] + $report['vat_vnd'] }} --></td>
        <td class="text-center" colspan="2"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
      </tr>
    </tbody>
  </table>

{{ Form::close() }}

@stop
