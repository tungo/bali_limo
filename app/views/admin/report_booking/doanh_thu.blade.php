@extends($layout)

@section('main')

@include($prefix.'.'.$main_route.'.sub_header')

<?php
  $query_strings = array_except(Input::query(), ['sort', 'order']);
?>
<?php ob_start(); ?>
@if ($logged_in_user->hasAccess($main_route.'.chi_tiet_export'))
  <a class="btn btn-success" href="{{ URL::route($prefix.'.'.$main_route.'.doanh_thu_export', $query_strings) }}"><i class="icon-doc-text"></i> {{ trans('tn.export_excel') }}</a>
@endif
<?php $toolbar = ob_get_clean(); ?>
@include($theme.'.partials.table-form-toolbar', ['toolbar' => $toolbar, 'no_search' => true, 'no_per_page' => true])

<?php
$input_class = ['class' => 'form-control'];
$search = [
  [
    'time' => [2, 0, '<div class="datetimepicker-wrapper">'.Form::text('time', $filter['time'], ['class' => 'form-control ympicker text-center']).'</div>'],
  ],
];
?>
@include($theme.'.partials.search-form', ['search' => $search, 'no_hide' => true])

<script type="text/javascript">
$(document).ready(function(){
  $('.ympicker').datetimepicker({
    useCurrent: true,
    viewMode: 'years',
    format: 'MM-YYYY'
  });
});
</script>

{{ Form::open(['id' => 'table-form', 'name' => 'table-form']) }}
  <table class="table table-bordered table-form">
  <?php
    $query_strings = array_except(Input::query(), ['sort', 'order']);
    $query_strings['order'] = ($filter['order'] == 'desc') ? 'asc' : 'desc';
    $options['image'] = ($filter['order'] == 'desc') ? '<i class="icon-sort-down"></i>' : '<i class="icon-sort-up"></i>';
    $options['link'] = URL::route($prefix.'.'.$main_route, $query_strings);

  ?>
    <thead>
      <tr>
        <th class="text-center">Ngày</th>
        <th class="text-center">Diễn giải</th>
        <th class="text-center">Số tiền (USD)</th>
        <th class="text-center">Số tiền (VND)<br>(Tỷ giá: 21.000)</th>
        <th class="text-center">Phải thu</th>
        <th class="text-center">Tiền mặt</th>
        <th class="text-center">Toàn lũy kế</th>
      </tr>
      <tr>
        <th class="text-center">A</th>
        <th class="text-center">B</th>
        <th class="text-center">1</th>
        <th class="text-center">2 = 1 * 21.000</th>
        <th class="text-center">3</th>
        <th class="text-center">4</th>
        <th class="text-center">5</th>
      </tr>
    </thead>

    <tbody>
      <tr>
        <td class="text-center"></td>
        <td>Tuần lễ (từ ngày - đến ngày)</td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
      </tr>
    <?php
      $i = 0;
    ?>
    @foreach ($report['rows'] as $week => $row)
    <?php
      $i++;

      $dien_giai = '';
      if ($week == 'week_1')
      {
        $dien_giai = 'Tuần 1 (1 - 7)';
      }
      elseif ($week == 'week_2')
      {
        $dien_giai = 'Tuần 2 (8 - 14)';
      }
      elseif ($week == 'week_3')
      {
        $dien_giai = 'Tuần 3 (15 - 21)';
      }
      elseif ($week == 'week_4')
      {
        $dien_giai = 'Tuần 4 (22 - '.$report['last_day'].')';
      }
    ?>
      <tr>
        <td class="text-center"></td>
        <td>{{ $dien_giai }}</td>
        <td class="text-right">{{ fn_hide_price($row) }}</td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
      </tr>
    @endforeach
      <tr>
        <td class="text-center"></td>
        <td>Thuê tài xế (PARTHYATT)</td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
      </tr>
      <tr>
        <td class="text-center"></td>
        <td>Thuế GTGT</td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
      </tr>
      <tr>
        <td class="text-center"></td>
        <td>Phí khác</td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
      </tr>
      <tr>
        <td class="text-center"></td>
        <td>Tổng cộng</td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
      </tr>

    </tbody>
  </table>

{{ Form::close() }}

@stop
