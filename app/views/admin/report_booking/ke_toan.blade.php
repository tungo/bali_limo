@extends($layout)

@section('main')

@include($prefix.'.'.$main_route.'.sub_header')

<?php
  $query_strings = array_except(Input::query(), ['sort', 'order']);
?>
<?php ob_start(); ?>
@if ($logged_in_user->hasAccess($main_route.'.chi_tiet_export'))
  <a class="btn btn-success" href="{{ URL::route($prefix.'.'.$main_route.'.ke_toan_export', $query_strings) }}"><i class="icon-doc-text"></i> {{ trans('tn.export_excel') }}</a>
@endif
<?php $toolbar = ob_get_clean(); ?>
@include($theme.'.partials.table-form-toolbar', ['toolbar' => $toolbar, 'no_search' => true, 'no_per_page' => true])

<?php
$input_class = ['class' => 'form-control'];
$search = [
  [
    'time' => [2, 0, '<div class="datetimepicker-wrapper">'.Form::text('time', $filter['time'], ['class' => 'form-control ympicker text-center']).'</div>'],
  ],
];
?>
@include($theme.'.partials.search-form', ['search' => $search, 'no_hide' => true])

<script type="text/javascript">
$(document).ready(function(){
  $('.ympicker').datetimepicker({
    useCurrent: true,
    viewMode: 'years',
    format: 'MM-YYYY'
  });
});
</script>

{{ Form::open(['id' => 'table-form', 'name' => 'table-form']) }}
  <table class="table table-bordered table-form">
  <?php
    $query_strings = array_except(Input::query(), ['sort', 'order']);
    $query_strings['order'] = ($filter['order'] == 'desc') ? 'asc' : 'desc';
    $options['image'] = ($filter['order'] == 'desc') ? '<i class="icon-sort-down"></i>' : '<i class="icon-sort-up"></i>';
    $options['link'] = URL::route($prefix.'.'.$main_route, $query_strings);

    $stt          = 'STT';
    $nha_cung_cap = 'Tên nhà cung cấp';
    $ton_dau      = 'Tồn đầu';
    $phat_sinh    = 'Phát sinh';
    $thang        = 'Tháng '.$month;
    $thanh_toan   = 'Thanh toán';
    $tien_mat     = 'Tiền mặt';
    $ngan_hang    = 'Ngân hàng';
    $tong_cong    = 'Tổng cộng';
    $ton_cuoi     = 'Tồn cuối';

  ?>
    <thead>
      <tr>
        <th class="text-center" rowspan="3">{{ $stt }}</th>
        <th class="text-center" rowspan="3">{{ $nha_cung_cap }}</th>
        <th class="text-center"></th>
        <th class="text-center" colspan="6">{{ $thang }}</th>
      </tr>
      <tr>
        <th class="text-center" rowspan="2">{{ $ton_dau }}</th>
        <th class="text-center" rowspan="2">{{ $phat_sinh }}</th>
        <th class="text-center" colspan="3">{{ $thanh_toan }}</th>
        <th class="text-center" rowspan="2">{{ $ton_cuoi }}</th>
      </tr>
      <tr>
        <th class="text-center">{{ $tien_mat }}</th>
        <th class="text-center">{{ $ngan_hang }}</th>
        <th class="text-center">{{ $tong_cong }}</th>
      </tr>
    </thead>

    <tbody>
    <?php
      $i = 0;
      $report['ty_gia'] = '';
    ?>
    @foreach ($partners as $partner_id => $partner)
    <?php
      $i++;
      $phat_sinh = !empty($report['rows'][$partner_id]) ? $report['rows'][$partner_id] : '';
    ?>
      <tr>
        <td class="text-center">{{ $i }}</td>
        <td>{{ $partner }}</td>
        <td class="text-right"></td>
        <td class="text-right">{{ fn_hide_price($phat_sinh) }}</td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
      </tr>
    @endforeach

      <tr>
        <td class="text-center"></td>
        <td><b>Tổng cộng</b></td>
        <td class="text-right"></td>
        <td class="text-right"><b>{{ fn_hide_price($report['total']) }}</b></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
      </tr>

    </tbody>
  </table>

{{ Form::close() }}

@stop
