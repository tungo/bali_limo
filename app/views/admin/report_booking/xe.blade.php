@extends($layout)

@section('main')

@include($prefix.'.'.$main_route.'.sub_header')

<?php
  $query_strings = array_except(Input::query(), ['sort', 'order']);
?>
<?php ob_start(); ?>
@if ($logged_in_user->hasAccess($main_route.'.chi_tiet_export'))
  <a class="btn btn-success" href="{{ URL::route($prefix.'.'.$main_route.'.xe_export', $query_strings) }}"><i class="icon-doc-text"></i> {{ trans('tn.export_excel') }}</a>
@endif
<?php $toolbar = ob_get_clean(); ?>
@include($theme.'.partials.table-form-toolbar', ['toolbar' => $toolbar, 'no_search' => true, 'no_per_page' => true])

<?php
$input_class = ['class' => 'form-control'];
$search = [
  [
    'time' => [2, 0, '<div class="datetimepicker-wrapper">'.Form::text('time', $filter['time'], ['class' => 'form-control ympicker text-center']).'</div>'],
  ],
];
?>
@include($theme.'.partials.search-form', ['search' => $search, 'no_hide' => true])

<script type="text/javascript">
$(document).ready(function(){
  $('.ympicker').datetimepicker({
    useCurrent: true,
    viewMode: 'years',
    format: 'MM-YYYY'
  });
});
</script>

{{ Form::open(['id' => 'table-form', 'name' => 'table-form']) }}
  <table class="table table-bordered table-form">
  <?php
    $query_strings = array_except(Input::query(), ['sort', 'order']);
    $query_strings['order'] = ($filter['order'] == 'desc') ? 'asc' : 'desc';
    $options['image'] = ($filter['order'] == 'desc') ? '<i class="icon-sort-down"></i>' : '<i class="icon-sort-up"></i>';
    $options['link'] = URL::route($prefix.'.'.$main_route, $query_strings);

  ?>
    <thead>
      <tr>
        <th class="text-center">STT</th>
        <th class="text-center">Số xe</th>
        <th class="text-center">Số chuyến</th>
        <th class="text-center">Số tiền<br>(USD)</th>
        <th class="text-center">Số tiền<br>(VND)</th>
        <th class="text-center">Xăng</th>
        <th class="text-center">Nước</th>
        <th class="text-center">Khăn</th>
        <th class="text-center">Cầu đường</th>
        <th class="text-center">OT</th>
        <th class="text-center">Lương</th>
        <th class="text-center">Thuê VP</th>
        <th class="text-center">Chi phí<br>khấu hao xe</th>
        <th class="text-center">KHVP</th>
        <th class="text-center">Chi phí<br>trích trước</th>
        <th class="text-center">Chi phí<br>linh tinh khác</th>
        <th class="text-center">Tổng cộng</th>
        <th class="text-center">Lãi lỗ</th>
      </tr>
    </thead>

    <tbody>
    <?php
      $i = 0;
    ?>
    @foreach ($cars as $car_id => $car)
    <?php
      $i++;
      $count = !empty($report['rows'][$car_id]['count']) ? $report['rows'][$car_id]['count'] : '';
      $total = !empty($report['rows'][$car_id]['total']) ? $report['rows'][$car_id]['total'] : '';
    ?>
      <tr>
        <td class="text-center">{{ $i }}</td>
        <td>{{ $car }}</td>
        <td class="text-center">{{ $count }}</td>
        <td class="text-right">{{ fn_hide_price($total) }}</td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
      </tr>
    @endforeach

      <tr>
        <td class="text-center"></td>
        <td><b>Tổng cộng</b></td>
        <td class="text-center"><b>{{ $report['count'] }}</b></td>
        <td class="text-right"><b>{{ fn_hide_price($report['total']) }}</b></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center"></td>
      </tr>

    </tbody>
  </table>

{{ Form::close() }}

@stop
