@extends($layout)

@section('main')

<?php
	$item_partner = count($item->partner) ? $item->partner->name : '';
	$item_status = count($item->status) ? $item->status->name : '';
	$item_driver = count($item->driver) ? $item->driver->full_name : '';
	$item_driver_phone = count($item->driver) ? $item->driver->phone : '';
	$item_car = count($item->car) ? $item->car->number : '';
?>

{{ Form::open(['id' => 'confirmation-form', 'name' => 'confirmation-form']) }}
<input type="hidden" name="cid[]" value="{{ $item->id }}">
<input type="hidden" name="redirect" value="manage">

<div class="row">

	<div class="col-sm-12">

		<div class="confirmation-form" style="border:none;">

			<div class="row">
				<div class="col-sm-6">
					<img src="{{ '/'.THEMES_URL.'/default/img/icon/logo.png' }}" style="width: 45%; padding: 30px;">
				</div>
				<div class="col-sm-6">
					<img src="{{ '/'.THEMES_URL.'/default/img/icon/logo-right.png' }}" style="float: right; width: 55%;">
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">
					<p style="text-align: center; font-size: 40px; font-weight: bold; margin-top: -35px;">{{ trans('tn.confirmation_form') }}</p>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<h3><b>{{ trans('tn.customer') }}</b></h3>
					<ul class="info-detail">
						<li><b>{{ trans('tn.date') }}</b>: {{ $item->date }}</li>
						<li><b>{{ trans('tn.name') }}</b>: {{ $item->customer_name }}</li>
						<li><b>{{ trans('tn.amount') }}</b>: {{ $item->customer_amount }}</li>
						<li><b>{{ trans('tn.time') }}</b>: {{ $item->time }}</li>
						<li><b>{{ trans('tn.flight') }}</b>: {{ $item->flight }}</li>
						<li><b>{{ trans('tn.partner') }}</b>: {{ $item_partner }}</li>
						<li><b>{{ trans('tn.type') }}</b>: {{ $types[$item->type] }}</li>
					</ul>
				</div>
				<div class="col-sm-6">
					<h3><b>{{ trans('tn.information') }}</b></h3>
					<ul class="info-detail">
						<li><b>{{ trans('tn.id') }}</b>: {{ $item->id }}</li>
						<li><b>{{ trans('tn.status') }}</b>: <span class="text-{{ Config::get('booking.label.'.$item->status_id) }}">{{ $item_status }}</span></li>
						<li><b>{{ trans('tn.driver') }}</b>: {{ $item_driver }}</li>
						<li><b>{{ trans('tn.driver_phone') }}</b>: {{ $item_driver_phone }}</li>
						<li><b>{{ trans('tn.car') }}</b>: {{ $item_car }}</li>
						<li><b>{{ trans('tn.total') }}</b>: {{ fn_hide_price($item->total) }}</li>
						<li><b>{{ trans('tn.remark') }}</b>: {{ $item->remark }}</li>
						<li><b>{{ trans('tn.note') }}</b>: {{ $item->note }}</li>
					</ul>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 text-center">

					<h4><b>{{ trans('tn.signature_customer') }}</b></h4>

					<div class="signature-wrapper">
				<?php
					if (!File::exists($signature_path.'/'.$customer_signature))
					{
						echo '<div>('.trans('tn.not_sign').')</div>';
					}
					else
					{
						echo '<div><img src="'.URL::asset($signature_url.'/'.$customer_signature).'" id="image-customer-signature" width="300"></div>';
					}
				?>
					</div>

					<p>{{ $item->customer_name }}</p>

				</div>

				<div class="col-sm-6 text-center">

					<h4><b>{{ trans('tn.signature_partner') }}</b></h4>

					<div class="signature-wrapper">
				<?php
					if (!File::exists($signature_path.'/'.$partner_signature))
					{
						echo '<div>('.trans('tn.not_sign').')</div>';
					}
					else
					{
						echo '<div><img src="'.URL::asset($signature_url.'/'.$partner_signature).'" id="image-partner-signature" width="300"></div>';
					}
				?>
					</div>

					<p>{{ $item_partner }}</p>

				</div>
			</div>

		</div>

	</div>
</div>

{{ Form::close() }}

@stop
