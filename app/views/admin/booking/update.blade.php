@extends($layout)

@section('main')

{{ Form::open(['id' => 'table-update-form', 'name' => 'table-update-form']) }}
	<table class="table table-bordered table-form table-update-form">
		<thead>
			<tr>
				<th class="text-center" width="40">{{ trans('tn.id') }}</th>
				<th class="text-center" width="100">{{ trans('tn.date') }}</th>
				<th width="100px">{{ trans('tn.customer') }}</th>
				<th>{{ trans('tn.partner') }}</th>
				<th class="text-center" width="70">{{ trans('tn.time') }}</th>
				<th>{{ trans('tn.flight') }}</th>
				<th>{{ trans('tn.car') }}</th>
				<th class="text-center">{{ trans('tn.type') }}</th>
				<th class="text-center" width="70">{{ trans('tn.amount') }}</th>
				<th width="120px">{{ trans('tn.remark') }}</th>
				<th>{{ trans('tn.status') }}</th>
				<th>{{ trans('tn.driver') }}</th>
				<th width="80">{{ trans('tn.car_code') }}</th>
				<th>{{ trans('tn.car_number') }}</th>
				<th>{{ trans('tn.note') }}</th>
				<th class="text-right" width="70">{{ trans('tn.total') }}</th>
				<th>{{ trans('tn.done_at') }}</th>
			</tr>
		</thead>
		<tbody>

		<?php foreach ($items as $item):

$hid             = Form::hidden('hid[]', $item->id);
$id              = '<div class="label-text text-center">'.$item->id.'</div>';
$date            = '<div class="datetimepicker-wrapper">'.Form::text('items[date][]', fn_datetime($item->date, 'Y-m-d', 'd-m-Y'), ['class' => 'form-control input-sm dtpicker text-center']).'</div>';
$customer_name   = Form::text('items[customer_name][]', $item->customer_name, ['class' => 'form-control input-sm']);
$partner_id      = Form::select('items[partner_id][]', $partners, $item->partner_id, ['class' => 'form-control input-sm']);
$time            = '<div class="datetimepicker-wrapper">'.Form::text('items[time][]', fn_datetime($item->time, 'H:i:s', 'H:i'), ['class' => 'form-control input-sm tmpicker text-center']).'</div>';
$flight          = Form::text('items[flight][]', $item->flight, ['class' => 'form-control input-sm']);
$cartype         = Form::text('items[cartype][]', $item->cartype, ['class' => 'form-control input-sm']);
$type            = Form::select('items[type][]', $types, $item->type, ['class' => 'form-control input-sm']);
$customer_amount = Form::text('items[customer_amount][]', $item->customer_amount, ['class' => 'form-control input-sm text-center']);
$remark          = Form::text('items[remark][]', $item->remark, ['class' => 'form-control input-sm']);
$status          = Form::select('items[status_id][]', $statuses, $item->status_id, ['class' => 'form-control input-sm']);
$driver_id       = Form::select('items[driver_id][]', $drivers, $item->driver_id, ['class' => 'form-control input-sm selectpicker', 'data-live-search' => 'true']);
$car_code        = Form::select('items[car_code][]', $car_codes, $item->car_code, ['class' => 'form-control input-sm selectpicker']);
$car_id          = Form::select('items[car_id][]', $cars, $item->car_id, ['class' => 'form-control input-sm selectpicker', 'data-live-search' => 'true']);
$note            = Form::text('items[note][]', $item->note, ['class' => 'form-control input-sm']);
$total           = Form::text('items[total][]', fn_hide_price($item->total), ['class' => 'form-control input-sm text-right']);
$done_at         = '<div class="datetimepicker-wrapper">'.Form::text('items[done_at][]', fn_datetime($item->done_at, 'Y-m-d H:i:s', 'd-m-Y H:m'), ['class' => 'form-control input-sm dttmpicker text-center']).'</div>';

			echo '<tr style="background-color: '.$item->partner->background_color.';">
				'.$hid.'
				<td>'.$id.'</td>
				<td>'.$date.'</td>
				<td>'.$customer_name.'</td>
				<td>'.$partner_id.'</td>
				<td>'.$time.'</td>
				<td>'.$flight.'</td>
				<td>'.$cartype.'</td>
				<td>'.$type.'</td>
				<td>'.$customer_amount.'</td>
				<td>'.$remark.'</td>
				<td>'.$status.'</td>
				<td>'.$driver_id.'</td>
				<td>'.$car_code.'</td>
				<td>'.$car_id.'</td>
				<td>'.$note.'</td>
				<td>'.$total.'</td>
				<td>'.$done_at.'</td>
			</tr>';

		endforeach; ?>

		</tbody>
	</table>
{{ Form::close() }}

<div class="wrapper-toolbar">
	<div class="row">
		<div class="col-sm-12">
			<a class="btn btn-success submit-data-form" data-form="table-update-form" href="{{ URL::current() }}"><i class="icon-ok"></i> {{ trans('tn.submit') }}</a>
			<a href="{{ URL::previous() }}" class="btn btn-danger"><i class="icon-back"></i> {{ trans('tn.back') }}</a>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	fn_init_datetimepicker();

	function fn_init_datetimepicker() {
		$('.dtpicker').datetimepicker({
			format: 'DD-MM-YYYY',
			widgetPositioning: {horizontal: 'left'}
		});
		$('.tmpicker').datetimepicker({
			format: 'HH:mm',
			widgetPositioning: {horizontal: 'left'}
		});
		$('.dttmpicker').datetimepicker({
			format: 'DD-MM-YYYY HH:mm',
			widgetPositioning: {horizontal: 'right'}
		});
	}
});
</script>

@stop
