<?php
$input_class = ['class' => 'form-control'];

$partner_id = Form::select('partner_id', $partners, null, $input_class);
$excel = Form::file('excel');

$inputs = [
	'partner' => $partner_id,
	'excel'   => $excel,
];
$parser = fn_init_parser($inputs);

$parsers[] = $parser;

$tabs = [
	[
		'id'     => 'general',
		'name'   => trans('tn.general'),
		'parser' => $parsers[0],
	],
];

?>

@extends($layout)

@section('main')
	@include($theme.'.partials.update-form', ['tabs' => $tabs])
@stop
