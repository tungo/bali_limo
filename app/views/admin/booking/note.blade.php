@extends($layout)

@section('main')

{{ Form::open(['id' => 'table-update-form', 'name' => 'table-update-form']) }}
	<table class="table table-bordered table-form table-update-form">
		<thead>
			<tr>
				<th class="text-center" width="40">{{ trans('tn.id') }}</th>
				<th class="text-center" width="100">{{ trans('tn.date') }}</th>
				<th width="100px">{{ trans('tn.customer') }}</th>
				<th>{{ trans('tn.partner') }}</th>
				<th class="text-center" width="70">{{ trans('tn.time') }}</th>
				<th>{{ trans('tn.flight') }}</th>
				<th>{{ trans('tn.car') }}</th>
				<th class="text-center">{{ trans('tn.type') }}</th>
				<th class="text-center" width="70">{{ trans('tn.amount') }}</th>
				<th width="120px">{{ trans('tn.remark') }}</th>
				<th>{{ trans('tn.status') }}</th>
				<th>{{ trans('tn.driver') }}</th>
				<th width="80">{{ trans('tn.car_code') }}</th>
				<th>{{ trans('tn.car_number') }}</th>
				<th>{{ trans('tn.note') }}</th>
				<th class="text-right" width="70">{{ trans('tn.total') }}</th>
			</tr>
		</thead>
		<tbody>

		<?php foreach ($items as $item):

			$item_partner = count($item->partner) ? $item->partner->name : '';
			$item_status  = count($item->status) ? $item->status->name : '';
			$item_driver  = count($item->driver) ? $item->driver->full_name : '';
			$item_car     = count($item->car) ? $item->car->number : '';

			$item_customer_name = AnnoyingClient::check($item->customer_name, $item->partner_id) ? '<span class="label label-xs color-annoying_client"><b>'.$item->customer_name.'</b></span> ' : $item->customer_name;

$hid             = Form::hidden('hid[]', $item->id);
$id              = '<div class="label-text text-center">'.$item->id.'</div>';
$date            = '<div class="label-text text-center">'.fn_datetime($item->date, 'Y-m-d', 'd-m-Y').'</div>';
$customer_name   = '<div class="label-text">'.$item_customer_name.'</div>';
$partner_id      = '<div class="label-text">'.$item_partner.'</div>';
$time            = '<div class="label-text text-center">'.fn_datetime($item->time, 'H:i:s', 'H:i').'</div>';
$flight          = '<div class="label-text">'.$item->flight.'</div>';
$cartype         = '<div class="label-text">'.$item->cartype.'</div>';
$type            = '<div class="label-text text-center">'.$types[$item->type].'</div>';
$customer_amount = '<div class="label-text text-center">'.$item->customer_amount.'</div>';
$remark          = '<div class="label-text">'.$item->remark.'</div>';
$status          = '<div class="label-text"><span class="label label-xs color-'.Config::get('booking.color.'.$item->status_id).'">'.$item_status.'</span></div>';
$driver_id       = '<div class="label-text">'.$item_driver.'</div>';
$car_code        = '<div class="label-text">'.$item->car_code.'</div>';
$car_id          = '<div class="label-text">'.$item_car.'</div>';
$note            = Form::text('items[note][]', $item->note, ['class' => 'form-control input-sm']);
$total           = '<div class="label-text">'.fn_hide_price($item->total).'</div>';

			echo '<tr style="background-color: '.$item->partner->background_color.'; color: '.$item->partner->text_color.';">
				'.$hid.'
				<td>'.$id.'</td>
				<td>'.$date.'</td>
				<td>'.$customer_name.'</td>
				<td>'.$partner_id.'</td>
				<td>'.$time.'</td>
				<td>'.$flight.'</td>
				<td>'.$cartype.'</td>
				<td>'.$type.'</td>
				<td>'.$customer_amount.'</td>
				<td>'.$remark.'</td>
				<td>'.$status.'</td>
				<td>'.$driver_id.'</td>
				<td>'.$car_code.'</td>
				<td>'.$car_id.'</td>
				<td>'.$note.'</td>
				<td>'.$total.'</td>
			</tr>';

		endforeach; ?>

		</tbody>
	</table>
{{ Form::close() }}

<div class="wrapper-toolbar">
	<div class="row">
		<div class="col-sm-12">
			<a class="btn btn-success submit-data-form" data-form="table-update-form" href="{{ URL::current() }}"><i class="icon-ok"></i> {{ trans('tn.submit') }}</a>
			<a href="{{ URL::previous() }}" class="btn btn-danger"><i class="icon-back"></i> {{ trans('tn.back') }}</a>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	fn_init_datetimepicker();

	function fn_init_datetimepicker() {
		$('.dtpicker').datetimepicker({
			format: 'DD-MM-YYYY',
			widgetPositioning: {horizontal: 'left'}
		});
		$('.tmpicker').datetimepicker({
			format: 'HH:mm',
			widgetPositioning: {horizontal: 'left'}
		});
	}
});
</script>

@stop
