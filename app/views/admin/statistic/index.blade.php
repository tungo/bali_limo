@extends($layout)

@section('main')

@include($prefix.'.'.$main_route.'.sub_header')

<div class="row">
  <div class="col-sm-3">
    <div class="btn btn-light-green btn-header" href="#"><span style="font-size: 24px;"><b>$ {{ fn_hide_price($today['total'], 0) }}</b></span><br>{{ trans('tn.today') }}</div>
    <br>
    <div class="btn btn-blue btn-header" href="#"><span style="font-size: 24px;"><b>$ {{ fn_hide_price($yesterday['total'], 0) }}</b></span><br>{{ trans('tn.yesterday') }}</div>
		<br>
    <div class="btn btn-orange btn-header" href="#"><span style="font-size: 24px;"><b>$ {{ fn_hide_price($week['total'], 0) }}</b></span><br>{{ trans('tn.this_week') }}</div>
		<br>
    <div class="btn btn-purple btn-header" href="#"><span style="font-size: 24px;"><b>$ {{ fn_hide_price($month['total'], 0) }}</b></span><br>{{ trans('tn.this_month') }}</div>
		<br>
    <div class="btn btn-dark btn-header" href="#"><span style="font-size: 24px;"><b>$ {{ fn_hide_price($year['total'], 0) }}</b></span><br>{{ trans('tn.this_year') }}</div>
		<br>
    <div class="btn btn-red btn-header" href="#"><span style="font-size: 24px;"><b>$ {{ fn_hide_price($all['total'], 0) }}</b></span><br>{{ trans('tn.all') }}</div>
	</div>

  <div class="col-sm-8">
    <form name="chart_form" method="get">

      <div class="row">
        <div class="col-sm-6">
          <div class="h3" style="margin-top: 5px;">{{ trans('tn.total_in_month') }}</div>
        </div>
        <div class="col-sm-6">
          <div style="margin-top: 10px; margin-right: 100px;">
            <select name="chart_month_year" class="chart_month_year">
            <?php
              foreach ($years as $value)
              {
                $selected = ($value == $chart_month_year) ? 'selected="selected"' : '';
                echo '<option value="'.$value.'">'.$value.'</option>';
              }
            ?>
            </select>
            <select name="chart_month_month" class="chart_month_month">
            <?php
              foreach ($months as $value)
              {
                $selected = ($value == $chart_month_month) ? 'selected="selected"' : '';
                echo '<option value="'.$value.'" '.$selected.'>'.$value.'</option>';
              }
            ?>
            </select>
          </div>
        </div>
      </div>
  		<canvas id="chart_total_month" width="800" height="400"></canvas>

      <script type="text/javascript">
      jQuery(document).ready(function($) {
        var ctx = $("#chart_total_month").get(0).getContext("2d");
        var chart_total_month = new Chart(ctx).Bar({
          labels: [<?php echo '"'.implode('", "',array_keys($chart_total_month)).'"'; ?>],
          datasets: [
            {
              label: "Booking",
              fillColor: "rgba(26,121,182,1)",
              // strokeColor: "rgba(26,121,182,1)",
              // highlightFill: "rgba(26,121,182,1)",
              // highlightStroke: "rgba(26,121,182,1)",
              data: [<?php echo implode(', ', $chart_total_month); ?>]
            }
          ]
        });

        $('.chart_month_month').on('change', function() {
          $('form[name="chart_form"]').submit();
        });
      });
      </script>


      <div class="row">
        <div class="col-sm-6">
          <div class="h3">{{ trans('tn.total_in_year') }}</div>
        </div>
        <div class="col-sm-6">
          <div style="margin-top: 25px; margin-right: 100px;">
            <select name="chart_year_year" class="chart_year_year">
            <?php
              foreach ($years as $value)
              {
                $selected = ($value == $chart_year_year) ? 'selected="selected"' : '';
                echo '<option value="'.$value.'">'.$value.'</option>';
              }
            ?>
            </select>
          </div>
        </div>
      </div>
  		<canvas id="chart_total_year" width="400" height="400"></canvas>

      <script type="text/javascript">
      jQuery(document).ready(function($) {
        var ctx = $("#chart_total_year").get(0).getContext("2d");
        var chart_total_year = new Chart(ctx).Bar({
          labels: [<?php echo '"'.implode('", "',array_keys($chart_total_year)).'"'; ?>],
          datasets: [
            {
              label: "Booking",
              fillColor: "rgba(26,121,182,1)",
              // strokeColor: "rgba(26,121,182,1)",
              // highlightFill: "rgba(26,121,182,1)",
              // highlightStroke: "rgba(26,121,182,1)",
              data: [<?php echo implode(', ', $chart_total_year); ?>]
            }
          ]
        });

        $('.chart_year_year').on('change', function() {
          $('form[name="chart_form"]').submit();
        });
      });
      </script>

    </form>
	</div>
</div>

@stop
