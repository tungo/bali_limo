@extends($layout)

@section('main')

@include($theme.'.partials.table-form-toolbar', ['toolbar' => []])

<?php
$input_class = ['class' => 'form-control'];
$search = [
	[
		'date'   => [4, 0, '<span class="input-group-addon default">'.trans('tn.from').'</span><div class="datetimepicker-wrapper">'.Form::text('date_from', $filter['date_from'], ['class' => 'form-control dtpicker text-center']).'</div><span class="input-group-addon default">'.trans('tn.to').'</span><div class="datetimepicker-wrapper">'.Form::text('date_to', $filter['date_to'], ['class' => 'form-control dtpicker text-center']).'</div>'],
		'user'   => [2, 0, Form::select('user_id', $users, $filter['user_id'], $input_class)],
		'type'   => [3, 0, Form::text('type', $filter['type'], $input_class)],
		'action' => [3, 0, Form::text('action', $filter['action'], $input_class)],
	],
	[
		// 'id'     => [2, 0, Form::text('id', $filter['id'], $input_class)],
		'url'    => [3, 0, Form::text('url', $filter['url'], $input_class)],
		'ip'     => [2, 0, Form::text('ip', $filter['ip'], $input_class)],
	],
];
?>
@include($theme.'.partials.search-form', ['search' => $search])

<script type="text/javascript">
$(document).ready(function(){
	$('.dtpicker').datetimepicker({
		format: 'DD-MM-YYYY',
		widgetPositioning: {horizontal: 'left'}
	});
});
</script>

{{ Form::open(['id' => 'table-form', 'name' => 'table-form']) }}
	<table class="table table-bordered table-form">
	<?php
		$query_strings = array_except(Input::query(), ['sort', 'order']);
		$query_strings['order'] = ($filter['order'] == 'desc') ? 'asc' : 'desc';
		$options['image'] = ($filter['order'] == 'desc') ? '<i class="icon-sort-down"></i>' : '<i class="icon-sort-up"></i>';
		$options['link'] = URL::route($prefix.'.'.$main_route, $query_strings);

		$id     = fn_sort_link(trans('tn.id'), 'id', $filter, $options);
		$user   = fn_sort_link(trans('tn.user'), 'user', $filter, $options);
		$type   = fn_sort_link(trans('tn.type'), 'type', $filter, $options);
		$action = fn_sort_link(trans('tn.action'), 'action', $filter, $options);
		$url    = fn_sort_link(trans('tn.url'), 'url', $filter, $options);
		$ip     = fn_sort_link(trans('tn.ip'), 'ip', $filter, $options);
		$at     = fn_sort_link(trans('tn.at'), 'at', $filter, $options);
	?>
		<thead>
			<tr>
				<th class="text-center" width="20">{{ Form::checkbox('check_all') }}</th>
				<!-- <th class="text-center">{{ $id }}</th> -->
				<th>{{ $user }}</th>
				<th class="text-center">{{ $type }}</th>
				<th class="text-center">{{ $action }}</th>
				<th>{{ $url }}</th>
				<th class="text-center">{{ $ip }}</th>
				<th class="text-center">{{ $at }}</th>
				<th class="text-center">&nbsp;</th>
			</tr>
		</thead>

		<tbody>
		@foreach ($items as $item)
		<?php
			$cid = Form::checkbox('cid[]', $item->id).Form::hidden('hid[]', $item->id);
			$item_full_name = count($item->user) ? $item->user->full_name : '';
		?>
			<tr>
				<td class="text-center">{{ $cid }}</td>
				<!-- <td class="text-center">{{ $item->id }}</td> -->
				<td>{{ $item_full_name }}</td>
				<td class="text-center">{{ $item->type }}</td>
				<td class="text-center">{{ $item->action }}</td>
				<td>{{ $item->url }}</td>
				<td class="text-center">{{ $item->ip }}</td>
				<td class="text-center">{{ fn_datetime_get($item->created_at) }}</td>
				<td class="text-center">

<!-- Button trigger modal -->
<button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModalInput{{ $item->id }}"><i class="icon-list"></i> {{ trans('tn.input') }}</button>

<!-- Modal -->
<div class="modal fade text-left" id="myModalInput{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
			<?php
				$item_input = isset($item->input) ? unserialize($item->input) : [];
				fn_print($item_input)
			?>
      </div>
    </div>
  </div>
</div>

				</td>
			</tr>
		@endforeach
		</tbody>
	</table>

	@include($theme.'.partials.table-form-footer')

{{ Form::close() }}

@stop
