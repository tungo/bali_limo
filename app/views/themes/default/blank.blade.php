<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Admin">
	<meta name="author" content="TuNgo">

	<title>{{{ $page_title }}}</title>

	{{ HTML::style(THEMES_URL.'/default/css/bootstrap.min.css') }}
	{{-- HTML::style(THEMES_URL.'/default/css/bootstrap-theme.min.css') --}}
	{{ HTML::style(THEMES_URL.'/default/css/fontello.css') }}
	{{ HTML::style(THEMES_URL.'/default/css/bootstrap-datetimepicker.min.css') }}
	{{ HTML::style(THEMES_URL.'/default/css/bootstrap-colorpicker.min.css') }}
	{{ HTML::style(THEMES_URL.'/default/css/bootstrap-select.min.css') }}
	{{ HTML::style(THEMES_URL.'/default/css/font-awesome.min.css') }}
	{{ HTML::style(THEMES_URL.'/default/css/admin.css') }}

  <link rel="icon" type="image/png" href="{{ URL::asset(THEMES_URL.'/default/img/icon/57x57.png') }}">

	<link href="{{ URL::asset(THEMES_URL.'/default/img/icon/57x57.png') }}" rel="apple-touch-icon" />
	<link href="{{ URL::asset(THEMES_URL.'/default/img/icon/76x76.png') }}" rel="apple-touch-icon" sizes="76x76" />
	<link href="{{ URL::asset(THEMES_URL.'/default/img/icon/120x120.png') }}" rel="apple-touch-icon" sizes="120x120" />
	<link href="{{ URL::asset(THEMES_URL.'/default/img/icon/152x152.png') }}" rel="apple-touch-icon" sizes="152x152" />
	<link href="{{ URL::asset(THEMES_URL.'/default/img/icon/180x180.png') }}" rel="apple-touch-icon" sizes="180x180" />
	<link href="{{ URL::asset(THEMES_URL.'/default/img/icon/192x192.png') }}" rel="icon" sizes="192x192" />
	<link href="{{ URL::asset(THEMES_URL.'/default/img/icon/128x128.png') }}" rel="icon" sizes="128x128" />

	<!-- <script src="{{ $websocket_server }}/socket.io/socket.io.js"></script> -->
	{{ HTML::script(THEMES_URL.'/default/js/jquery-1.11.1.min.js') }}
	{{ HTML::script(THEMES_URL.'/default/js/jquery-ui.min.js') }}
	{{ HTML::script(THEMES_URL.'/default/js/admin.js') }}
</head>

<body>

	<div class="container-fluid">

		<div class="site">
			@yield('main')
		</div>

	</div>

	{{ HTML::script(THEMES_URL.'/default/js/bootstrap.min.js') }}
	{{ HTML::script(THEMES_URL.'/default/js/moment.min.js') }}
	{{ HTML::script(THEMES_URL.'/default/js/bootstrap-datetimepicker.min.js') }}
	{{ HTML::script(THEMES_URL.'/default/js/bootstrap-colorpicker.min.js') }}
	{{ HTML::script(THEMES_URL.'/default/js/bootstrap-select.min.js') }}
	{{ HTML::script(THEMES_URL.'/default/js/Chart.min.js') }}
	{{ HTML::script(THEMES_URL.'/default/js/admin-foot.js') }}

</body>
</html>
