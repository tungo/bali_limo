<div class="fixed-notification">
</div>

<!-- Fixed navbar -->
<div class="navbar navbar-inverse navbar-fixed-top top-navigation" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<!-- <a class="navbar-brand">Balilimo</a> -->
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">

				<li class="@if (!isset($breadcrumb[1])) active @endif"><a href="{{ URL::route($prefix.'.dashboard') }}"><i class="icon-home"></i></a></li>

			<?php
				foreach ($navigation as $nav)
				{
					$nav_has_children = !empty($nav['children']) ? true : false ;
					$nav_permission = true;

					if (isset($nav['permissions']))
					{
						if (!empty($nav['permissions']) && !$logged_in_user->hasAccess($nav['permissions']))
						{
							$nav_permission = false;
						}
					}
					elseif ($nav_has_children)
					{
						$i = -1;
						foreach ($nav['children'] as $k => $nav_child)
						{
							if (!isset($nav_child['type']))
							{
								if ($logged_in_user->hasAccess($nav_child['permissions']) || $logged_in_user->id == 1)
								{
									break;
								}
							}
							$i = $k + 1;
						}
						if ($i == count($nav['children']))
						{
							$nav_permission = false;
						}
					}

					if ($nav_permission)
					{
						$html = '';
						$active = isset($breadcrumb[1]) && $breadcrumb[1] == trans('tn.'.$nav['name']) ? 'active' : '';
						if ($nav_has_children)
						{
							$html .= '
							<li class="dropdown '.$active.'">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">'.trans('tn.'.$nav['name']).' <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">';
								foreach ($nav['children'] as $nav_child)
								{
									if (!isset($nav_child['type']))
									{
										if ($logged_in_user->hasAccess($nav_child['permissions']) || $logged_in_user->id == 1)
										{
											$link = isset($nav_child['route']) ? URL::route($prefix.'.'.$nav_child['route']) : (isset($nav_child['link']) ? $nav_child['link'] : '#');
											$html .= '<li><a href="'.$link.'">'.trans('tn.'.$nav_child['name']).'</a></li>';
										}
									}
									elseif ($nav_child['type'] == 'divider')
									{
										$html .= '<li role="presentation" class="divider"></li>';
									}
								}
							$html .= '
								</ul>
							</li>';
						}
						else
						{
							$link = isset($nav['route']) ? URL::route($prefix.'.'.$nav['route']) : (isset($nav['link']) ? $nav['link'] : '#');
							$html .= '
							<li class="'.$active.'">
								<a href="'.$link.'">'.trans('tn.'.$nav['name']).'</a>
							</li>';
						}
						echo $html;
					}
				}
			?>

			</ul>

			<ul class="nav navbar-nav navbar-right">
				<!-- <li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-language-1"></i> <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">English</a></li>
						<li><a href="#">Tiếng Việt</a></li>
					</ul>
				</li> -->
				<!-- <li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog-5"></i> <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">{{ trans('tn.setting') }}</a></li>
						<li><a href="#">{{ trans('tn.company') }}</a></li>
						<li><a href="#">{{ trans('tn.log') }}</a></li>
					</ul>
				</li> -->

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="notification-new" style="display: none;"></span> <i class="icon-bell-alt"></i> <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li style="font-size: 90%; padding: 3px 10px 5px; border-bottom: 1px solid #ddd;"><b>{{ trans('tn.notifications') }}</b></li>
						<li>
							<ul class="scrollable-menu notification-list" role="menu">
							<?php
							foreach ($notifications as $notification)
							{
								echo '<li>
									<a href="'.URL::route($prefix.'.booking', ['ids' => $notification->booking_ids]).'">
										<b>'.count(explode(',', $notification->booking_ids)).'</b> '.trans('tn.booking_s_').'
										<span class="label label-xs color-'.Config::get('booking.color.'.$notification->status_id).'">'.trans('tn.'.Config::get('booking.statuses.'.$notification->status_id)).'</span>
										<br>
										<span class="notification-info"><i class="icon-user"></i> '.$notification->user->full_name.' - <i class="icon-clock"></i> '.fn_datetime_get($notification->created_at).'</span>
									</a>
								</li>';
							}
							?>
								<!-- <li><a href="#">noti</a></li> -->
							</ul>
						</li>
						<li class="text-center" style="font-size: 90%; border-top:1px solid #ddd;"><a href="{{ URL::route($prefix.'.booking_notification') }}">{{ trans('tn.see_all_notifications') }}</a></li>
					</ul>

					<script type="text/javascript">// <![CDATA[
					var socket = io.connect('{{ $websocket_server }}');

					socket.on('{{ Config::get($prefix.'.event.status_update') }}', function (data) {
						var notification = JSON.parse(data);
						if (notification.type == {{ Config::get('user.type_id.'.$prefix) }})
						{
							if (notification.type == {{ Config::get('user.type_id.admin') }})
							{
								showNotification(notification);
							}
							else if (notification.type == {{ Config::get('user.type_id.partner') }} && notification.type_id == {{ $logged_in_user->partner_id }})
							{
								showNotification(notification);
							}
							else if (notification.type == {{ Config::get('user.type_id.car') }} && notification.type_id == {{ $logged_in_user->car_id }})
							{
								showNotification(notification);
							}
						}
					});

					function showNotification(noti)
					{
						$('.notification-list').prepend(htmlNotification(noti));
						$('.notification-new').show();
						$('.fixed-notification').prepend(alertNotification(noti));
						window.setTimeout(function() { $('.alert-bali').first().alert('close'); }, 5000);
						$('.notification-sound')[0].play();
					}
					function htmlNotification(noti)
					{
						var html = '<li class="new"><a href="'+noti.link+'"><b>'+noti.ids.length+'</b> {{ trans('tn.booking_s_') }} <span class="label label-xs color-'+noti.color+'">'+noti.status+'</span><br><span class="notification-info"><i class="icon-user"></i> '+noti.user+' - <i class="icon-clock"></i> '+noti.datetime+'</span></a></li>';
						return html;
					}
					function alertNotification(noti)
					{
						var html = '<div class="alert alert-bali fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><a href="'+noti.link+'"><b>'+noti.ids.length+'</b> {{ trans('tn.booking_s_') }} <span class="label label-xs color-'+noti.color+'">'+noti.status+'</span><br><span class="notification-info"><i class="icon-user"></i> '+noti.user+' - <i class="icon-clock"></i> '+noti.datetime+'</span></a></div>';
						return html;
					}

					// ]]></script>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user-1"></i> <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li class="dropdown-header">{{ $logged_in_user->email }}</li>
						<li class="divider"></li>
						<!-- <li><a href="#">{{ trans('tn.account') }}</a></li> -->
						<li><a href="{{ URL::route($prefix.'.logout') }}">{{ trans('tn.logout') }}</a></li>
					</ul>
				</li>
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</div>