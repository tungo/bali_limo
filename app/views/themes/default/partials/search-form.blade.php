{{ Form::open(['id' => 'search-form', 'method' => 'get', 'class' => '']) }}

<div class="wrapper-search" @if ( ! isset($no_hide) && ! $filter['search']) style="display: none;" @endif>
<?php
	$html = '';
	foreach ($search as $row)
	{
		$html .= '<div class="row">';
		foreach ($row as $name => $input)
		{
			$html .= '<div class="col-sm-'.$input[0].' col-sm-offset-'.$input[1].'">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon default">'.trans('tn.'.$name).'</span>
						'.$input[2].'
					</div>
				</div>
			</div>';
		}
		$html .= '</div>';
	}
	echo $html;
?>
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group text-right">
					<button type="submit" class="btn btn-success"><i class="icon-search"></i> {{ trans('tn.apply') }}</button>
					<button type="reset" class="btn btn-warning"><i class="icon-arrows-ccw"></i> {{ trans('tn.reset') }}</button>
					<button type="button" class="btn btn-danger btn-clear-data-form" data-form="search-form"><i class="icon-cancel"></i> {{ trans('tn.clear') }}</button>
				</div>
			</div>
		</div>
</div>

{{ Form::close() }}