<div id="helper">
	<div class="helper-header">
		<i class="icon-move"></i> <a href="#" class="show-helper">{{ trans('tn.helper') }}</a>
	</div>
	<div class="helper-content" style="display: none;">

		<div role="tabpanel">
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#driver-helper" aria-controls="driver-helper" role="tab" data-toggle="tab">{{ trans('tn.driver') }}</a></li>
				<li role="presentation"><a href="#car-helper" aria-controls="car-helper" role="tab" data-toggle="tab">{{ trans('tn.car') }}</a></li>
			</ul>

			<div class="tab-content">

				<div role="tabpanel" class="tab-pane active" id="driver-helper">
					<table class="table">
						<thead>
							<tr>
								<th>{{ trans('tn.name') }}</th>
								<th>{{ trans('tn.time') }}</th>
								<th>{{ trans('tn.car') }}</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($driver_works as $driver_work): ?>
							<tr>
								<td>{{ $driver_work->full_name }}</td>
								<td>{{ $driver_work->time }}</td>
								<td>{{ $driver_work->car }}</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>

				<div role="tabpanel" class="tab-pane" id="car-helper">
					<table class="table">
						<thead>
							<tr>
								<th>{{ trans('tn.car') }}</th>
								<th class="text-center">{{ trans('tn.status') }}</th>
							</tr>
						</thead>
						<tbody>
						<?php
						foreach ($car_works as $car_work):
							$status = $car_work->on_duty ? 'danger' : 'success';
						?>
							<tr>
								<input type="hidden" name="car_works[]" data-id="{{ $car_work->id }}" value="{{ $car_work->on_duty }}">
								<td>{{ $car_work->number }}</td>
								<td class="text-center"><span class="label label-{{ $status }} car_work_label" data-id="{{ $car_work->id }}"> </span></td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>

			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function($) {
	$( "#helper" ).draggable({
		handle: '.helper-header'
		// cancel: '.helper-content .tab-content',
		// containment: 'body',
		// scroll: false
	});
$( "#draggable3" ).draggable({  })

	$('.show-helper').on('click', function(){
		$('.helper-content').toggle();
	});

	$(':hidden[name^="car_works"]').change(function(){
		var car_work_id = $(this).attr('data-id');
		if ($(this).val() == 1)
		{
			$('.car_work_label[data-id="'+car_work_id+'"]').removeClass('label-success').addClass('label-danger');
		}
		else if ($(this).val() == 0)
		{
			$('.car_work_label[data-id="'+car_work_id+'"]').removeClass('label-danger').addClass('label-success');
		}
		else
		{

		}
	})
});
</script>

<script type="text/javascript">// <![CDATA[
var socket = io.connect('{{ $websocket_server }}');

socket.on('{{ Config::get($prefix.'.event.car_work') }}', function (data) {
	var cars = JSON.parse(data);
	cars.ids.forEach(function(entry) {
		$(':hidden[name^="car_works"][data-id="'+entry+'"]').val(cars.on_duty).trigger('change');
	});
});

// ]]></script>