{{ Form::open(['class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
	<ul class="nav nav-tabs" role="tablist">
	@foreach ($tabs as $i => $tab)
		<li role="presentation" class="@if ($i == 0) active @endif">
			<a href="#{{ $tab['id'] }}" role="tab" data-toggle="tab">{{ $tab['name'] }}</a>
		</li>
	@endforeach
	</ul>

	<div class="tab-content">
	@foreach ($tabs as $i => $tab)
		<div role="tabpanel" class="tab-pane fade @if ($i == 0) in active @endif" id="{{ $tab['id'] }}">
		@if (!isset($tab['type']))
			@foreach ($tab['parser'] as $input)
				@include($theme.'.parser.input', $input)
			@endforeach
		@elseif ($tab['type'] == 'html')
			{{ $tab['html'] }}
		@else

		@endif
		</div>
	@endforeach
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<a href="{{ URL::previous() }}" class="btn btn-danger"><i class="icon-back"></i> {{ trans('tn.back') }}</a>
			</div>
		</div>
	</div>
{{ Form::close() }}