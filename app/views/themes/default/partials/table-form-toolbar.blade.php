<div class="wrapper-toolbar">
	<div class="row">
		<div class="col-sm-6">
			<div id="toolbar-fixed">
			@if (is_array($toolbar))
				@if (in_array('add', $toolbar) && $logged_in_user->hasAccess($main_route.'.add'))
					<a class="btn btn-primary" href="{{ URL::route($prefix.'.'.$main_route.'.add') }}"><i class="icon-plus"></i> {{ trans('tn.add') }}</a>
				@endif
				@if (in_array('delete', $toolbar) && $logged_in_user->hasAccess($main_route.'.delete.post'))
					<a class="btn btn-danger submit-data-form" data-form="table-form" data-require="{{ trans('tn.require_checked_rows') }}" data-confirm="{{ trans('tn.confirm_delete') }}" href="{{ URL::route($prefix.'.'.$main_route.'.delete.post') }}"><i class="icon-trash-8"></i> {{ trans('tn.delete') }}</a>
				@endif
				@if (in_array('enable', $toolbar) && $logged_in_user->hasAccess($main_route.'.enable.post'))
					<a class="btn btn-success submit-data-form" data-form="table-form" data-require="{{ trans('tn.require_checked_rows') }}" href="{{ URL::route($prefix.'.'.$main_route.'.enable.post') }}"><i class="icon-ok-circled2-1"></i> {{ trans('tn.enable') }}</a>
				@endif
				@if (in_array('disable', $toolbar) && $logged_in_user->hasAccess($main_route.'.disable.post'))
					<a class="btn btn-warning submit-data-form" data-form="table-form" data-require="{{ trans('tn.require_checked_rows') }}" href="{{ URL::route($prefix.'.'.$main_route.'.disable.post') }}"><i class="icon-block-4"></i> {{ trans('tn.disable') }}</a>
				@endif
				@if (in_array('save', $toolbar) && $logged_in_user->hasAccess($main_route.'.save'))
					<a class="btn btn-info submit-data-form" data-form="table-form" href="{{ URL::route($prefix.'.'.$main_route.'.save') }}"><i class="icon-floppy"></i> {{ trans('tn.save') }}</a>
				@endif
			@else
				{{ $toolbar }}
			@endif
			</div>
		</div>
		<div class="col-sm-6">
			<div class="pull-right">
			@if ( ! isset($no_search))
				<button type="button" class="btn btn-success btn-search" @if ($filter['search']) data-status="open" @endif>{{ trans('tn.search') }} <i class="icon-down-open" @if (!$filter['search']) style="display: none;" @endif></i><i class="icon-right-open" @if ($filter['search']) style="display: none;" @endif></i></button>
			@endif
			@if ( ! isset($no_per_page))
				<div class="btn-group">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{{ $filter['per_page'] }} {{ trans('tn.rows') }} <span class="caret"></span></button>
					<ul class="dropdown-menu dropdown-menu-right" role="menu">
						{{ fn_select_per_page(Input::query(), $prefix.'.'.$main_route, trans('tn.rows')) }}
					</ul>
				</div>
			@endif
			</div>
		</div>
	</div>
</div>
