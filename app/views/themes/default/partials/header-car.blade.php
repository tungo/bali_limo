<div class="fixed-notification">
</div>

<script type="text/javascript">// <![CDATA[
var socket = io.connect('{{ $websocket_server }}');

socket.on('{{ Config::get($prefix.'.event.status_update') }}', function (data) {
	var notification = JSON.parse(data);
	if (notification.type == {{ Config::get('user.type_id.car') }} && notification.type_id == {{ $logged_in_user->car_id }})
	{
		$('.fixed-notification').prepend(alertNotification(notification));
	}
});

function alertNotification(noti)
{
	var html = '<div class="alert alert-bali fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><a href=""><b>'+noti.ids.length+'</b> {{ trans('tn.booking_s_') }} <span class="label label-xs color-'+noti.color+'">'+noti.status+'</span><br><span class="notification-info"><i class="icon-user"></i> '+noti.user+' - <i class="icon-clock"></i> '+noti.datetime+'</span></a></div>';
	return html;
}
// ]]></script>