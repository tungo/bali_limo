<?php
	$status = [
		0 => '<span class="label label-warning"><i class="icon-block-4"></i> '.trans('tn.inactive').'</span>',
		1 => '<span class="label label-success"><i class="icon-ok-circled2-1"></i> '.trans('tn.active').'</span>',
	];
	if ($logged_in_user->hasAccess($main_route.'.enable'))
	{
		$status[0] = '<a href="[url]" class="btn btn-warning btn-xs" title="'.trans('tn.click_to_enable').'"><i class="icon-block-4"></i> '.trans('tn.inactive').'</a>';
	}
	if ($logged_in_user->hasAccess($main_route.'.disable'))
	{
		$status[1] = '<a href="[url]" class="btn btn-success btn-xs" title="'.trans('tn.click_to_disable').'"><i class="icon-ok-circled2-1"></i> '.trans('tn.active').'</a>';
	}

	$position = '[value]';
	if ($logged_in_user->hasAccess($main_route.'.save'))
	{
		$position = Form::text('position[[id]]', '[value]', ['class' => 'form-control input-sm input-text text-center']);
	}

	$control = '';
	if ($logged_in_user->hasAccess($main_route.'.edit'))
	{
		$control .= '<a href="[edit_url]" class="btn btn-primary btn-xs" title="Edit"><i class="icon-pencil"></i> '.trans('tn.edit').'</a> ';
	}
	if ($logged_in_user->hasAccess($main_route.'.delete'))
	{
		$control .= '<a href="[delete_url]" class="btn btn-danger btn-xs" title="Delete" onclick="return confirm(\''.trans('tn.confirm_delete').'\')"><i class="icon-trash-8"></i> '.trans('tn.delete').'</a>';
	}
?>