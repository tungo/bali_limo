@if ($items->count())
	<div class="row">
		<div class="col-sm-6">
			<div class="text-info text-left">
				{{ trans('tn.showing').': '.$items->getFrom().' - '.$items->getTo() }}
				<br>
				{{ trans('tn.pages').': '.$items->getCurrentPage().' / '.$items->getLastPage() }}
				<br>
				{{ trans('tn.total').': '.$items->count().' / '.$items->getTotal() }}
			</div>
		</div>
		<div class="col-sm-6">
			<div class="text-right table-paging">
				{{ $items->links() }}
			</div>
		</div>
	</div>
@else
	<div class="text-info text-center">
		{{ trans('tn.no_info') }}
	</div>
@endif