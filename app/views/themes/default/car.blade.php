<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Admin">
	<meta name="author" content="TuNgo">

	<title>{{{ $page_title }}}</title>

	{{ HTML::style(THEMES_URL.'/default/css/bootstrap.min.css') }}
	{{-- HTML::style(THEMES_URL.'/default/css/bootstrap-theme.min.css') --}}
	{{ HTML::style(THEMES_URL.'/default/css/fontello.css') }}
	{{ HTML::style(THEMES_URL.'/default/css/bootstrap-datetimepicker.min.css') }}
	{{ HTML::style(THEMES_URL.'/default/css/bootstrap-colorpicker.min.css') }}
	{{ HTML::style(THEMES_URL.'/default/css/admin.css') }}

  <link rel="icon" type="image/png" href="{{ '/'.THEMES_URL.'/default/img/icon/57x57.png' }}">

	<link href="{{ '/'.THEMES_URL.'/default/img/icon/57x57.png' }}" rel="apple-touch-icon" />
	<link href="{{ '/'.THEMES_URL.'/default/img/icon/76x76.png' }}" rel="apple-touch-icon" sizes="76x76" />
	<link href="{{ '/'.THEMES_URL.'/default/img/icon/120x120.png' }}" rel="apple-touch-icon" sizes="120x120" />
	<link href="{{ '/'.THEMES_URL.'/default/img/icon/152x152.png' }}" rel="apple-touch-icon" sizes="152x152" />
	<link href="{{ '/'.THEMES_URL.'/default/img/icon/180x180.png' }}" rel="apple-touch-icon" sizes="180x180" />
	<link href="{{ '/'.THEMES_URL.'/default/img/icon/192x192.png' }}" rel="icon" sizes="192x192" />
	<link href="{{ '/'.THEMES_URL.'/default/img/icon/128x128.png' }}" rel="icon" sizes="128x128" />

	<script src="{{ $websocket_server }}/socket.io/socket.io.js"></script>
	{{ HTML::script(THEMES_URL.'/default/js/jquery-1.11.1.min.js') }}
	{{ HTML::script(THEMES_URL.'/default/js/jquery-ui.min.js') }}
</head>

<body class="car-body">

	<?php if ($notification_allowed): ?>
	@include('themes.default.partials.header-car')
	<?php endif; ?>

	<div class="container-fluid">

		<div class="page-header">

			<h3><a href="{{ URL::route($prefix.'.'.$main_route) }}">{{{ $page_title }}}</a></h3>

		</div>

		{{ Notification::showAll() }}

		<div class="site">
			@yield('main')
		</div>

	</div>

	{{ HTML::script(THEMES_URL.'/default/js/bootstrap.min.js') }}
	{{ HTML::script(THEMES_URL.'/default/js/moment.min.js') }}
	{{ HTML::script(THEMES_URL.'/default/js/bootstrap-datetimepicker.min.js') }}
	{{ HTML::script(THEMES_URL.'/default/js/bootstrap-colorpicker.min.js') }}
	{{ HTML::script(THEMES_URL.'/default/js/admin-foot.js') }}

</body>
</html>
