<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Admin">
	<meta name="author" content="TuNgo">

	<title>{{{ $page_title }}}</title>

	{{ HTML::style(THEMES_URL.'/default/css/bootstrap.min.css') }}
	{{-- HTML::style(THEMES_URL.'/default/css/bootstrap-theme.min.css') --}}
	{{ HTML::style(THEMES_URL.'/default/css/login.css') }}

	{{ HTML::script(THEMES_URL.'/default/js/jquery-1.11.1.min.js') }}
</head>

<body>

	<div class="container">
		@yield('main')
	</div>

	{{ HTML::script(THEMES_URL.'/default/js/bootstrap.min.js') }}

</body>
</html>
