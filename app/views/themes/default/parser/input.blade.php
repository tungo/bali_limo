<div class="form-group {{{ $type or '' }}}">
	<label for="{{{ $name or '' }}}" class="col-sm-2 control-label">{{ $label }}</label>
	<div class="col-sm-6">
		{{ $input }}
		{{ $help or '' }}
	</div>
</div>