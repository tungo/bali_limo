@extends($layout)

@section('main')

<?php ob_start(); ?>

@if ($logged_in_user->hasAccess($main_route.'.add'))
	<a class="btn btn-primary" href="{{ URL::route($prefix.'.'.$main_route.'.add') }}"><i class="icon-plus"></i> {{ trans('tn.add') }}</a>
@endif

@if ($logged_in_user->hasAccess($main_route.'.import'))
	<a class="btn btn-success" href="{{ URL::route($prefix.'.'.$main_route.'.import') }}"><i class="icon-doc-text"></i> {{ trans('tn.import') }}</a>
@endif

@if ($logged_in_user->hasAccess($main_route.'.edit'))
	<a class="btn btn-primary submit-data-form" data-form="table-form" data-require="{{ trans('tn.require_checked_rows') }}" data-method="get" href="{{ URL::route($prefix.'.'.$main_route.'.edit') }}"><i class="icon-pencil"></i> {{ trans('tn.edit') }}</a>
@endif

@if ($logged_in_user->hasAccess($main_route.'.cancel.post'))
	<a class="btn btn-danger submit-data-form" data-form="table-form" data-require="{{ trans('tn.require_checked_rows') }}" data-toggle="modal" data-target="#modal-reason" data-method="post" href="{{ URL::route($prefix.'.'.$main_route.'.cancel.post') }}"><i class="icon-cancel"></i> {{ trans('tn.cancel') }}</a>
@endif

<?php $toolbar = ob_get_clean(); ?>
@include($theme.'.partials.table-form-toolbar', ['toolbar' => $toolbar])

<?php
$input_class = ['class' => 'form-control'];
$search = [
	[
		'date'    => [4, 0, '<span class="input-group-addon default">'.trans('tn.from').'</span><div class="datetimepicker-wrapper">'.Form::text('date_from', $filter['date_from'], ['class' => 'form-control dtpicker text-center']).'</div><span class="input-group-addon default">'.trans('tn.to').'</span><div class="datetimepicker-wrapper">'.Form::text('date_to', $filter['date_to'], ['class' => 'form-control dtpicker text-center']).'</div>'],
		'status'  => [3, 0, Form::select('status_id', $statuses, $filter['status_id'], $input_class)],
	],
	[
		'time'       => [4, 0, '<span class="input-group-addon default">'.trans('tn.from').'</span><div class="datetimepicker-wrapper">'.Form::text('time_from', $filter['time_from'], ['class' => 'form-control tmpicker text-center']).'</div><span class="input-group-addon default">'.trans('tn.to').'</span><div class="datetimepicker-wrapper">'.Form::text('time_to', $filter['time_to'], ['class' => 'form-control tmpicker text-center']).'</div>'],
		'driver'     => [3, 0, Form::select('driver_id', $drivers, $filter['driver_id'], $input_class)],
		'car_number' => [3, 0, Form::select('car_id', $cars, $filter['car_id'], $input_class)],
	],
	[
		'total' => [4, 0, '<span class="input-group-addon default">'.trans('tn.from').'</span>'.Form::text('total_from', $filter['total_from'], $input_class).'<span class="input-group-addon default">'.trans('tn.to').'</span>'.Form::text('total_to', $filter['total_to'], $input_class)],
		'type'  => [2, 0, Form::select('type', $types, $filter['type'], $input_class)],
		'car'   => [3, 0, Form::text('cartype', $filter['cartype'], $input_class)],
	],
	[
		'customer' => [3, 0, Form::text('customer_name', $filter['customer_name'], $input_class)],
		'amount'   => [2, 0, Form::text('customer_amount', $filter['customer_amount'], $input_class)],
		'flight'   => [3, 0, Form::text('flight', $filter['flight'], $input_class)],
	],
];
?>
@include($theme.'.partials.search-form', ['search' => $search])

<script type="text/javascript">
$(document).ready(function(){
	$('.dtpicker').datetimepicker({
		format: 'DD-MM-YYYY',
		widgetPositioning: {horizontal: 'left'}
	});
	$('.tmpicker').datetimepicker({
		format: 'HH:mm',
		widgetPositioning: {horizontal: 'left'}
	});
});
</script>

{{ Form::open(['id' => 'table-form', 'name' => 'table-form']) }}

	<div class="modal" id="modal-reason" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h3 class="modal-title" id="myModalLabel">{{ trans('tn.reason_require') }}</h3>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="reason" class="control-label">{{ trans('tn.reason') }}:</label>
						<textarea class="form-control" id="reason" name="reason"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" id="submit-reason">{{ trans('tn.submit') }}</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#submit-reason').click(function() {
			if (! $('textarea#reason').val())
			{
				alert('{{ trans('tn.reason_require') }}');
			}
			else
			{
				$('form#table-form').submit();
			}
		});
	});
	</script>

	<div class="modal" id="modal-description" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h3 class="modal-title" id="myModalLabel">{{ trans('tn.description_require') }}</h3>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="description" class="control-label">{{ trans('tn.description') }}:</label>
						<textarea class="form-control" id="description" name="description"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success">{{ trans('tn.submit') }}</button>
				</div>
			</div>
		</div>
	</div>

	<table class="table table-bordered table-form">
	<?php
		$query_strings = array_except(Input::query(), ['sort', 'order']);
		$query_strings['order'] = ($filter['order'] == 'desc') ? 'asc' : 'desc';
		$options['image'] = ($filter['order'] == 'desc') ? '<i class="icon-sort-down"></i>' : '<i class="icon-sort-up"></i>';
		$options['link'] = URL::route($prefix.'.'.$main_route, $query_strings);

		$id              = fn_sort_link(trans('tn.id'), 'id', $filter, $options);
		$date            = fn_sort_link(trans('tn.date'), 'date', $filter, $options);
		$customer_name   = fn_sort_link(trans('tn.customer'), 'customer_name', $filter, $options);
		$time            = fn_sort_link(trans('tn.time'), 'time', $filter, $options);
		$flight          = fn_sort_link(trans('tn.flight'), 'flight', $filter, $options);
		$cartype         = fn_sort_link(trans('tn.car'), 'cartype', $filter, $options);
		$type            = fn_sort_link(trans('tn.type'), 'type', $filter, $options);
		$customer_amount = fn_sort_link(trans('tn.amount'), 'customer_amount', $filter, $options);
		$remark          = fn_sort_link(trans('tn.remark'), 'remark', $filter, $options);
		$status          = fn_sort_link(trans('tn.status'), 'status', $filter, $options);
		$driver          = fn_sort_link(trans('tn.driver'), 'driver', $filter, $options);
		$car             = fn_sort_link(trans('tn.car_number'), 'car', $filter, $options);
		$total           = fn_sort_link(trans('tn.total'), 'total', $filter, $options);
	?>
		<thead>
			<tr>
				<th class="text-center" width="20">{{ Form::checkbox('check_all') }}</th>
				<th class="text-center" width="40">{{ $id }}</th>
				<th class="text-center">{{ $date }}</th>
				<th width="100">{{ $customer_name }}</th>
				<th class="text-center">{{ $time }}</th>
				<th>{{ $flight }}</th>
				<th>{{ $cartype }}</th>
				<th class="text-center">{{ $type }}</th>
				<th class="text-center">{{ $customer_amount }}</th>
				<th width="150">{{ $remark }}</th>
				<th>{{ $status }}</th>
				<th>{{ $driver }}</th>
				<th class="text-center">{{ $car }}</th>
				<th class="text-right">{{ $total }}</th>
				<th class="text-center">&nbsp;</th>
			</tr>
		</thead>

		<tbody>
		@foreach ($items as $item)
		<?php
			$cid = Form::checkbox('cid[]', $item->id).Form::hidden('hid[]', $item->id);

			$control = '<div class="btn-group">
				<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-cog-5"></i> <span class="caret"></span></button>
				<ul class="dropdown-menu dropdown-menu-right" role="menu">
					<li><a href="'.URL::route($prefix.'.'.$main_route.'.view', [$item->id]).'">'.trans('tn.view').'</a></li>
				</ul>
			</div>';

			$item_partner = count($item->partner) ? $item->partner->name : '';
			$item_status  = count($item->status) ? $item->status->name : '';
			$item_driver  = count($item->driver) ? $item->driver->full_name : '';
			$item_car     = count($item->car) ? $item->car->number : '';
		?>
			<tr>
				<td class="text-center">{{ $cid }}</td>
				<td class="text-center">{{ $item->id }}</td>
				<td class="text-center">{{ fn_datetime($item->date, 'Y-m-d', 'd-m-Y') }}</td>
				<td>{{ $item->customer_name }}</td>
				<td class="text-center">{{ fn_datetime($item->time, 'H:i:s', 'H:i') }}</td>
				<td>{{ $item->flight }}</td>
				<td>{{ $item->cartype }}</td>
				<td class="text-center">{{ $types[$item->type] }}</td>
				<td class="text-center">{{ $item->customer_amount }}</td>
				<td>{{ $item->remark }}</td>
				<td><span class="label label-xs color-{{ Config::get('booking.color.'.$item->status_id) }}">{{ $item_status }}</span></td>
				<td>{{ $item_driver }}</td>
				<td class="text-center">{{ $item->car_code.' '.$item_car }}</td>
				<td class="text-right">{{ fn_hide_price($item->total) }}</td>
				<td class="text-center">{{ $control }}</td>
			</tr>
		@endforeach
		</tbody>
	</table>

	@include($theme.'.partials.table-form-footer')

{{ Form::close() }}

@stop
