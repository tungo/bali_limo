@extends($layout)

@section('main')

<div class="wrapper-toolbar">
	<div class="row">
		<div class="col-sm-12">
			<a class="btn btn-primary add-row-data-form" data-form="table-update-form" href="#"><i class="icon-plus"></i> {{ trans('tn.add_row') }}</a>
		</div>
	</div>
</div>

{{ Form::open(['id' => 'table-update-form', 'name' => 'table-update-form']) }}
	<table class="table table-bordered table-form table-update-form">
		<thead>
			<tr>
				<th class="text-center" width="120px">{{ trans('tn.date') }}</th>
				<th>{{ trans('tn.customer_name') }}</th>
				<th class="text-center" width="80px">{{ trans('tn.time') }}</th>
				<th>{{ trans('tn.flight') }}</th>
				<th width="120px">{{ trans('tn.car') }}</th>
				<th class="text-center" width="100px">{{ trans('tn.type') }}</th>
				<th class="text-center" width="70px">{{ trans('tn.amount') }}</th>
				<th>{{ trans('tn.remark') }}</th>
				<th class="text-center" width="20px">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
<?php
$hid             = Form::hidden('hid[]', '');
$date            = '<div class="datetimepicker-wrapper">'.Form::text('items[date][]', '', ['class' => 'form-control input-sm dtpicker text-center']).'</div>';
$customer_name   = Form::text('items[customer_name][]', '', ['class' => 'form-control input-sm']);
$time            = '<div class="datetimepicker-wrapper">'.Form::text('items[time][]', '', ['class' => 'form-control input-sm tmpicker text-center']).'</div>';
$flight          = Form::text('items[flight][]', '', ['class' => 'form-control input-sm']);
$cartype         = Form::text('items[cartype][]', '', ['class' => 'form-control input-sm']);
$type            = Form::select('items[type][]', $types, null, ['class' => 'form-control input-sm']);
$customer_amount = Form::text('items[customer_amount][]', '', ['class' => 'form-control input-sm text-center']);
$remark          = Form::text('items[remark][]', '', ['class' => 'form-control input-sm']);
?>
			<tr>
				{{ $hid }}
				<td>{{ $date }}</td>
				<td>{{ $customer_name }}</td>
				<td>{{ $time }}</td>
				<td>{{ $flight }}</td>
				<td>{{ $cartype }}</td>
				<td>{{ $type }}</td>
				<td>{{ $customer_amount }}</td>
				<td>{{ $remark }}</td>
				<td><a class="btn btn-sm btn-danger remove-row-data-form"><i class="icon-trash-8"></i></a></td>
			</tr>
		</tbody>
	</table>
{{ Form::close() }}

<div class="wrapper-toolbar">
	<div class="row">
		<div class="col-sm-12">
			<a class="btn btn-success submit-data-form" data-form="table-update-form" href="{{ URL::route($prefix.'.'.$main_route.'.add') }}"><i class="icon-ok"></i> {{ trans('tn.submit') }}</a>
			<a href="{{ URL::previous() }}" class="btn btn-danger"><i class="icon-back"></i> {{ trans('tn.back') }}</a>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('a.add-row-data-form').on('click', function(){
		$('table.'+$(this).attr('data-form')+' tbody').append('<tr>{{ $hid }}<td>{{ $date }}</td><td>{{ $customer_name }}</td><td>{{ $time }}</td><td>{{ $flight }}</td><td>{{ $cartype }}</td><td>{{ $type }}</td><td>{{ $customer_amount }}</td><td>{{ $remark }}</td><td><a class="btn btn-sm btn-danger remove-row-data-form"><i class="icon-trash-8"></i></a></td></tr>');
		fn_remove_row();
		fn_init_datetimepicker();
		return false;
	});
	fn_remove_row();
	fn_init_datetimepicker();

	function fn_remove_row() {
		$('a.remove-row-data-form').on('click', function(){
			$(this).closest('tr').remove();
			return false;
		});
	}
	function fn_init_datetimepicker() {
		$('.dtpicker').datetimepicker({
			format: 'DD-MM-YYYY',
			widgetPositioning: {horizontal: 'left'}
		});
		$('.tmpicker').datetimepicker({
			format: 'HH:mm',
			widgetPositioning: {horizontal: 'left'}
		});
	}

});
</script>

@stop
