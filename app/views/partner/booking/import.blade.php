<?php
$input_class = ['class' => 'form-control'];

$excel = Form::file('excel');

$inputs = [
	'excel'   => $excel,
];
$parser = fn_init_parser($inputs);

$parsers[] = $parser;

$tabs = [
	[
		'id'     => 'general',
		'name'   => trans('tn.general'),
		'parser' => $parsers[0],
	],
];

?>

@extends($layout)

@section('main')
	@include($theme.'.partials.update-form', ['tabs' => $tabs])
@stop
