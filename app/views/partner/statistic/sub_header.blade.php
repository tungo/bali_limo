<div class="sub-header">
  <div class="row">
    <div class="col-sm-3">
      <a class="btn btn-success btn-header" href="{{ URL::route($prefix.'.'.$main_route) }}">{{ trans('tn.general') }}</a>
    </div>
    <div class="col-sm-3">
      <a class="btn btn-info btn-header" href="{{ URL::route($prefix.'.'.$main_route.'.detail') }}">{{ trans('tn.detail') }}</a>
    </div>
  </div>
</div>
