@extends($layout)

@section('main')

@include($prefix.'.'.$main_route.'.sub_header')

<?php
$input_class = ['class' => 'form-control'];
$search = [
	[
		'date'    => [4, 0, '<span class="input-group-addon default">'.trans('tn.from').'</span><div class="datetimepicker-wrapper">'.Form::text('date_from', $filter['date_from'], ['class' => 'form-control dtpicker text-center']).'</div><span class="input-group-addon default">'.trans('tn.to').'</span><div class="datetimepicker-wrapper">'.Form::text('date_to', $filter['date_to'], ['class' => 'form-control dtpicker text-center']).'</div>'],
		'status'  => [3, 0, Form::select('status_id', $statuses, $filter['status_id'], $input_class)],
	],
];
?>
@include($theme.'.partials.search-form', ['search' => $search, 'no_hide' => true])

<script type="text/javascript">
$(document).ready(function(){
	$('.dtpicker').datetimepicker({
		format: 'DD-MM-YYYY',
		widgetPositioning: {horizontal: 'left'}
	});
	$('.tmpicker').datetimepicker({
		format: 'HH:mm',
		widgetPositioning: {horizontal: 'left'}
	});
});
</script>


{{ Form::open(['id' => 'table-form', 'name' => 'table-form']) }}

	<table class="table table-bordered table-form">
	<?php
		$query_strings = array_except(Input::query(), ['sort', 'order']);
		$query_strings['order'] = ($filter['order'] == 'desc') ? 'asc' : 'desc';
		$options['image'] = ($filter['order'] == 'desc') ? '<i class="icon-sort-down"></i>' : '<i class="icon-sort-up"></i>';
		$options['link'] = URL::route($prefix.'.'.$main_route, $query_strings);

		$number          = trans('tn.stt');
		$id              = fn_sort_link(trans('tn.id'), 'id', $filter, $options);
		$date            = fn_sort_link(trans('tn.date'), 'date', $filter, $options);
		$customer_name   = fn_sort_link(trans('tn.customer'), 'customer_name', $filter, $options);
		$time            = fn_sort_link(trans('tn.time'), 'time', $filter, $options);
		$flight          = fn_sort_link(trans('tn.flight'), 'flight', $filter, $options);
		$cartype         = fn_sort_link(trans('tn.car'), 'cartype', $filter, $options);
		$type            = fn_sort_link(trans('tn.type'), 'type', $filter, $options);
		$customer_amount = fn_sort_link(trans('tn.amount'), 'customer_amount', $filter, $options);
		$remark          = fn_sort_link(trans('tn.remark'), 'remark', $filter, $options);
		$status          = fn_sort_link(trans('tn.status'), 'status', $filter, $options);
		$driver          = fn_sort_link(trans('tn.driver'), 'driver', $filter, $options);
		$car             = fn_sort_link(trans('tn.car_number'), 'car', $filter, $options);
		$total           = fn_sort_link(trans('tn.total'), 'total', $filter, $options);
	?>
		<thead>
			<tr>
				<th class="text-center" width="40">{{ $number }}</th>
				<th class="text-center" width="40">{{ $id }}</th>
				<th class="text-center">{{ $date }}</th>
				<th width="100">{{ $customer_name }}</th>
				<th class="text-center">{{ $time }}</th>
				<th>{{ $flight }}</th>
				<th>{{ $cartype }}</th>
				<th class="text-center">{{ $type }}</th>
				<th class="text-center">{{ $customer_amount }}</th>
				<th>{{ $remark }}</th>
				<th>{{ $status }}</th>
				<th>{{ $driver }}</th>
				<th class="text-center">{{ $car }}</th>
				<th class="text-right">{{ $total }}</th>
			</tr>
		</thead>

		<tbody>
		<?php
			$i = 0;
		?>
		@foreach ($items as $item)
		<?php
			$i++;

			$item_partner = count($item->partner) ? $item->partner->name : '';
			$item_status  = count($item->status) ? $item->status->name : '';
			$item_driver  = count($item->driver) ? $item->driver->full_name : '';
			$item_car     = count($item->car) ? $item->car->number : '';
		?>
			<tr>
				<td class="text-center">{{ $i }}</td>
				<td class="text-center">{{ $item->id }}</td>
				<td class="text-center">{{ fn_datetime($item->date, 'Y-m-d', 'd-m-Y') }}</td>
				<td>{{ $item->customer_name }}</td>
				<td class="text-center">{{ fn_datetime($item->time, 'H:i:s', 'H:i') }}</td>
				<td>{{ $item->flight }}</td>
				<td>{{ $item->cartype }}</td>
				<td class="text-center">{{ $types[$item->type] }}</td>
				<td class="text-center">{{ $item->customer_amount }}</td>
				<td >{{ $item->remark }}</td>
				<td><span class="label label-xs color-{{ Config::get('booking.color.'.$item->status_id) }}">{{ $item_status }}</span></td>
				<td>{{ $item_driver }}</td>
				<td class="text-center">{{ $item->car_code.' '.$item_car }}</td>
				<td class="text-right">{{ fn_hide_price($item->total, 0) }}</td>
			</tr>
		@endforeach
			<tr class="text-primary">
				<td class="text-left" colspan="3" style="font-size: 24px;">{{ trans('tn.bookings').': <b>'.$statistic['count'].'</b>' }}</td>
				<td class="text-center"></td>
				<td class="text-center"></td>
				<td class="text-center"></td>
				<td class="text-center"></td>
				<td class="text-center"></td>
				<td class="text-center"></td>
				<td class="text-center"></td>
				<td class="text-center"></td>
				<td class="text-center"></td>
				<td class="text-right" colspan="2" style="font-size: 24px;">{{ trans('tn.total').': <b>'.fn_hide_price($statistic['total'], 0).'</b>' }}</td>
			</tr>

		</tbody>
	</table>

{{ Form::close() }}

@stop
