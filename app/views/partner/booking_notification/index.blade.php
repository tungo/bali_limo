@extends($layout)

@section('main')

@include($theme.'.partials.table-form-toolbar', ['toolbar' => ''])

<?php
$input_class = ['class' => 'form-control'];
$search = [
	[
		// 'id'        => [2, 0, Form::text('id', $filter['id'], $input_class)],
		'status'     => [2, 0, Form::select('status_id', $statuses, $filter['status_id'], $input_class)],
		// 'user'    => [3, 0, Form::select('user_id', $users, $filter['user_id'], $input_class)],
	],
];
?>
@include($theme.'.partials.search-form', ['search' => $search])

{{ Form::open(['id' => 'table-form', 'name' => 'table-form']) }}
	<table class="table table-bordered table-form">
	<?php
		$query_strings = array_except(Input::query(), ['sort', 'order']);
		$query_strings['order'] = ($filter['order'] == 'desc') ? 'asc' : 'desc';
		$options['image'] = ($filter['order'] == 'desc') ? '<i class="icon-sort-down"></i>' : '<i class="icon-sort-up"></i>';
		$options['link'] = URL::route($prefix.'.'.$main_route, $query_strings);

		$id      = fn_sort_link(trans('tn.id'), 'id', $filter, $options);
		$booking = trans('tn.booking');
		$status  = fn_sort_link(trans('tn.status'), 'status', $filter, $options);
		$user    = fn_sort_link(trans('tn.user'), 'user', $filter, $options);
		$at      = fn_sort_link(trans('tn.at'), 'at', $filter, $options);
	?>
		<thead>
			<tr>
				<!-- <th class="text-center" width="20">{{ Form::checkbox('check_all') }}</th> -->
				<!-- <th class="text-center">{{ $id }}</th> -->
				<th>{{ $booking }}</th>
				<th class="text-center">{{ $status }}</th>
				<th>{{ $user }}</th>
				<th class="text-center">{{ $at }}</th>
				<th class="text-center">&nbsp;</th>
			</tr>
		</thead>

		<tbody>
		@foreach ($items as $item)
		<?php
			$cid = Form::checkbox('cid[]', $item->id).Form::hidden('hid[]', $item->id);

			$item_booking = HTML::link(URL::route($prefix.'.booking', ['ids' => $item->booking_ids]), count(explode(',', $item->booking_ids)).' '.trans('tn.booking_s_'));
			$item_status = count($item->status) ? '<span class="label label-xs color-'.Config::get('booking.color.'.$item->status_id).'">'.$item->status->name.'</span>' : '';
			$item_user = count($item->user) ? $item->user->full_name : '';
		?>
			<tr>
				<td>{{ $item_booking }}</td>
				<td class="text-center">{{ $item_status }}</td>
				<td>{{ $item_user }}</td>
				<td class="text-center">{{ fn_datetime_get($item->created_at) }}</td>
				<td class="text-center"></td>
			</tr>
		@endforeach
		</tbody>
	</table>

	@include($theme.'.partials.table-form-footer')

{{ Form::close() }}

@stop
