@extends($layout)

@section('main')

<div class="row">
	<div class="col-sm-3">
		<div class="stat-box">
			<div class="value btn-light-green">{{ $new_booking }}</div>
			<div class="title">{{ trans('tn.new_booking_today') }}</div>
		</div>
		<div class="stat-box">
			<div class="value btn-blue">{{ $booking_proceed }}</div>
			<div class="title">{{ trans('tn.booking_proceed_today') }}</div>
		</div>

	</div>
	<div class="col-sm-9">
		<div class="h3" style="margin-top: 5px;">{{ trans('tn.bookings_this_month') }}</div>
		<canvas id="chart_bookings_this_month" width="800" height="300"></canvas>

		<div class="h3">{{ trans('tn.process_bookings') }}</div>
		<canvas id="chart_process_bookings" width="300" height="300"></canvas>
	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function($) {
	var ctx = $("#chart_bookings_this_month").get(0).getContext("2d");
	var chart_bookings_this_month = new Chart(ctx).Bar({
		labels: [<?php echo '"'.implode('", "',array_keys($chart_bookings_this_month)).'"'; ?>],
    datasets: [
      {
        label: "Booking",
        fillColor: "rgba(26,121,182,1)",
        // strokeColor: "rgba(26,121,182,1)",
        // highlightFill: "rgba(26,121,182,1)",
        // highlightStroke: "rgba(26,121,182,1)",
        data: [<?php echo implode(', ', $chart_bookings_this_month); ?>]
      }
    ]
	});

	var ctx = $("#chart_process_bookings").get(0).getContext("2d");

	var chart_process_bookings = new Chart(ctx).Pie([
		<?php
		$html_arr = [];
		foreach ($chart_process_bookings as $value) {
			$html_arr[] = '{
				value: '.$value->value.',
				color: "'.$value->color.'",
				label: "'.$value->label.'"
			}';
		}
		echo implode(',', $html_arr);
		?>
	]);

});
</script>

@stop
