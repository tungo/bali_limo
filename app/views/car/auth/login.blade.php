@extends($layout)

@section('main')

<img src="{{ URL::asset(FILES_URL.'/img/logo.png') }}" class="logo">

{{ Form::open(['class' => 'form-signin']) }}

	<input type="email" name="email" class="form-control" placeholder="Email" required autofocus>
	<input type="password" name="password" class="form-control" placeholder="Password" required>
	<label class="checkbox">
		<input type="checkbox" name="remember_me" value="remember-me" checked="checked"> {{ trans('tn.remember_me') }}
	</label>
	<button class="btn btn-lg btn-primary btn-block" type="submit">{{ trans('tn.sign_in') }}</button>

	{{ Notification::showAll() }}

{{ Form::close() }}

@stop
