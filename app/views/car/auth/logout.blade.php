@extends($layout)

@section('main')

<img src="{{ URL::asset(FILES_URL.'/img/logo.png') }}" class="logo">

{{ Form::open(['class' => 'form-signin']) }}

	<input type="password" name="password" class="form-control" placeholder="Password" required>
	<button class="btn btn-lg btn-primary btn-block" type="submit">{{ trans('tn.sign_out') }}</button>

	{{ Notification::showAll() }}

{{ Form::close() }}

@stop
