@extends($layout)

@section('main')

{{ Form::open(['id' => 'table-form', 'name' => 'table-form']) }}

	<div class="modal" id="modal-description" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h3 class="modal-title" id="myModalLabel">{{ trans('tn.description_require') }}</h3>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="description" class="control-label">{{ trans('tn.description') }}:</label>
						<textarea class="form-control" id="description" name="description"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success">{{ trans('tn.submit') }}</button>
				</div>
			</div>
		</div>
	</div>

	<table class="table table-bordered table-form">
	<?php
		$query_strings = array_except(Input::query(), ['sort', 'order']);
		$query_strings['order'] = ($filter['order'] == 'desc') ? 'asc' : 'desc';
		$options['image'] = ($filter['order'] == 'desc') ? '<i class="icon-sort-down"></i>' : '<i class="icon-sort-up"></i>';
		$options['link'] = URL::route($prefix.'.'.$main_route, $query_strings);

		$id              = fn_sort_link(trans('tn.id'), 'id', $filter, $options);
		$date            = fn_sort_link(trans('tn.date'), 'date', $filter, $options);
		$customer_name   = fn_sort_link(trans('tn.customer'), 'customer_name', $filter, $options);
		$partner         = fn_sort_link(trans('tn.partner'), 'partner', $filter, $options);
		$time            = fn_sort_link(trans('tn.time'), 'time', $filter, $options);
		$flight          = fn_sort_link(trans('tn.flight'), 'flight', $filter, $options);
		$cartype         = fn_sort_link(trans('tn.car'), 'cartype', $filter, $options);
		$type            = fn_sort_link(trans('tn.type'), 'type', $filter, $options);
		$customer_amount = fn_sort_link(trans('tn.amount'), 'customer_amount', $filter, $options);
		$remark          = fn_sort_link(trans('tn.remark'), 'remark', $filter, $options);
		$status          = fn_sort_link(trans('tn.status'), 'status', $filter, $options);
		$driver          = fn_sort_link(trans('tn.driver'), 'driver', $filter, $options);
		$note            = fn_sort_link(trans('tn.note'), 'note', $filter, $options);
	?>
		<thead>
			<tr>
				<th class="text-center" width="40">{{ $id }}</th>
				<th class="text-center">{{ $date }}</th>
				<th width="120">{{ $customer_name }}</th>
				<th>{{ $partner }}</th>
				<th class="text-center">{{ $time }}</th>
				<th>{{ $flight }}</th>
				<th class="text-center">{{ $type }}</th>
				<th class="text-center">{{ $customer_amount }}</th>
				<th width="150">{{ $remark }}</th>
				<th>{{ $status }}</th>
				<th>{{ $driver }}</th>
				<th width="120">{{ $note }}</th>
				<th class="text-center" style="white-space:nowrap">&nbsp;</th>
			</tr>
		</thead>

		<tbody>
		@foreach ($items as $item)
		<?php
			$cid = Form::hidden('hid[]', $item->id);

			$control = '';
			if (in_array($item->status_id, Config::get('booking.may.on_duty')))
			{
				$control .= '<a class="btn btn-primary btn-xs submit-data-form" data-form="table-form" data-method="post" href="'.URL::route($prefix.'.'.$main_route.'.on_duty.post', [$item->id]).'"><i class="icon-forward"></i> '.trans('tn.on_duty').'</a>';
			}
			if (in_array($item->status_id, Config::get('booking.may.issue')) && $item->status_id != Config::get('booking.status_id.finished'))
			{
				$control .= '<a class="btn btn-danger btn-xs submit-data-form" data-form="table-form" data-method="post" href="'.URL::route($prefix.'.'.$main_route.'.issue.post', [$item->id]).'"><i class="icon-error"></i> '.trans('tn.issue').'</a>';
			}
			if (in_array($item->status_id, Config::get('booking.may.finished')))
			{
				$control .= ' <a class="btn btn-success btn-xs" href="'.URL::route($prefix.'.'.$main_route.'.finished', [$item->id]).'"><i class="icon-ok"></i> '.trans('tn.finished').'</a>';
			}

			$item_partner = count($item->partner) ? $item->partner->name : '';
			$item_status  = count($item->status) ? $item->status->name : '';
			$item_driver  = count($item->driver) ? $item->driver->full_name : '';
			$item_car     = count($item->car) ? $item->car->number : '';

			$item_customer_name = AnnoyingClient::check($item->customer_name, $item->partner_id) ? '<span class="label label-xs color-annoying_client"><b>'.$item->customer_name.'</b></span> ' : $item->customer_name;
		?>
			<tr>
				{{ $cid }}
				<td class="text-center">{{ $item->id }}</td>
				<td class="text-center">{{ fn_datetime($item->date, 'Y-m-d', 'd-m-Y') }}</td>
				<td>{{ $item_customer_name }}</td>
				<td>{{ $item_partner }}</td>
				<td class="text-center">{{ fn_datetime($item->time, 'H:i:s', 'H:i') }}</td>
				<td>{{ $item->flight }}</td>
				<td class="text-center">{{ $types[$item->type] }}</td>
				<td class="text-center">{{ $item->customer_amount }}</td>
				<td>{{ $item->remark }}</td>
				<td><span class="label label-xs color-{{ Config::get('booking.color.'.$item->status_id) }}">{{ $item_status }}</span></td>
				<td>{{ $item_driver }}</td>
				<td>{{ $item->note }}</td>
				<td class="text-center" style="white-space:nowrap">{{ $control }}</td>
			</tr>
		@endforeach
		</tbody>
	</table>

{{ Form::close() }}

@stop
