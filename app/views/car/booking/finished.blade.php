@extends($layout)

@section('main')

<?php
	$item_partner = count($item->partner) ? $item->partner->name : '';
	$item_status = count($item->status) ? $item->status->name : '';
	$item_driver = count($item->driver) ? $item->driver->full_name : '';
	$item_driver_phone = count($item->driver) ? $item->driver->phone : '';
	$item_car = count($item->car) ? $item->car->number : '';
?>

<div class="wrapper-toolbar">
	<div class="row">
		<div class="col-sm-12">
			<a class="btn btn-success submit-data-form" data-form="confirmation-form" data-method="post" href="{{ URL::route($prefix.'.'.$main_route.'.finished.post', [$item->id]) }}"><i class="icon-ok"></i>{{ trans('tn.submit') }}</a>
			<a href="{{ URL::previous() }}" class="btn btn-danger"><i class="icon-back"></i> {{ trans('tn.back') }}</a>
		</div>
	</div>
</div>

{{ Form::open(['id' => 'confirmation-form', 'name' => 'confirmation-form']) }}

<div class="confirmation-form">

	<div class="row">
		<div class="col-sm-6">
			<img src="{{ '/'.THEMES_URL.'/default/img/icon/logo.png' }}" style="width: 45%; padding: 30px;">
		</div>
		<div class="col-sm-6">
			<img src="{{ '/'.THEMES_URL.'/default/img/icon/logo-right.png' }}" style="float: right; width: 55%;">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<p style="text-align: center; font-size: 40px; font-weight: bold; margin-top: -35px;">{{ trans('tn.confirmation_form') }}</p>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<h3><b>{{ trans('tn.customer') }}</b></h3>
			<ul class="info-detail">
				<li><b>{{ trans('tn.name') }}</b>: {{ $item->customer_name }}</li>
				<li><b>{{ trans('tn.email') }}</b>: {{ $item->customer_email }}</li>
				<li><b>{{ trans('tn.phone') }}</b>: {{ $item->customer_phone }}</li>
				<li><b>{{ trans('tn.amount') }}</b>: {{ $item->customer_amount }}</li>
				<li><b>{{ trans('tn.flight') }}</b>: {{ $item->flight }}</li>
				<li><b>{{ trans('tn.partner') }}</b>: {{ $item_partner }}</li>
				<li><b>{{ trans('tn.datetime') }}</b>: {{ $item->timestamp }}</li>
				<li><b>{{ trans('tn.type') }}</b>: {{ $types[$item->type] }}</li>
			</ul>
		</div>
		<div class="col-sm-6">
			<h3><b>{{ trans('tn.information') }}</b></h3>
			<ul class="info-detail">
				<li><b>{{ trans('tn.id') }}</b>: {{ $item->id }}</li>
				<li><b>{{ trans('tn.remarks') }}</b>: {{ $item->remark }}</li>
				<li><b>{{ trans('tn.status') }}</b>: <span class="text-{{ Config::get('booking.label.'.$item->status_id) }}">{{ $item_status }}</span></li>
				<li><b>{{ trans('tn.driver') }}</b>: {{ $item_driver }}</li>
				<li><b>{{ trans('tn.driver_phone') }}</b>: {{ $item_driver_phone }}</li>
				<li><b>{{ trans('tn.car') }}</b>: {{ $item_car }}</li>
				<li><b>{{ trans('tn.rate') }}</b>: {{ $item->rate }}</li>
			</ul>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 text-center">

			<h4><b>{{ trans('tn.signature_customer') }}</b></h4>

			<div class="modal" id="modal-customer-signature" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-signature">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<h3 class="modal-title" id="myModalLabel">{{ trans('tn.signature') }}</h3>
						</div>
						<div class="modal-body text-center">
							<canvas id="customer-signature" width="600" height="300"></canvas>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-success" id="save-customer-signature">{{ trans('tn.save') }}</button>
							<button type="button" class="btn btn-warning" id="clear-customer-signature">{{ trans('tn.clear') }}</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal" id="close-customer-signature">{{ trans('tn.cancel') }}</button>
						</div>
					</div>
				</div>
			</div>

			<div class="signature-wrapper">
		<?php
			if (!File::exists($signature_path.'/'.$customer_signature))
			{
				echo '<a data-toggle="modal" data-target="#modal-customer-signature" href="#" id="link-customer-sign" class="btn btn-primary">'.trans('tn.click_to_sign').'</a>
				<div><img src="" id="image-customer-signature" style="display: none;" width="300"></div>';
			}
			else
			{
				echo '<div><img src="'.URL::asset($signature_url.'/'.$customer_signature).'" id="image-customer-signature" width="300"></div>';
			}
		?>
			</div>

			<p>{{ $item->customer_name }}</p>

			<a data-toggle="modal" data-target="#modal-review" href="#" id="link-review" class="btn btn-primary" style="display: none;">{{ trans('tn.click_to_review') }}</a>
			<div class="modal" id="modal-review" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<h3 class="modal-title" id="myModalLabel">{{ trans('tn.comment_and_review') }}</h3>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-sm-8">
									<div class="comment-wrapper">
										<textarea class="form-control" rows="5" name="comment" placeholder=""></textarea>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="rating-wrapper text-left">
									<?php for ($i = 5; $i >= 1; $i--)
									{
										$star = '';
										for ($j = 1; $j <= $i; $j++)
										{
											$star .= '<i class="icon-star"></i>';
										}
										echo '<div class="radio">
										  <label>
										    <input type="radio" name="rating" value="'.$i.'"> '.$star.'
										  </label>
										</div>';
									}
									?>

									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-success" id="save-review">{{ trans('tn.save') }}</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal" id="close-review">{{ trans('tn.cancel') }}</button>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="col-sm-6 text-center">

			<h4><b>{{ trans('tn.signature_partner') }}</b></h4>

			<div class="modal" id="modal-partner-signature" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-signature">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<h3 class="modal-title" id="myModalLabel">{{ trans('tn.signature') }}</h3>
						</div>
						<div class="modal-body text-center">
							<canvas id="partner-signature" width="600" height="300"></canvas>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-success" id="save-partner-signature">{{ trans('tn.save') }}</button>
							<button type="button" class="btn btn-warning" id="clear-partner-signature">{{ trans('tn.clear') }}</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal" id="close-partner-signature">{{ trans('tn.cancel') }}</button>
						</div>
					</div>
				</div>
			</div>

			<div class="signature-wrapper">
		<?php
			if (!File::exists($signature_path.'/'.$partner_signature))
			{
				echo '<a data-toggle="modal" data-target="#modal-partner-signature" href="#" id="link-partner-sign" class="btn btn-primary">'.trans('tn.click_to_sign').'</a>
				<div><img src="" id="image-partner-signature" style="display: none;" width="300"></div>';
			}
			else
			{
				echo '<div><img src="'.URL::asset($signature_url.'/'.$partner_signature).'" id="image-partner-signature" width="300"></div>';
			}
		?>
			</div>

			<p>{{ $item_partner }}</p>

		</div>
	</div>

</div>

{{ Form::close() }}

{{ HTML::script(THEMES_URL.'/default/js/signature_pad.min.js') }}
<script type="text/javascript">
jQuery(document).ready(function($) {

	var customer_canvas = document.getElementById("customer-signature");
	var customer_signaturePad = new SignaturePad(customer_canvas);

	$('#save-customer-signature').on('click', function(){
    if (customer_signaturePad.isEmpty()) {
      alert("Please provide signature first.");
    } else {
      var image = customer_signaturePad.toDataURL();
      $.ajax({
				type: 'POST',
				url: '{{ URL::route($prefix.'.'.$main_route.'.sign.post', [$item->id]) }}',
				data: {'signature': image, 'type': 'customer'},
				dataType: 'json',
				success: function(data){
					if (data.result == 'success')
					{
						$('#link-customer-sign').hide();
						$('#image-customer-signature').show().attr('src', data.file);
						$('#close-customer-signature').trigger('click');

						$('#link-review').click();
					}
					else if (data.result == 'error')
					{
						alert(data.error);
					}
				}
			});
    }
	});
	$('#clear-customer-signature').on('click', function(){
    customer_signaturePad.clear();
	});

	var partner_canvas = document.getElementById("partner-signature");
	var partner_signaturePad = new SignaturePad(partner_canvas);

	$('#save-partner-signature').on('click', function(){
    if (partner_signaturePad.isEmpty()) {
      alert("Please provide signature first.");
    } else {
      var image = partner_signaturePad.toDataURL();
      $.ajax({
				type: 'POST',
				url: '{{ URL::route($prefix.'.'.$main_route.'.sign.post', [$item->id]) }}',
				data: {'signature': image, 'type': 'partner'},
				dataType: 'json',
				success: function(data){
					if (data.result == 'success')
					{
						$('#link-partner-sign').hide();
						$('#image-partner-signature').show().attr('src', data.file);
						$('#close-partner-signature').trigger('click');
					}
					else if (data.result == 'error')
					{
						alert(data.error);
					}
				}
			});
    }
	});
	$('#clear-partner-signature').on('click', function(){
    partner_signaturePad.clear();
	});

	$('#save-review').on('click', function(){
		var rating = $('input[name="rating"]:radio:checked').val();
		var comment = $('textarea[name="comment"]').val();
    $.ajax({
			type: 'POST',
			url: '{{ URL::route($prefix.'.'.$main_route.'.review.post', [$item->id]) }}',
			data: {'rating': rating, 'comment': comment},
			dataType: 'json',
			success: function(data){
				if (data.result == 'success')
				{
					$('#close-review').trigger('click');
				}
				else if (data.result == 'error')
				{
					alert(data.error);
				}
			}
		});
	});

});
</script>
@stop
