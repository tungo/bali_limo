<?php

Event::listen(Config::get('admin.event.status_update'), 'UpdateStatusEventHandler@handleAdmin');
Event::listen(Config::get('partner.event.status_update'), 'UpdateStatusEventHandler@handlePartner');
Event::listen(Config::get('car.event.status_update'), 'UpdateStatusEventHandler@handleCar');

Event::listen(Config::get('admin.event.car_work'), 'UpdateStatusEventHandler@handleCarWork');