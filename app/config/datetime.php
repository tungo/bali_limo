<?php
// datetime format
return [
	'system' => 'Y-m-d H:i:s',
	'display' => 'd-m-Y H:i',
	'full_display' => 'd-m-Y H:i:s',
];
