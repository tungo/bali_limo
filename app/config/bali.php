<?php

return array(

	'websocket_port' => '3000',

	'permissions' => [
		'booking_view'             => ['booking', 'booking.view'],
		'booking_add'              => ['booking.add', 'booking.add.post'],
		'booking_import'           => ['booking.import', 'booking.import.post'],
		'booking_edit'             => ['booking.edit', 'booking.edit.post', 'booking.save'],
		'booking_delete'           => ['booking.delete', 'booking.delete.post'],
		'booking_update'           => ['booking.update', 'booking.update.post'],
		'booking_note'             => ['booking.note', 'booking.note.post'],
		'booking_print'            => ['booking.print', 'booking.print.post'],
		'booking_approved'         => ['booking.approved', 'booking.approved.post'],
		'booking_not_approved'     => ['booking.not_approved', 'booking.not_approved.post'],
		'booking_car_requested'    => ['booking.car_requested', 'booking.car_requested.post'],
		'booking_on_duty'          => ['booking.on_duty', 'booking.on_duty.post'],
		'booking_issue'            => ['booking.issue', 'booking.issue.post'],
		'booking_finished'         => ['booking.finished', 'booking.finished.post'],
		'booking_cancel'           => ['booking.cancel', 'booking.cancel.post'],
		'booking_no_show'          => ['booking.no_show', 'booking.no_show.post'],
		'booking_completed'        => ['booking.completed', 'booking.completed.post'],
		'booking_free_of_charge'   => ['booking.free_of_charge', 'booking.free_of_charge.post'],

		'user_view'                => ['user'],
		'user_add'                 => ['user.add', 'user.add.post'],
		'user_edit'                => ['user.edit', 'user.edit.post'],
		'user_delete'              => ['user.delete', 'user.delete.post'],

		'group_view'               => ['group'],
		'group_add'                => ['group.add', 'group.add.post'],
		'group_edit'               => ['group.edit', 'group.edit.post'],
		'group_delete'             => ['group.delete', 'group.delete.post'],

		'car_view'                 => ['car'],
		'car_add'                  => ['car.add', 'car.add.post'],
		'car_edit'                 => ['car.edit', 'car.edit.post', 'car.save', 'car.enable', 'car.enable.post', 'car.disable', 'car.disable.post'],
		'car_delete'               => ['car.delete', 'car.delete.post'],

		'cartype_view'             => ['cartype'],
		'cartype_add'              => ['cartype.add', 'cartype.add.post'],
		'cartype_edit'             => ['cartype.edit', 'cartype.edit.post', 'cartype.save', 'cartype.enable', 'cartype.enable.post', 'cartype.disable', 'cartype.disable.post'],
		'cartype_delete'           => ['cartype.delete', 'cartype.delete.post'],

		'driver_view'              => ['driver'],
		'driver_add'               => ['driver.add', 'driver.add.post'],
		'driver_edit'              => ['driver.edit', 'driver.edit.post', 'driver.save', 'driver.enable', 'driver.enable.post', 'driver.disable', 'driver.disable.post'],
		'driver_delete'            => ['driver.delete', 'driver.delete.post'],

		'partner_view'             => ['partner'],
		'partner_add'              => ['partner.add', 'partner.add.post'],
		'partner_edit'             => ['partner.edit', 'partner.edit.post', 'partner.save', 'partner.enable', 'partner.enable.post', 'partner.disable', 'partner.disable.post'],
		'partner_delete'           => ['partner.delete', 'partner.delete.post'],

		'partner_type_view'        => ['partner_type'],
		'partner_type_add'         => ['partner_type.add', 'partner_type.add.post'],
		'partner_type_edit'        => ['partner_type.edit', 'partner_type.edit.post', 'partner_type.save', 'partner_type.enable', 'partner_type.enable.post', 'partner_type.disable', 'partner_type.disable.post'],
		'partner_type_delete'      => ['partner_type.delete', 'partner_type.delete.post'],

		'annoying_client_view'     => ['annoying_client'],
		'annoying_client_add'      => ['annoying_client.add', 'annoying_client.add.post'],
		'annoying_client_edit'     => ['annoying_client.edit', 'annoying_client.edit.post', 'annoying_client.save', 'annoying_client.enable', 'annoying_client.enable.post', 'annoying_client.disable', 'annoying_client.disable.post'],
		'annoying_client_delete'   => ['annoying_client.delete', 'annoying_client.delete.post'],

		'contract_view'            => ['contract'],
		'contract_add'             => ['contract.add', 'contract.add.post'],
		'contract_edit'            => ['contract.edit', 'contract.edit.post', 'contract.save', 'contract.enable', 'contract.enable.post', 'contract.disable', 'contract.disable.post'],
		'contract_delete'          => ['contract.delete', 'contract.delete.post'],

		'driver_work_view'         => ['driver_work'],
		'driver_work_add'          => ['driver_work.add', 'driver_work.add.post'],
		'driver_work_edit'         => ['driver_work.edit', 'driver_work.edit.post', 'driver_work.save', 'driver_work.enable', 'driver_work.enable.post', 'driver_work.disable', 'driver_work.disable.post'],
		'driver_work_delete'       => ['driver_work.delete', 'driver_work.delete.post'],

		'news_view'                => ['news', 'news.view'],
		'news_add'                 => ['news.add', 'news.add.post'],
		'news_edit'                => ['news.edit', 'news.edit.post', 'news.save', 'news.enable', 'news.enable.post', 'news.disable', 'news.disable.post'],
		'news_delete'              => ['news.delete', 'news.delete.post'],

		'report_booking'           => ['report_booking'],
		'report_booking_chi_tiet'  => ['report_booking.chi_tiet', 'report_booking.chi_tiet_export'],
		'report_booking_ke_toan'   => ['report_booking.ke_toan', 'report_booking.ke_toan_export'],
		'report_booking_xe'        => ['report_booking.xe', 'report_booking.xe_export'],
		'report_booking_doanh_thu' => ['report_booking.doanh_thu', 'report_booking.doanh_thu_export'],
		'report_booking_thong_ke'  => ['report_booking.thong_ke', 'report_booking.thong_ke_export'],

		'statistic'                => ['statistic', 'statistic.detail'],

		'log'                      => ['log'],

		'booking_notification'     => ['booking_notification'],
	],

	'permissions_manage' => [
		'booking' => [
			'booking_view', 'booking_add', 'booking_import', 'booking_edit', 'booking_update', 'booking_note', 'booking_delete', 'booking_print', 'booking_approved', 'booking_not_approved', 'booking_car_requested', 'booking_on_duty', 'booking_issue', 'booking_finished', 'booking_cancel', 'booking_no_show', 'booking_completed', 'booking_free_of_charge',
		],
		'report' => [
			'report_booking', 'report_booking_chi_tiet', 'report_booking_ke_toan', 'report_booking_xe', 'report_booking_doanh_thu', 'report_booking_thong_ke',
			'statistic',
		],
		'account' => [
			'user_view', 'user_add', 'user_edit', 'user_delete',
			'group_view', 'group_add', 'group_edit', 'group_delete',
		],
		'fleet' => [
			'car_view', 'car_add', 'car_edit', 'car_delete',
			'cartype_view', 'cartype_add', 'cartype_edit', 'cartype_delete',
			'driver_view', 'driver_add', 'driver_edit', 'driver_delete',
		],
		'network' => [
			'partner_view', 'partner_add', 'partner_edit', 'partner_delete',
			'partner_type_view', 'partner_type_add', 'partner_type_edit', 'partner_type_delete',
			'annoying_client_view', 'annoying_client_add', 'annoying_client_edit', 'annoying_client_delete',
		],
		'admin' => [
			'contract_view', 'contract_add', 'contract_edit', 'contract_delete',
			'driver_work_view', 'driver_work_add', 'driver_work_edit', 'driver_work_delete',
			'news_view', 'news_add', 'news_edit', 'news_delete',
			'log',
			'booking_notification',
		],
	],

	'actions' => [
		'booking.add.post', 'booking.import.post', 'booking.edit.post', 'booking.update.post', 'booking.note.post', 'booking.save', 'booking.delete', 'booking.delete.post', 'booking.approved.post', 'booking.not_approved.post', 'booking.car_requested.post', 'booking.on_duty.post', 'booking.issue.post', 'booking.finished.post', 'booking.cancel.post', 'booking.no_show.post', 'booking.completed.post', 'booking.free_of_charge.post', 'user.add.post',
		'user.edit.post', 'user.delete', 'user.delete.post', 'group.add.post', 'group.edit.post', 'group.delete', 'group.delete.post',
		'car.add.post', 'car.edit.post', 'car.save', 'car.enable', 'car.enable.post', 'car.disable', 'car.disable.post', 'car.delete', 'car.delete.post',
		'cartype.add.post', 'cartype.edit.post', 'cartype.save', 'cartype.enable', 'cartype.enable.post', 'cartype.disable', 'cartype.disable.post', 'cartype.delete', 'cartype.delete.post',
		'driver.add.post', 'driver.edit.post', 'driver.save', 'driver.enable', 'driver.enable.post', 'driver.disable', 'driver.disable.post', 'driver.delete', 'driver.delete.post',
		'partner.add.post', 'partner.edit.post', 'partner.save', 'partner.enable', 'partner.enable.post', 'partner.disable', 'partner.disable.post', 'partner.delete', 'partner.delete.post',
		'partner_type.add.post', 'partner_type.edit.post', 'partner_type.save', 'partner_type.enable', 'partner_type.enable.post', 'partner_type.disable', 'partner_type.disable.post', 'partner_type.delete', 'partner_type.delete.post',
		'annoying_client.add.post', 'annoying_client.edit.post', 'annoying_client.save', 'annoying_client.enable', 'annoying_client.enable.post', 'annoying_client.disable', 'annoying_client.disable.post', 'annoying_client.delete', 'annoying_client.delete.post',
		'contract.add.post', 'contract.edit.post', 'contract.save', 'contract.enable', 'contract.enable.post', 'contract.disable', 'contract.disable.post', 'contract.delete', 'contract.delete.post',
		'driver_work.add.post', 'driver_work.edit.post', 'driver_work.save', 'driver_work.enable', 'driver_work.enable.post', 'driver_work.disable', 'driver_work.disable.post', 'driver_work.delete', 'driver_work.delete.post',
		'news.add.post', 'news.edit.post', 'news.save', 'news.enable', 'news.enable.post', 'news.disable', 'news.disable.post', 'news.delete', 'news.delete.post',
		'report_booking.chi_tiet_export', 'report_booking.ke_toan_export', 'report_booking.xe_export', 'report_booking.doanh_thu_export', 'report_booking.thong_ke_export',
		'login.post', 'logout',
	],

);
