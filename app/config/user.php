<?php

return [

	'type' => [
		0 => 'customer',
		1 => 'admin',
		2 => 'partner',
		3 => 'car',
	],

	'type_id' => [
		'customer' => 0,
		'admin' => 1,
		'partner' => 2,
		'car' => 3,
	],

];