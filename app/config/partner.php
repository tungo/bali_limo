<?php

return array(
	'prefix' => 'partner',
	'path'   => 'partner',
	'theme'  => 'default',
	'layout' => 'partner',

	'event' => [
		'status_update' => 'status.update.partner',
	],

	'items_per_page' => 10,

	'notification' => [
		'list' => 15,
	],

	'routes_objects' => [
	],

	'routes_actions' => [
	],

	'routes' => [
		[
			'',
			'PartnerSystemController',
			[
				['get', '', 'dashboard', 'getIndex'],
			],
		],
		[
			'',
			'PartnerAuthController',
			[
				['get', 'login', 'login', 'getLogin'],
				['post', 'login', 'login.post', 'postLogin'],
				['get', 'logout', 'logout', 'getLogout'],
			],
		],
		[
			'booking',
			'PartnerBookingController',
			[
				['get', '', '', 'getIndex'],
				['get', '/add', '.add', 'getAdd'],
				['post', '/add', '.add.post', 'postAdd'],
				['get', '/view/{id}', '.view', 'getView'],
				['get', '/edit/{id?}', '.edit', 'getEdit'],
				['post', '/edit/{id?}', '.edit.post', 'postEdit'],

				['get', '/import', '.import', 'getImport'],
				['post', '/import', '.import.post', 'postImport'],

				['get', '/print/{id}', '.print', 'getPrint'],
				
				['post', '/cancel', '.cancel.post', 'postCancel'],
			],
		],
		[
			'statistic',
			'PartnerStatisticController',
			[
				['get', '', '', 'getIndex'],
				['get', '/detail', '.detail', 'getDetail'],
			],
		],
		[
			'booking_notification',
			'PartnerBookingNotificationController',
			[
				['get', '', '', 'getIndex'],
			],
		],
	],

	'routes_free_auth' => [
		'login', 'login.post', 'logout',
	],
	'routes_free_permission' => [
		'login', 'login.post', 'logout',
		'dashboard',
	],

	'navigation' => [
		[
			'name'        => 'booking',
			'permissions' => ['booking'],
			'route'       => 'booking',
		],
		[
			'name'        => 'statistic',
			'permissions' => ['statistic'],
			'route'       => 'statistic',
		],
	],

);
