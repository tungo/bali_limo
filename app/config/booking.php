<?php

return [

	'statuses' => [
		0  => '',
		1  => 'booking',
		2  => 'approved',
		3  => 'not_approved',
		4  => 'car_requested',
		5  => 'on_duty',
		6  => 'issue',
		7  => 'finished',
		8  => 'cancel',
		9  => 'no_show',
		10 => 'completed',
		11 => 'free_of_charge',
	],

	'status_id' => [
		'booking'        => 1,
		'approved'       => 2,
		'not_approved'   => 3,
		'car_requested'  => 4,
		'on_duty'        => 5,
		'issue'          => 6,
		'finished'       => 7,
		'cancel'         => 8,
		'no_show'        => 9,
		'completed'      => 10,
		'free_of_charge' => 11,
	],

	'may' => [
		'edit'           => [1, 2, 3, 4],
		'delete'         => [1, 2, 3, 4, 8],
		'note'           => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
		'update'         => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],

		'approved'       => [1, 3],
		'not_approved'   => [1, 2],
		'car_requested'  => [2, 4, 5],
		'on_duty'        => [4],
		'issue'          => [5, 7],
		'finished'       => [5, 6],
		'cancel'         => [3, 4, 6],
		'no_show'        => [6],
		'completed'      => [7],
		'free_of_charge' => [7, 11],
	],

	'may_partner' => [
		'edit'           => [1, 2, 3, 4],
		'delete'         => [1, 2, 3, 4, 8],
		'note'           => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
		'update'         => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],

		'approved'       => [1, 3],
		'not_approved'   => [1, 2],
		'car_requested'  => [2, 4, 5],
		'on_duty'        => [4],
		'issue'          => [5, 7],
		'finished'       => [5, 6],
		'cancel'         => [1, 2, 3, 4, 8],
		'no_show'        => [6],
		'completed'      => [7],
		'free_of_charge' => [7, 11],
	],

	'color' => [
		1  => 'booking',
		2  => 'approved',
		3  => 'not_approved',
		4  => 'car_requested',
		5  => 'on_duty',
		6  => 'issue',
		7  => 'finished',
		8  => 'cancel',
		9  => 'no_show',
		10 => 'completed',
		11 => 'free_of_charge',
	],

	'color_hex' => [
		1  => '#f39c12',
		2  => '#18bc9c',
		3  => '#e5584a',
		4  => '#3498db',
		5  => '#903667',
		6  => '#d92626',
		7  => '#63B81F',
		8  => '#95a5a6',
		9  => '#2c3e50',
		10 => '#1b822b',
		11 => '#333333',
	],

	'label' => [
		1  => 'warning',
		2  => 'success',
		3  => 'danger',
		4  => 'primary',
		5  => 'primary',
		6  => 'warning',
		7  => 'info',
		8  => 'danger',
		9  => 'danger',
		10 => 'success',
		11 => 'success',
	],

	'process' => [
		1  => 1,
		2  => 2,
		3  => 2,
		4  => 3,
		5  => 4,
		6  => 5,
		7  => 5,
		8  => 6,
		9  => 6,
		10 => 6,
		11 => 6,
	],

	'done' => [
		9  => 'no_show',
		10 => 'completed',
		11 => 'free_of_charge',
	],

	'types' => [
		1 => 'pick_up',
		2 => 'drop_off',
		3 => 'other',
	],

	'types_code' => [
		'p' => 1,
		'd' => 2,
		'o' => 3,
	],

	'data_types' => [
		'review' => 'review',
	],

	'car_works' => [
		'y' => ['on_duty', 'issue'],
		'n' => ['finished'],
	],


];