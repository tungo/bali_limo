<?php

return array(
	'prefix' => 'car',
	'path'   => 'car',
	'theme'  => 'default',
	'layout' => 'car',

	'event' => [
		'status_update' => 'status.update.car',
	],

	'items_per_page' => 10,

	'notification' => [
		'list' => 15,
	],

	'routes_objects' => [
	],

	'routes_actions' => [
	],

	'routes' => [
		[
			'',
			'CarSystemController',
			[
				['get', '', 'dashboard', 'getIndex'],
			],
		],
		[
			'',
			'CarAuthController',
			[
				['get', 'login', 'login', 'getLogin'],
				['post', 'login', 'login.post', 'postLogin'],
				['get', 'logout', 'logout', 'getLogout'],
				['post', 'logout', 'logout.post', 'postLogout'],
			],
		],
		[
			'booking',
			'CarBookingController',
			[
				['get', '', '', 'getIndex'],

				// ['get', '/on_duty', '.on_duty', 'getOnDuty'],
				['post', '/on_duty/{id}', '.on_duty.post', 'postOnDuty'],
				// ['get', '/issue', '.issue', 'getIssue'],
				['post', '/issue/{id}', '.issue.post', 'postIssue'],
				['get', '/finished/{id}', '.finished', 'getFinished'],
				['post', '/finished/{id}', '.finished.post', 'postFinished'],

				['post', '/sign/{id}', '.sign.post', 'postSign'],
				['get', '/sign/{id}', '.sign', 'postSign'],
				['post', '/review/{id}', '.review.post', 'postReview'],
			],
		],
	],

	'routes_free_auth' => [
		'login', 'login.post', 'logout', 'logout.post',
	],
	'routes_free_permission' => [
		'login', 'login.post', 'logout', 'logout.post',
		'dashboard',
		'booking.sign.post', 'booking.review.post',
	],

);
