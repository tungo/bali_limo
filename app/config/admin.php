<?php

return array(
	'prefix' => 'admin', // module name
	'path'   => 'admin', // path url
	'theme'  => 'default',
	'layout' => 'default',

	'event' => [
		'status_update' => 'status.update.admin',
		'car_work'      => 'car.work',
	],

	'items_per_page' => 10,

	'notification' => [
		'list' => 15,
	],

	'routes_objects' => [
		// 'object' => [ // properties
		// 	'controller',
		// 	['disabled'],
		// ],
		'car'             => ['AdminCarController', []],
		'cartype'         => ['AdminCartypeController', []],
		'driver'          => ['AdminDriverController', []],
		'partner'         => ['AdminPartnerController', []],
		'partner_type'    => ['AdminPartnerTypeController', []],
		'contract'        => ['AdminContractController', []],
		'annoying_client' => ['AdminAnnoyingClientController', []],
		'news'            => ['AdminNewsController', []],

		'driver_work'     => [
			'AdminDriverWorkController',
			['.add', '.add.post', '.edit', '.edit.post', '.delete', '.delete.post', '.enable', '.enable.post', '.disable', '.disable.post'],
		],
		'user' => [
			'AdminUserController',
			['.save', '.enable', '.enable.post', '.disable', '.disable.post'],
		],
		'group' => [
			'AdminGroupController',
			['.save', '.enable', '.enable.post', '.disable', '.disable.post'],
		],
	],

	'routes_actions' => [
		// 'action' => ['verb', 'path', 'method'],
		''              => ['get', '', 'getIndex'],
		'.add'          => ['get', '/add', 'getAdd'],
		'.add.post'     => ['post', '/add', 'postAdd'],
		'.view'         => ['get', '/view/{id}', 'getView'],
		'.edit'         => ['get', '/edit/{id}', 'getEdit'],
		'.edit.post'    => ['post', '/edit/{id}', 'postEdit'],
		'.save'         => ['post', '/save', 'postSave'],
		'.delete'       => ['get', '/delete/{id}', 'getDelete'],
		'.delete.post'  => ['post', '/delete', 'postDelete'],
		'.enable'       => ['get', '/enable/{id}', 'getEnable'],
		'.enable.post'  => ['post', '/enable', 'postEnable'],
		'.disable'      => ['get', '/disable/{id}', 'getDisable'],
		'.disable.post' => ['post', '/disable', 'postDisable'],
	],

	'routes' => [
		// [
		// 	'object',
		// 	'controller',
		// 	[ // actions
		// 		['verb', 'path', 'action', 'method'],
		// 	],
		// ],
		[
			'',
			'AdminSystemController',
			[
				['get', '', 'dashboard', 'getIndex'],
				['get', 'dev', 'dev', 'getDev'],
				['get', 'test124', 'test124', 'getTest124'],
			],
		],
		[
			'',
			'AdminAuthController',
			[
				['get', 'login', 'login', 'getLogin'],
				['post', 'login', 'login.post', 'postLogin'],
				['get', 'logout', 'logout', 'getLogout'],
			],
		],
		[
			'booking',
			'AdminBookingController',
			[
				['get', '', '', 'getIndex'],
				['get', '/add', '.add', 'getAdd'],
				['post', '/add', '.add.post', 'postAdd'],
				['get', '/view/{id}', '.view', 'getView'],
				['get', '/edit/{id?}', '.edit', 'getEdit'],
				['post', '/edit/{id?}', '.edit.post', 'postEdit'],
				// ['get', '/delete/{id?}', '.delete', 'getDelete'],
				['post', '/delete', '.delete.post', 'postDelete'],

				['get', '/update/{id?}', '.update', 'getUpdate'],
				['post', '/update/{id?}', '.update.post', 'postUpdate'],
				['get', '/note/{id?}', '.note', 'getNote'],
				['post', '/note/{id?}', '.note.post', 'postNote'],

				['get', '/import', '.import', 'getImport'],
				['post', '/import', '.import.post', 'postImport'],
				
				['get', '/print/{id}', '.print', 'getPrint'],

				// ['get', '/approved', '.approved', 'getApproved'],
				['post', '/approved', '.approved.post', 'postApproved'],
				// ['get', '/not_approved', '.not_approved', 'getNotApproved'],
				['post', '/not_approved', '.not_approved.post', 'postNotApproved'],
				['get', '/car_requested', '.car_requested', 'getCarRequested'],
				['post', '/car_requested', '.car_requested.post', 'postCarRequested'],
				// ['get', '/on_duty', '.on_duty', 'getOnDuty'],
				['post', '/on_duty', '.on_duty.post', 'postOnDuty'],
				// ['get', '/issue', '.issue', 'getIssue'],
				['post', '/issue', '.issue.post', 'postIssue'],
				// ['get', '/finished', '.finished', 'getFinished'],
				['post', '/finished', '.finished.post', 'postFinished'],
				// ['get', '/cancel', '.cancel', 'getCancel'],
				['post', '/cancel', '.cancel.post', 'postCancel'],
				['get', '/no_show', '.no_show', 'getNoShow'],
				['post', '/no_show', '.no_show.post', 'postNoShow'],
				// ['get', '/completed', '.completed', 'getCompleted'],
				['post', '/completed', '.completed.post', 'postCompleted'],
				['post', '/free_of_charge', '.free_of_charge.post', 'postFreeOfCharge'],
			],
		],
		[
			'report_booking',
			'AdminReportBookingController',
			[
				['get', '', '', 'getIndex'],
				['get', '/chi_tiet', '.chi_tiet', 'getChiTiet'],
				['get', '/chi_tiet_export', '.chi_tiet_export', 'getChiTietExport'],
				['get', '/ke_toan', '.ke_toan', 'getKeToan'],
				['get', '/ke_toan_export', '.ke_toan_export', 'getKeToanExport'],
				['get', '/xe', '.xe', 'getXe'],
				['get', '/xe_export', '.xe_export', 'getXeExport'],
				['get', '/doanh_thu', '.doanh_thu', 'getDoanhThu'],
				['get', '/doanh_thu_export', '.doanh_thu_export', 'getDoanhThuExport'],
				['get', '/thong_ke', '.thong_ke', 'getThongKe'],
				['get', '/thong_ke_export', '.thong_ke_export', 'getThongKeExport'],
			],
		],
		[
			'log',
			'AdminLogController',
			[
				['get', '', '', 'getIndex'],
			],
		],
		[
			'statistic',
			'AdminStatisticController',
			[
				['get', '', '', 'getIndex'],
				['get', '/detail', '.detail', 'getDetail'],
			],
		],
		[
			'booking_notification',
			'AdminBookingNotificationController',
			[
				['get', '', '', 'getIndex'],
			],
		],
	],

	'routes_free_auth' => [
		'login', 'login.post', 'logout',
	],
	'routes_free_permission' => [
		'login', 'login.post', 'logout',
		'dashboard', 'dev', 'test124',
	],

	'navigation' => [
		// [
		// 	'name'     => '',
		// 	'children' => [
		// 		[
		// 			'name'        => '',
		// 			'route'       => '',
		// 			'permissions' => [''],
		// 		],
		// 		// [
		// 		// 	'type'   => 'header',
		// 		// 	'header' => '',
		// 		// ],
		// 	],
		// 	// 'permissions' => [], // !isset: children permisions, empty: free, !empty: own permisions
		// 	// 'route' => '',
		// ],
		[
			'name'     => 'booking',
			'permissions' => ['booking'],
			'route' => 'booking',
		],
		[
			'name'     => 'report',
			'children' => [
				[
					'name'        => 'report_booking',
					'route'       => 'report_booking',
					'permissions' => ['report_booking'],
				],
				[
					'name'        => 'statistic',
					'permissions' => ['statistic'],
					'route'       => 'statistic',
				],
			],
		],
		[
			'name'     => 'account',
			'children' => [
				[
					'name'        => 'user',
					'route'       => 'user',
					'permissions' => ['user'],
				],
				[
					'name'        => 'group',
					'route'       => 'group',
					'permissions' => ['group'],
				],
			],
		],
		[
			'name'     => 'fleet',
			'children' => [
				[
					'name'        => 'car',
					'route'       => 'car',
					'permissions' => ['car'],
				],
				[
					'name'        => 'cartype',
					'route'       => 'cartype',
					'permissions' => ['cartype'],
				],
				[
					'name'        => 'driver',
					'route'       => 'driver',
					'permissions' => ['driver'],
				],
			],
		],
		[
			'name'     => 'network',
			'children' => [
				[
					'name'        => 'partner',
					'route'       => 'partner',
					'permissions' => ['partner'],
				],
				[
					'name'        => 'partner_type',
					'route'       => 'partner_type',
					'permissions' => ['partner_type'],
				],
				[
					'name'        => 'annoying_client',
					'route'       => 'annoying_client',
					'permissions' => ['annoying_client'],
				],
			],
		],
		[
			'name'     => 'admin',
			'children' => [
				[
					'name'        => 'contract',
					'route'       => 'contract',
					'permissions' => ['contract'],
				],
				[
					'name'        => 'driver_work',
					'route'       => 'driver_work',
					'permissions' => ['driver_work'],
				],
				[
					'name'        => 'news',
					'route'       => 'news',
					'permissions' => ['news'],
				],
				[
					'name'        => 'log',
					'route'       => 'log',
					'permissions' => ['log'],
				],
			],
		],
	],

);
