<?php namespace TnExceptions;

use Exception;

class TnException extends Exception {

}

class InfoNotFoundException extends TnException {}

class AddException extends TnException {}
class EditException extends TnException {}
class DeleteException extends TnException {}

class RelationRecordExistException extends TnException {}

class UpdateStatusException extends TnException {}