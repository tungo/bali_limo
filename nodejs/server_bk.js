const io = require('socket.io').listen(3001);
const redis = require('redis');
const redisClient = redis.createClient();

redisClient.subscribe('status.update');

redisClient.on('message', function(channel, message) {
io.sockets.emit(channel, message);
});