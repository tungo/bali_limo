var express =   require('express'),
    http =      require('http'),
    server =    http.createServer(app);

var app = express();

const redis =   require('redis');
const io =      require('socket.io');
const client =  redis.createClient();

server.listen(3000);

io.listen(server).on('connection', function(client) {
    const redisClient = redis.createClient();

    redisClient.subscribe('status.update.admin');
    redisClient.subscribe('status.update.partner');
    redisClient.subscribe('status.update.car');

    redisClient.subscribe('car.work');

    redisClient.on("message", function(channel, message) {
        //Channel is e.g 'status.update'
        client.emit(channel, message);
    });

    client.on('disconnect', function() {
        redisClient.quit();
    });
});