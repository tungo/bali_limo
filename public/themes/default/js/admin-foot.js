$(document).ready(function(){

	$("#backtop").hide();
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#backtop').fadeIn();
			} else {
				$('#backtop').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#backtop a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});

	$(':checkbox[name="check_all"]').on('change', function(){
		$(':checkbox[name="cid[]"]').prop('checked', $(this).is(':checked'));
	});
	$(':checkbox[name="cid[]"]').on('change', function() {
		if (! $(this).is(':checked'))
		{
			$(':checkbox[name="check_all"]').prop('checked', false);
		}
		else
		{
			if ($(':checkbox[name="cid[]"]:checked').length == $(':checkbox[name="cid[]"]').length)
			{
				$(':checkbox[name="check_all"]').prop('checked', true);
			}
		}
	});

	$('.manage-table tbody tr').on('click', function(event) {
		var $target = $(event.target);
		if($target.is('td'))
		{
			$(this).find(':checkbox[name="cid[]"]').click();
		}
	});

	$('#update_form').on('click', '.delete_image', function(){
		$(this).prevAll('img.old_image').prop('src', '/public/images/no-image.png');
		$(this).prevAll(':hidden[name^="delete_image"]').val('1');
	});

	$('.submit-data-form').on('click', function(e){
		e.preventDefault();
		if ($(this).attr('data-require'))
		{
			if ($(':checkbox[name="cid[]"]:checked').length == 0)
			{
				alert($(this).attr('data-require'));
				return false;
			}
		}
		if ($(this).attr('data-confirm'))
		{
			if (!confirm($(this).attr('data-confirm')))
			{
				return false;
			}
		}
		if ($(this).attr('data-method'))
		{
			$('#'+$(this).attr('data-form')).attr('method', $(this).attr('data-method'));
		}

		if ($(this).attr('data-toggle') == 'modal')
		{
			$('#'+$(this).attr('data-form')).attr('action', $(this).attr('href'));
		}
		else
		{
			$('#'+$(this).attr('data-form')).attr('action', $(this).attr('href')).submit();
		}
	});


	$('.check-all-data-items').on('change', function(){
		$('.check-data-item[data-item="'+$(this).attr('data-item')+'"]').prop('checked', $(this).is(':checked'));
	});
	$('.check-data-item').on('change', function() {
		if (! $(this).is(':checked'))
		{
			$('.check-all-data-items[data-item="'+$(this).attr('data-item')+'"]').prop('checked', false);
		}
		else
		{
			if ($('.check-data-item[data-item="'+$(this).attr('data-item')+'"]:checked').length == $('.check-data-item[data-item="'+$(this).attr('data-item')+'"]').length)
			{
				$('.check-all-data-items[data-item="'+$(this).attr('data-item')+'"]').prop('checked', true);
			}
		}
	});
	$('.check-data-item').each(function(index) {
		if (! $(this).is(':checked'))
		{
			$('.check-all-data-items[data-item="'+$(this).attr('data-item')+'"]').prop('checked', false);
		}
	});

	$('.btn-search').click(function(){
		var status = $(this).attr('data-status');
		if (status == 'open')
		{
			$(this).attr('data-status', 'close');
			$(this).find('i.icon-down-open').hide();
			$(this).find('i.icon-right-open ').show();
			$(this).closest('.site').find('.wrapper-search').hide();
		}
		else
		{
			$(this).attr('data-status', 'open');
			$(this).find('i.icon-right-open').hide();
			$(this).find('i.icon-down-open').show();
			$(this).closest('.site').find('.wrapper-search').show();
		}
		return false;
	});

	$('.btn-clear-data-form').on('click', function(){
		clear_form($(this).attr('data-form'));
	});
});

function submit_form(url, cf)
{
	if (! url)
	{
		return false;
	}
	if (! cf || (cf && confirm(cf)))
	{
		$('#manage_form').prop('action', url).submit();
		return false;
	}
	return false;
}

function submit_manage_form(url, cf)
{
	if ($(':checkbox[name="cid[]"]:checked').length == 0)
	{
		alert('Please select one or more rows.');
		return false;
	}
	if (! url)
	{
		return false;
	}
	if (! cf || (cf && confirm(cf)))
	{
		$('#manage_form').prop('action', url).submit();
		return false;
	}
	return false;
}

function valid_update_form(rule)
{
	$('#update_form').validate({
		rules: rule,
		highlight: function(element) {
			// $(element).closest('.form-control').parent().parent().removeClass('has-success').addClass('has-error');
		},
		success: function(element) {
			// $(element).closest('.form-control').parent().parent().removeClass('has-error').addClass('has-success');
			$(element).remove();
		}
	});
}

function clear_form(form_id)
{
	$('#'+form_id+' :input')
	.not(':button, :submit, :reset, :hidden').removeAttr('checked').removeAttr('selected')
	// .not(':checkbox, :radio, select').val('');
	.not(':checkbox, :radio').val('');
}

